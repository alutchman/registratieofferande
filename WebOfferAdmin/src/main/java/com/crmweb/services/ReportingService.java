package com.crmweb.services;

import com.crmweb.reports.ConceptContractSettings;
import com.crmweb.reports.MiscCalculations;
import com.crmweb.reports.PdfBytesProducer;
import com.crmweb.utils.MaandTransacties;
import com.crmweb.utils.TransferUtils;
import com.crmweb.utils.ZipUtilExtra;
import com.domain.ukgr.dao.DaoJaarPersonTransacties;
import com.domain.ukgr.dao.DaoLocatieJaar;
import com.domain.ukgr.dao.DaoTransacties;
import com.domain.ukgr.repo.JaarPerMethodRepo;
import com.domain.ukgr.repo.LocatiesRepo;
import com.domain.ukgr.repo.MaandPerMethodRepo;
import com.domain.ukgr.repo.PayerRepo;
import com.domain.ukgr.tables.JaarPerLocatie;
import com.domain.ukgr.tables.JaarPerMethod;
import com.domain.ukgr.tables.JaarPersonTransacties;
import com.domain.ukgr.tables.MaandPerMethod;
import com.domain.ukgr.tables.Payer;
import com.domain.ukgr.tables.Transacties;
import com.shared.transfers.LocatieData;
import com.shared.transfers.LocatiePayerYearTotal;
import com.shared.transfers.LocatieYearTotal;
import com.shared.transfers.NewUploadFileInfo;
import com.shared.transfers.PayerData;
import com.shared.transfers.TransactieData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportingService {
    private static final Logger logger = LoggerFactory.getLogger(ReportingService.class);

    @Autowired
    private PdfBytesProducer pdfBytesProducer;

    @Autowired
    private PayerRepo payerRepo;

    @Autowired
    private LocatiesRepo locatiesRepo;


    @Autowired
    private DaoTransacties<Transacties> daoTransacties;

    @Autowired
    private JaarPerMethodRepo jaarPerMethodRepo;

    @Autowired
    private DaoLocatieJaar<JaarPerLocatie> daoLocatieJaar;

    @Autowired
    private DaoJaarPersonTransacties<JaarPersonTransacties> daoJaarPersonTransacties;

    @Autowired
    private MaandPerMethodRepo maandPerMethodRepo;

    @Autowired
    private ConceptContractSettings conceptContractSettings;

    public String getBaseProjectMap(){
        return conceptContractSettings.getDirScanFiles();
    }

    public byte[] createPDF(HttpServletRequest request, Map<String, String> paramsRequest, String sessionID, Long locationID) {
        String baseUrl = initBaseUrlAndPerapre(request, paramsRequest, sessionID);
        paramsRequest.put("locationID",String.valueOf(locationID));

        String url       =  baseUrl  + conceptContractSettings.getBaseUrlUnformatted();

        String[] htmlBody;

        if (paramsRequest.get("reportType").equals("PERSONAL_PRINT")) {
            int maxPerPage = 76;

            Payer payer = payerRepo.findById(Long.valueOf(paramsRequest.get("payerID"))).orElse(null);
            int aantal = findTransactionsInYear(payer, Integer.valueOf(paramsRequest.get("jaartal")), locationID);

            paramsRequest.put("aantal",String.valueOf(aantal));
            paramsRequest.put("maxPerPage",String.valueOf(maxPerPage));


            int pages = TransferUtils.calculatePages(aantal, maxPerPage);

            paramsRequest.put("pages",String.valueOf(pages));

            htmlBody = getHTMLPersonal(pages, paramsRequest, url, sessionID);
        } else {
            paramsRequest.put("pages",String.valueOf(1));
            htmlBody = getHTMLTaxes(paramsRequest, url, sessionID);

        }
        byte[] contents = pdfBytesProducer.createPdfDataFromHtml(htmlBody, paramsRequest);
        return contents;
    }


    public int findTransactionsInYear(Payer payer, int jaarTal, long adminLocatie){
       return daoTransacties.findTransactionsInYear(payer.getId(), Integer.valueOf(jaarTal), adminLocatie);
    }

    public String initBaseUrlAndPerapre(HttpServletRequest request, Map<String, String> paramsRequest, String sessionID){
        String baseUrl   = MiscCalculations.getBaseUrlFromRequest(request);

        paramsRequest.put("serverURL"  , "http://127.0.0.1:8080");
        paramsRequest.put("appContext"  , request.getContextPath());
        paramsRequest.put("briefUrl"  ,  baseUrl  + "/pdfDynamic/brief.html");
        paramsRequest.put("sessionID", sessionID);

        return baseUrl;

    }


    /**
     *
     * @param params
     * @param url
     * @param sessionID
     * @return
     */
    public String[] getHTMLTaxes(Map<String, String> params, String url, String sessionID) {
        List<String> htmlData = new ArrayList<>();
        params.put("currentIndex",String.valueOf(0));
        params.put("jspPage", String.format(conceptContractSettings.getJspPage(), 0));
        TransferUtils.fetchHtmlData(htmlData, params, String.format(url,0), sessionID);
        String[] htmlBody=htmlData.toArray(new String[htmlData.size()]);
        return htmlBody;
    }


    /**
     *
     * @param pages
     * @param params
     * @param url
     * @param sessionID
     * @return
     */
    public String[] getHTMLPersonal(int pages, Map<String, String> params, String url, String sessionID) {
        List<String> htmlData = new ArrayList<>();

        params.put("director", conceptContractSettings.getDirector());
        if (!params.containsKey("skipLetter")) {
            params.put("jspPage", "pdfDynamic/brief");
            TransferUtils.fetchHtmlData(htmlData, params, params.get("briefUrl"), sessionID);
        }

        for(int index= 0; index < pages; index++) {
            params.put("currentIndex",String.valueOf(index));
            int pageIndexToUse = index == 0 ? index : 1;
            params.put("jspPage", String.format(conceptContractSettings.getJspPage(), pageIndexToUse));
            TransferUtils.fetchHtmlData(htmlData, params, String.format(url,index), sessionID);
        }
        String[] htmlBody=htmlData.toArray(new String[htmlData.size()]);

        return htmlBody;
    }


    public String[] getHtmlForLocations(Map<String, String> params, String url, String sessionID, int pages) {
        List<String> htmlData = new ArrayList<>();

        for(int index= 0; index < pages; index++) {
            params.put("currentIndex",String.valueOf(index));
            TransferUtils.fetchHtmlData(htmlData, params, url, sessionID);
        }
        String[] htmlBody=htmlData.toArray(new String[htmlData.size()]);
        return htmlBody;
    }


    public byte[] createPDFLocation(HttpServletRequest request, Map<String, String> paramsRequest, String sessionID) {
        String baseUrl   = MiscCalculations.getBaseUrlFromRequest(request);
        String url       =  baseUrl  + paramsRequest.get("reportURL");
        paramsRequest.put("serverURL"  , "http://127.0.0.1:8080");
        paramsRequest.put("appContext"  , request.getContextPath());

        Integer maxPerPage = Integer.valueOf(paramsRequest.get("maxPerPage"));

        Long currentLocatie = Long.valueOf(paramsRequest.get("currentLocatie"));
        int jaartal = Integer.valueOf(paramsRequest.get("jaartal"));

        List<BigInteger> countResult = payerRepo.findPayersForYear(currentLocatie, jaartal);
        int aantal = countResult.get(0).intValue();
        int pages = TransferUtils.calculatePages(aantal, maxPerPage);
        paramsRequest.put("pages",String.valueOf(pages));
        String[] htmlBody = getHtmlForLocations(paramsRequest, url, sessionID, pages);
        byte[] contents = pdfBytesProducer.createPdfDataFromHtml( htmlBody, paramsRequest);
        return contents;
    }

    public void updateModelmap(ModelMap modelMap, HttpServletRequest request, Map<String, String> params) {
        modelMap.addAllAttributes(params);
        String title = String.format("%s %s", conceptContractSettings.getHeaderTitle() , params.get("jaartal"));
        modelMap.addAttribute("title"    , title) ;
        modelMap.addAttribute("plaats"   , conceptContractSettings.getLocation());
        modelMap.addAttribute("director" , conceptContractSettings.getDirector());

        modelMap.put("baseUrl"  , MiscCalculations.getBaseUrlFromRequest(request));
        modelMap.addAllAttributes(params);
    }

    public List<JaarPerMethod> getJaarOverzichtForPayer(Long payerId, int jaartal, long adminLocatie){
        return jaarPerMethodRepo.findByMethodPayerJaarPerJaar(payerId, jaartal, adminLocatie);
    }


    public List<MaandPerMethod> getMaandenInJaarOverzichtForPayer(Long locatieID, Long payerId, int jaartal){
        return maandPerMethodRepo.findByMethodPayerInMaandPerJaar(locatieID, payerId, jaartal);
    }

    @Transactional
    public List<TransactieData> getPayerTransacties(Long locationID, Long payerId, int jaartal, int index, int maxPerPage){
        Payer payer = payerRepo.getOne(payerId);
        List<Transacties> dbData = daoTransacties.findByPayerYear(locationID, payer, jaartal, index, maxPerPage);

        return dbData.stream().map(TransferUtils::fromTransacties).collect(Collectors.toList());
    }


    public List<MaandTransacties> vulMaandOverzicht(long locationID, long payerId, int jaartal){
        List<MaandTransacties> maandoverzicht = new ArrayList<>();


        List<MaandPerMethod> dbmaandoverzicht =  getMaandenInJaarOverzichtForPayer(locationID, payerId, jaartal);
        for (int i=1; i<=12; i++) {
            maandoverzicht.add(new MaandTransacties(i));
        }
        for(MaandPerMethod item : dbmaandoverzicht) {
            int maand = item.getMaand();
            MaandTransacties maandTransactie = maandoverzicht.get(maand-1);
            if (item.getBetaalmethode().equals("BANK")){
                maandTransactie.setBank(item.getTotaal());
            } else if (item.getBetaalmethode().equals("PIN")){
                maandTransactie.setPin(item.getTotaal());
            } else if (item.getBetaalmethode().equals("CONTANT")){
                maandTransactie.setContant(item.getTotaal());
            }
        }

        return maandoverzicht;
    }


    @Transactional
    public PayerData getPayerInfo(Long payerId){
        Payer payer = payerRepo.getOne(payerId);
        return payer != null ? TransferUtils.fromPayer(payer) : new PayerData();
    }

    public void writeOSDownloadScanAsZip(int yearTax, PayerData currentPayer, OutputStream os) {
        NewUploadFileInfo newUploadFileInfo =
                new NewUploadFileInfo(conceptContractSettings.getDirScanFiles(), currentPayer, yearTax);
        String dirDownload = newUploadFileInfo.getPathName();
        logger.info("Downloading From loacation {}",   dirDownload );

        ZipUtilExtra zipUtilExtra = new ZipUtilExtra(dirDownload);
        try {
            zipUtilExtra.compressFiles(os);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                os.flush();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    /**
     *
     * @param yearTax
     * @param ukgrcode
     * @return
     */
    public LocatieYearTotal yearLocatieTotal(int yearTax, int ukgrcode){
        JaarPerLocatie jaarPerLocatie = daoLocatieJaar.findForJaarUkgrCode(yearTax, ukgrcode);
        return TransferUtils.fromJaarPerLocatie(jaarPerLocatie);
    }

    /**
     *
     * @param jaar
     * @param locatieId
     * @param index
     * @param maxPerPage
     * @param zipsort
     * @return
     */
    public List<LocatiePayerYearTotal> findpayerdPerYearAtLocation(int jaar, Long locatieId, int index, int maxPerPage, boolean zipsort){
        List<LocatiePayerYearTotal> result = new ArrayList<>();
        List<JaarPersonTransacties> dateList = zipsort ?
                daoJaarPersonTransacties.findForJaarPostCodeSort(jaar, locatieId,index, maxPerPage) :
                daoJaarPersonTransacties.findForJaarNameSort(jaar, locatieId,index, maxPerPage)  ;

        dateList.stream().forEach(e -> result.add(TransferUtils.fromJaarPersonTransacties(e)));
        return result;
    }


    public List<LocatieData> getListForPayer(long payerid){
       return locatiesRepo.usedLocationByPayer(payerid).stream().
               map(TransferUtils::fromLocaties).collect(Collectors.toList());

    }


    public String getSignatureForLetter() {
        String basemap = conceptContractSettings.getDirScanFiles();
        String signFileName = basemap + "signature.png";

        String encodeImageData = null;

        File signFile = new File(signFileName);
        if (signFile.exists()) {
            try {
                RandomAccessFile f = new RandomAccessFile(signFileName, "r");
                long size = f.length();
                byte[] imgData = new byte[(int) size];
                f.readFully(imgData);

                encodeImageData = Base64.getEncoder().encodeToString(imgData);
            } catch (IOException e) {

            }
        }

        if (encodeImageData == null) {
            try {
                byte[] imgData = Files.readAllBytes(
                        Paths.get(getClass().getResource("/base_sign.png").toURI()));
                encodeImageData = Base64.getEncoder().encodeToString(imgData);
            } catch (URISyntaxException | IOException e) {

            }
        }

        if (encodeImageData != null) {
            return "data:image/png;charset=utf-8;base64," + encodeImageData.trim();
        } else {
            return null;
        }
    }
}
