package com.crmweb.services;

import com.crmweb.reports.ConceptContractSettings;
import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.dao.DaoTransacties;
import com.domain.ukgr.tables.Transacties;
import com.shared.transfers.NewUploadFileInfo;
import com.shared.transfers.PayerData;
import com.shared.transfers.TransactCorrection;
import com.shared.transfers.TransactieData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CorrectionServices {
    private static final Logger LOGGER = LoggerFactory.getLogger(CorrectionServices.class);
    DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");


    @Autowired
    private DaoTransacties<Transacties> daoTransacties;

    @Autowired
    private ConceptContractSettings conceptContractSettings;


    public NewUploadFileInfo uploadFileFromScan(PayerData payerData, TransactieData transactieData){
        NewUploadFileInfo newUploadFileInfo =
                new NewUploadFileInfo(conceptContractSettings.getDirScanFiles(), payerData, transactieData);
        return newUploadFileInfo;
    }

    @Transactional
    public List<TransactCorrection> findForCorrecting(String webuserid, Date creation_time, String postcode,
                                                      String voornaam, String achternaam) {
        List<TransactCorrection> fulllList = new ArrayList<>();
        //LOGGER.info("Getting info from date: {}", inputFormatter.format(creation_time));
        List<Transacties> dbData = daoTransacties.findForCorrecting(webuserid, creation_time, postcode,
                voornaam, achternaam);
        for (int i= 0; i < dbData.size(); i++) {
            fulllList.add(TransferUtils.fromTransactiesCombined(dbData.get(i)));
        }
        return fulllList;
    }

    @Transactional
    public void updateTransaction(TransactCorrection currentTrx) {
        Transacties trx = daoTransacties.fetchById(currentTrx.getTransactieData().getId());
        trx.setBedrag(currentTrx.getTransactieData().getBedrag());
        trx.setBetaalmethode(currentTrx.getTransactieData().getBetaalmethode());
        trx.setScanfile(currentTrx.getTransactieData().getScanfile());
        trx.setAdminlocatie(currentTrx.getTransactieData().getAdminlocatie());
        trx.setTransactiedatum(new Timestamp(currentTrx.getTransactieData().getTransactiedatum().getTime()));
        trx.setCreation_time(new Timestamp(currentTrx.getTransactieData().getCreation_time().getTime()));
    }

    @Transactional
    public void removeTransaction(TransactCorrection delTrx) {
        NewUploadFileInfo newUploadFileInfo =
                uploadFileFromScan(delTrx.getPayerData(), delTrx.getTransactieData());
        String scanFile = delTrx.getTransactieData().getScanfile();
        Transacties trx = daoTransacties.fetchById(delTrx.getTransactieData().getId());
        daoTransacties.delete(trx);
        newUploadFileInfo.deleteOldInfo(scanFile);
    }
}
