package com.crmweb.services;

import com.crmweb.reports.ConceptContractSettings;
import com.crmweb.utils.AdresInfo;
import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.dao.DaoTransacties;
import com.domain.ukgr.repo.PayerRepo;
import com.domain.ukgr.repo.PostcodeRepo;
import com.domain.ukgr.tables.Payer;
import com.domain.ukgr.tables.Postcodes;
import com.domain.ukgr.tables.Transacties;
import com.shared.transfers.NewUploadFileInfo;
import com.shared.transfers.PayerData;
import com.shared.transfers.PostCodeData;
import com.shared.transfers.TransactieData;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PayerService {
    private static final Logger logger = LoggerFactory.getLogger(PayerService.class);
    private static final String url = "https://www.postnl.nl/api/addressexpress/address";
    //https://www.postnl.nl/adres-zoeken/?number=11&postal-code=2907DD&additional_value=

    @Autowired
    private PayerRepo payerRepo;

    @Autowired
    private PostcodeRepo postcodeRepo;

    @Autowired
    private DaoTransacties<Transacties> daoTransacties;

    @Autowired
    private ConceptContractSettings conceptContractSettings;

    public List<PayerData> findForPostCodeHuisNummer(String postCode, int huisnummer){
        return payerRepo.findbyPostcodeHuisNummer(postCode, huisnummer).stream().
                map(TransferUtils::fromPayer).collect(Collectors.toList())  ;
    }

    public List<PayerData> findForNames(String achternaam, String voornaam) {
        if ((voornaam == null || voornaam.trim().length() == 0)  &&  achternaam != null && achternaam.trim().length()>0) {
            return payerRepo.findByLastName(achternaam.trim() + "%").stream().map(TransferUtils::fromPayer).collect(Collectors.toList());
        } else if ((achternaam == null || achternaam.trim().length() == 0)  &&  voornaam != null && voornaam.trim().length()>0){
                return payerRepo.findByLastNameFirstName("%", voornaam.trim()+"%").stream().
                        map(TransferUtils::fromPayer).collect(Collectors.toList());
        } else if (achternaam != null && achternaam.trim().length()>0 && voornaam != null && voornaam.trim().length()>0){
            return payerRepo.findByLastNameFirstName(achternaam.trim() + "%", voornaam.trim() + "%").stream().
                    map(TransferUtils::fromPayer).collect(Collectors.toList())  ;
        } else {
            return new ArrayList<>();
        }
    }

    public PostCodeData findDetailsPostCode(String postCode, Integer huisnummer){
        List<Postcodes>  foundList = postcodeRepo.findByPostcode(postCode);
        if (foundList.size() == 0) {
            PostCodeData postCodeData = new PostCodeData();
            postCodeData.setPostcode(postCode);
            if (huisnummer != null && huisnummer > 0) {
                try {
                    AdresInfo adresInfo = this.fetchData(postCode, huisnummer,"");
                    if (adresInfo != null) {
                        postCodeData.setHuisnummer(huisnummer);
                        postCodeData.setPlaats(adresInfo.getCity());
                        postCodeData.setStraat(adresInfo.getStreet());
                    }
                } catch (IOException e) {

                }
            }
            return postCodeData;
        } else {
            String joinedFirstNames = foundList.stream()
                    .map(Postcodes::getStraat)
                    .collect(Collectors.joining("/"));
            PostCodeData entityPostcode =  foundList.get(0).buildTransferObject();;
            entityPostcode.setStraat(joinedFirstNames);
            return entityPostcode;
        }
    }


    @Transactional
    public void addPayer(PayerData payerData) {
        Payer payer = TransferUtils.toPayer(payerData);
        payerRepo.save(payer);
    }

    @Transactional
    public void addTransaction(Long payerId, TransactieData transactieData, String webUserName){
        Payer payer = payerRepo.getOne(payerId);

        Transacties transactie = new Transacties();
        transactie.setPayer(payer);
        transactie.setAdminlocatie(transactieData.getAdminlocatie());
        transactie.setBedrag(transactieData.getBedrag());
        transactie.setBetaalmethode(transactieData.getBetaalmethode());
        transactie.setCategorie(transactieData.getCategorie());
        transactie.setWebuserid(webUserName);
        transactie.setScanfile(transactieData.getScanfile());

        Timestamp transactiedatum = new Timestamp(transactieData.getTransactiedatum().getTime());

        transactie.setTransactiedatum(transactiedatum);
        daoTransacties.add(transactie);

    }

    private AdresInfo fetchData(String postcode, int huisnummer, String toevoeging) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(url);

        List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("postal-code", postcode));
        postParameters.add(new BasicNameValuePair("number", String.valueOf(huisnummer)));
        postParameters.add(new BasicNameValuePair("addition-value", toevoeging));

        request.setEntity(new UrlEncodedFormEntity(postParameters));
        HttpResponse response = client.execute(request);

        String data = EntityUtils.toString(response.getEntity(), "UTF-8") ;

        if (data.contains("[]")) {
            return null;
        }

        String out = StringEscapeUtils.unescapeJava(data).substring(2);
        out = out.replace("City","city").
                replace("PostalCode","postalCode").
                replace("Street","street").
                replace("HouseNumber","houseNumber");



        int endStr = out.indexOf(",\"FormattedAddress\"");
        if (endStr < 0 ) {
            return null;
        }
        out = out.substring(0,endStr)+ "}";

        ObjectMapper om = new ObjectMapper();

        AdresInfo adres = om.readValue(out, AdresInfo.class);

        return adres;
    }


    @Transactional
    public void updatePayerAdress(PayerData payerData) {
        Payer payer = payerRepo.getOne(payerData.getId());
        payer.setAchternaam(payerData.getAchternaam());
        payer.setGeslacht(payerData.getGeslacht());
        payer.setVoornaam(payerData.getVoornaam());
        payer.setPostcode(payerData.getPostcode());
        payer.setWoonplaats(payerData.getWoonplaats());
        payer.setStraatnaam(payerData.getStraatnaam());
        payer.setNummer(payerData.getNummer());
        payer.setToevoeging(payerData.getToevoeging());
        payer.setTelefoon(payerData.getTelefoon().trim());
    }


    @Transactional
    public List<PayerData> findInYearAtLocation(int year, Long locationID, int currentIndex,  int maxPerPage){
        List<PayerData> result = new ArrayList<>();
        if (locationID == null) {
            return result;
        }
        List<Payer> daoList = payerRepo.payerVoorJaarList(locationID, year, PageRequest.of(currentIndex, maxPerPage));
        daoList.forEach(aPayer -> result.add(TransferUtils.fromPayer(aPayer)));
        return result;
    }


    @Transactional
    public void removePayer(PayerData editablePayer) {
        Payer payer = payerRepo.getOne(editablePayer.getId());
        if (payer != null) {
            daoTransacties.removePayerTransactions(payer);
            daoTransacties.flush();

            payerRepo.delete(payer);
            payerRepo.flush();
        }
    }


    public NewUploadFileInfo uploadFileFromScan(PayerData currentPayer, TransactieData transactieData) {
        NewUploadFileInfo newUploadFileInfo =
                new NewUploadFileInfo(conceptContractSettings.getDirScanFiles(), currentPayer, transactieData);
        return newUploadFileInfo;
    }

}
