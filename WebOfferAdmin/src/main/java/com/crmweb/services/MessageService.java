package com.crmweb.services;

import com.crmweb.reports.ConceptContractSettings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class MessageService {
    private static final String JAAR_BRIEF_BEZOEKER = "BezoeersTemplate.html";
    private static final String JAAR_BRIEF_BEDRIJF = "BedrijfsTemplate.html";
    private static final List<String> TAGS = Arrays.asList("Bezoeker", "Bedrijf");


    private Map<String, Path> tagToPathMap = new HashMap<>();


    @Autowired
    private ConceptContractSettings conceptContractSettings;

    @PostConstruct
    void init() {
        try {

            Path brief = Paths.get(conceptContractSettings.getDirScanFiles() + JAAR_BRIEF_BEZOEKER);
            tagToPathMap.put("Bezoeker", brief);
            if (!Files.exists(brief)) {
                String msg = StreamUtils.copyToString(new ClassPathResource(JAAR_BRIEF_BEZOEKER).getInputStream(), Charset.defaultCharset());
                Files.write(brief, msg.getBytes(),  StandardOpenOption.CREATE);
            }
            brief = Paths.get(conceptContractSettings.getDirScanFiles() + JAAR_BRIEF_BEDRIJF);
            tagToPathMap.put("Bedrijf", brief);
            if (!Files.exists(brief)) {
                String msg = StreamUtils.copyToString(new ClassPathResource(JAAR_BRIEF_BEDRIJF).getInputStream(), Charset.defaultCharset());
                Files.write(brief, msg.getBytes(),  StandardOpenOption.CREATE);
            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }


    public Map<String,String> fetchLetterMapping(){
        Map<String, String> result = new HashMap<>();
        tagToPathMap.forEach((key, value) -> {
            Path path = value;
            String template = fetchByPathLetter(value);
            result.put(key, template);
        });
        return result;
    }

    public String fetchByPathLetter(Path path){
         Stream<String> lines = null;
        try {
            lines = Files.lines(path);
            String data = lines.collect(Collectors.joining("\n"));
            lines.close();
            return data;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return "Error stopped the processing!!";

    }

    public String fetchLetterData(String tag){
        String file  = tag.equalsIgnoreCase(TAGS.get(0)) ? JAAR_BRIEF_BEZOEKER : JAAR_BRIEF_BEDRIJF;
        Path locatieBrief = Paths.get(conceptContractSettings.getDirScanFiles() + file);
        return fetchByPathLetter(locatieBrief);
    }

    public void updateBrief(String tag, String briefData) {
        try {
            String file  = tag.equalsIgnoreCase(TAGS.get(0)) ? JAAR_BRIEF_BEZOEKER : JAAR_BRIEF_BEDRIJF;
            Path locatieBrief = Paths.get(conceptContractSettings.getDirScanFiles() + file);
            Files.write(locatieBrief, briefData.getBytes(),  StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

    }

    public List<String> getTags(){
        return TAGS;
    }

    public Map<String, Path> getTagToPathMap() {
        return tagToPathMap;
    }

}
