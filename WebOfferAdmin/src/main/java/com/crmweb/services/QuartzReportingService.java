package com.crmweb.services;

import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.repo.LocatiesRepo;
import com.domain.ukgr.repo.ReportQuartzRepo;
import com.domain.ukgr.tables.Locaties;
import com.domain.ukgr.tables.ReportQuartz;
import com.shared.transfers.LocatieData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class QuartzReportingService {
    private static final Logger logger = LoggerFactory.getLogger(QuartzReportingService.class);

    @Autowired
    private ReportQuartzRepo reportQuartzRepo;

    @Autowired
    private LocatiesRepo locatiesRepo;

    @Autowired
    private TurboReportsService turboReportsService;

    @Transactional
    public void makeLocationReportCurrentYear(){
        Locaties locatie = getNextToStart(4);

        if (locatie != null) {
            LocatieData locatieData =  TransferUtils.fromLocaties(locatie);

            int reportYear = LocalDate.now().minusYears(1).getYear();
            if (turboReportsService.jobPageJaarNamedLocatie(locatieData, reportYear)){
                addQuartzDone(reportYear, locatie, 4);
            }
        }
    }

    /**
     * Vanaf 9 februari tot en met 28/29  tussen 1 uur en 8 uur am
     */
    private Locaties getNextToStart(int rtype){
        LocalDateTime today = LocalDateTime.now();

        if (today.getMonth().getValue() == 2  && today.getDayOfMonth() >= 9 &&
                today.getHour() > 0 &&  today.getHour() < 24 )  {
            int reportYear = LocalDate.now().minusYears(1).getYear();
            List<Locaties> alleLoc = locatiesRepo.allAtLocatieForYear(reportYear, rtype);
            return alleLoc.stream().findFirst().orElse(null);
        }

        return null;
    }

    @Transactional
    public void makeVisitorReportCurrentYear(){
        Locaties locatie = getNextToStart(5);

        if (locatie != null) {
            LocatieData locatieData =  TransferUtils.fromLocaties(locatie);

            int reportYear = LocalDate.now().minusYears(1).getYear();
            if (turboReportsService.jobVisitorForLocatie(locatieData, reportYear)){
                addQuartzDone(reportYear, locatie, 5);
            }
        }
    }


    @Transactional
    public void jobVisitorForLocatieSummary(){

        Locaties locatie = getNextToStart(6);

        if (locatie != null) {
            LocatieData locatieData =  TransferUtils.fromLocaties(locatie);

            int reportYear = LocalDate.now().minusYears(1).getYear();
            if (turboReportsService.jobVisitorForLocatieSummary(locatieData, reportYear)){
                addQuartzDone(reportYear, locatie, 6);
            }
        }
    }

    private void addQuartzDone(int reportYear,Locaties locatie, int rType ){
        ReportQuartz reportQuartz = new ReportQuartz();
        reportQuartz.setReportItemId(locatie.getId());
        reportQuartz.setReportLocation(locatie.getUkgrcode());
        reportQuartz.setReportType(rType);
        reportQuartz.setReportYear(reportYear);
        reportQuartz.setReportCreated(LocalDateTime.now());
        reportQuartzRepo.save(reportQuartz);

        logger.info("Added To Reports ReportType = {};  Ukgr Code = {}; Year = {}", rType, locatie.getUkgrcode(), reportYear);
    }

}
