package com.crmweb.services;

import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.repo.LocatiesRepo;
import com.domain.ukgr.tables.Locaties;
import com.shared.transfers.LocatieData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class LocatiesService {
    @Autowired
    private LocatiesRepo locatiesRepo;

    public List<LocatieData> findAll(){
        List<Locaties> result = locatiesRepo.receiveOrderedWoonpl();
        List<LocatieData> transforms = new ArrayList<>();
        for(Locaties loc: result) {
            transforms.add(TransferUtils.fromLocaties(loc));
        }
        return transforms;
    }


    public List<LocatieData> findAllUkgrCode(){
        List<Locaties> result = locatiesRepo.receiveOrderedUkgrCode();
        List<LocatieData> transforms = new ArrayList<>();
        for(Locaties loc: result) {
            transforms.add(TransferUtils.fromLocaties(loc));
        }
        return transforms;
    }

    @Transactional
    public void saveNewLocatie(LocatieData newLocatie) {
        Locaties newloc =  TransferUtils.toLocaties(newLocatie);
        locatiesRepo.save(newloc);
    }


    @Transactional
    public void updateLocatie(LocatieData editLocatie) {
        Locaties locUpdate = locatiesRepo.getOne(editLocatie.getId());
        locUpdate.setNummer(editLocatie.getNummer());
        locUpdate.setToevoeging(editLocatie.getToevoeging());
        locUpdate.setPostcode(editLocatie.getPostcode());
        locUpdate.setStraatnaam(editLocatie.getStraatnaam());
        locUpdate.setWoonplaats(editLocatie.getWoonplaats());
        locUpdate.setUkgrcode(editLocatie.getUkgrcode());
        locUpdate.setTelefoon(editLocatie.getTelefoon());
    }

    @Transactional
    public LocatieData findUkgrCodeLocatie(Integer ukgrcode,Long  locatieId){
        Locaties logUkgrcode = locatiesRepo.ukgrCodeUsedByOther(locatieId, ukgrcode);
        return logUkgrcode != null ? TransferUtils.fromLocaties(logUkgrcode) : null;
    }

    @Transactional
    public LocatieData findLocatie(Long currentLocatie) {
        Locaties logUkgrcode = locatiesRepo.getOne(currentLocatie);
        return logUkgrcode != null ? TransferUtils.fromLocaties(logUkgrcode) : null;
    }
}
