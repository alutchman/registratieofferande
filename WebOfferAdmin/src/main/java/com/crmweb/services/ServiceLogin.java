package com.crmweb.services;

import com.crmweb.utils.SessionWallet;
import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.repo.WebuserRepo;
import com.domain.ukgr.tables.Webuser;
import com.shared.transfers.SearchUser;
import com.shared.transfers.UserLevel;
import com.shared.transfers.WebUserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class ServiceLogin {
    private static final Logger logger = LoggerFactory.getLogger(ServiceLogin.class);
    private static final String MAIN_ADMIN = "admin";
    private static final String INIT_PW = "welkom01";


    @Autowired
    private WebuserRepo webuserRepo;

    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        ensureAdmin();
    }

    @Transactional
    public boolean login(String username, String password, HttpServletRequest request,HttpSession session ) {
        Webuser entity = webuserRepo.findWebuserByUseridAndWachtwoord(username, createHashData(password));
        if (entity != null) {
            WebUserData webUserData = TransferUtils.fromWebuser(entity);
            session.invalidate();
            HttpSession newSession = request.getSession();
            newSession.setAttribute(WebUserData.class.getSimpleName(),webUserData);
            newSession.setAttribute(SessionWallet.class.getSimpleName(),new SessionWallet());
            return true;
        } else {
            return false;
        }
    }


    @Transactional
    public boolean wachtwoordNaInloggen(String oldPassword, String newPassword, WebUserData alterUser){
        Webuser entity;
        if ( (oldPassword == null || oldPassword.trim().length() == 0 ) &&
             (newPassword == null || newPassword.trim().length() == 0 )  ){
            entity = webuserRepo.findWebuserByUserid(alterUser.getUserid());
            if (entity != null) {
                entity.setVoornaam(alterUser.getVoornaam());
                entity.setAchternaam(alterUser.getAchternaam());
            }
        } else {
            entity = webuserRepo.findWebuserByUseridAndWachtwoord(alterUser.getUserid(),createHashData(oldPassword.trim()));
            if (entity != null) {
                entity.setVoornaam(alterUser.getVoornaam());
                entity.setAchternaam(alterUser.getAchternaam());
                entity.setWachtwoord(createHashData(newPassword.trim()));
            }
        }
        return entity != null;
    }

    @Transactional
    public void ensureAdmin()  {
        logger.info("Admin registation.....");
        Webuser entity =  webuserRepo.findWebuserByUserid(MAIN_ADMIN);
        if (entity == null) {
            Webuser webuser = new Webuser();
            webuser.setUserid(MAIN_ADMIN);
            webuser.setGroupnaam(UserLevel.BEHEER.name());
            webuser.setAchternaam("Applicatie");
            webuser.setVoornaam("Beheer");
            webuser.setWachtwoord(createHashData(INIT_PW));
            webuserRepo.save(webuser);
        }
    }

    private String createHashData(String input)  {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());

            byte byteData[] = md.digest();

            String digested = DatatypeConverter
                    .printHexBinary(byteData).toUpperCase();
            return digested;
        } catch (NoSuchAlgorithmException e) {

            return null;
        }
    }


    public List<WebUserData> haalGebruikersOp(SearchUser searchUser, String excudeUserId) {
        List<Webuser> results = webuserRepo.searchContainNotUserID(excudeUserId, searchUser.getVoornaam()+"%",
                searchUser.getAchternaam() + "%");
        List<WebUserData> exportList = new ArrayList<>();
        for (Webuser entity : results) {
            exportList.add(TransferUtils.fromWebuser(entity));
        }

        return exportList;
    }

    @Transactional
    public void updateUser(WebUserData currentUser) {
        Webuser entity = webuserRepo.findWebuserByUserid(currentUser.getUserid());
        if (entity != null) {
            entity.setAchternaam(currentUser.getAchternaam());
            entity.setVoornaam(currentUser.getVoornaam());
            entity.setGroupnaam(currentUser.getCurrentlevel().name());
        }
    }

    public boolean isUserIdFree(WebUserData newUser) {
        Webuser entity = webuserRepo.findWebuserByUserid(newUser.getUserid());
        return entity == null;
    }

    @Transactional
    public void addNewUser(WebUserData newUser) {
        Webuser webuser = new Webuser();
        webuser.setUserid(newUser.getUserid());
        webuser.setGroupnaam(newUser.getCurrentlevel().name());
        webuser.setAchternaam(newUser.getAchternaam());
        webuser.setVoornaam(newUser.getVoornaam());
        webuser.setWachtwoord(createHashData(INIT_PW));
        webuserRepo.save(webuser);
    }
}
