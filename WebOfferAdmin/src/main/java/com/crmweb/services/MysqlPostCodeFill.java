package com.crmweb.services;

import java.io.*;
import java.sql.*;

public class MysqlPostCodeFill {

    public static void fillTable(String host,String user, String password){
        File file = new File("/home/alutchman/Downloads/postcodetabelX.csv");
        BufferedReader br = null ;
        Connection con = null;
        int linesHandled = 0;
        int recordsinserted = 0;
        String errorLines = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection(
                    "jdbc:mysql://"+ host+":3306/ukgradmin",user,password);

            br = new BufferedReader(new FileReader(file));

            String line;

            while ((line = br.readLine()) != null) {

                Statement stmt=con.createStatement();
                try {
                    linesHandled++;
                    if (!line.contains("Postbus")) {
                        stmt.execute("INSERT IGNORE INTO postcodes VALUES ("+ line +")");
                        recordsinserted++;
                    }
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                    errorLines += e.getMessage() + "\n";
                }
                if (linesHandled % 100 == 0) {
                    System.out.println(String.format("Lines handels = %d. Recordes inserted = %d", linesHandled, recordsinserted));
                    System.out.println("LastInsert: " + line);
                    System.out.println("errorLines: " + errorLines);
                    errorLines = "";
                }
            }
            System.out.println(String.format("Lines handels = %d. Recordes inserted = %d", linesHandled, recordsinserted));
            System.out.println("LastInsert: " + line);
            System.out.println("errorLines: " + errorLines);
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            try {
                if (con != null) con.close();
                if (br != null) br.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args){
        fillTable("127.0.0.1", "xxxx", "xxxx");
    }
}
