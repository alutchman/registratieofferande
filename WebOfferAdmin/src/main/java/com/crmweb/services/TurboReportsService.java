package com.crmweb.services;

import com.crmweb.reports.ConceptContractSettings;
import com.crmweb.reports.HeaderFooterPdf;
import com.crmweb.reports.PayerPathsData;
import com.crmweb.reports.PdfBytesProducer;
import com.crmweb.reports.PdfMarginValues;
import com.crmweb.reports.html.TransactPersonPageInfo;
import com.crmweb.utils.MaandTransacties;
import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.dao.DaoTransacties;
import com.domain.ukgr.repo.JaarPerMethodRepo;
import com.domain.ukgr.repo.PayerRepo;
import com.domain.ukgr.tables.JaarPerMethod;
import com.domain.ukgr.tables.MaandPerMethod;
import com.domain.ukgr.tables.Payer;
import com.domain.ukgr.tables.Transacties;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.shared.transfers.LocatieData;
import com.shared.transfers.PayerData;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class TurboReportsService {
    private static final Logger logger = LoggerFactory.getLogger(TurboReportsService.class);
    private static final int MAX_SET = 40;

    @Autowired
    private PdfBytesProducer pdfBytesProducer;

    @Autowired
    private ReportingService reportingService;

    @Autowired
    private ConceptContractSettings conceptContractSettings;

    @Autowired
    private PayerRepo payerRepo;

    @Autowired
    private DaoTransacties<Transacties> daoTransacties;

    @Autowired
    private JaarPerMethodRepo jaarPerMethodRepo;

    @Autowired
    private MessageService messageService;


    public boolean jobPageJaarNamedLocatie(LocatieData locatiedata, int taxYear) {
        String filename = getRealReportFile(locatiedata.getUkgrcode(), taxYear, 4, true);

        if (!Files.exists(Paths.get(filename))) {
            List<Path> payerPagePaths = createPageJaarNamedLocatie(locatiedata, taxYear);
            try {
                parseDownloadDataByPaths(payerPagePaths, locatiedata, taxYear, 4);
                return true;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                return false;
            }
        }
        return true;
    }


    public boolean jobVisitorForLocatie(LocatieData locatiedata, int taxYear) {
        String filename = getRealReportFile(locatiedata.getUkgrcode(), taxYear, 5, true);

        if (!Files.exists(Paths.get(filename))) {
            List<Path> payerPagePaths = createVisitorPageJaarNamedLocatie(locatiedata, taxYear);
            try {
                parseDownloadDataByPaths(payerPagePaths, locatiedata, taxYear, 5);
                return true;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                return false;
            }
        }
        return true;
    }

    public boolean jobVisitorForLocatieSummary(LocatieData locatiedata, int taxYear) {

        String filename = getRealReportFile(locatiedata.getUkgrcode(), taxYear, 6, true);

        if (!Files.exists(Paths.get(filename))) {
            List<Path> payerPagePaths = fetchVisitorForLocatieSummary(locatiedata, taxYear);
            try {
                parseDownloadDataByPaths(payerPagePaths, locatiedata, taxYear, 6);
                return true;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                return false;
            }
        }
        return true;

    }


    public List<Path> createVisitorPageJaarNamedLocatie(LocatieData locatiedata, int taxYear) {
        List<Payer> payerDataList = payerRepo.payerVoorJaarList(locatiedata.getId(), taxYear, PageRequest.of(0, 999999999));
        List<PayerPathsData> payerPagePathsMulti = new ArrayList<>();

        String signed = reportingService.getSignatureForLetter();
        Map<String,String> template = messageService.fetchLetterMapping();

        for (Payer payer : payerDataList) {
            PayerData payerData = TransferUtils.fromPayer(payer);
            PayerPathsData payerPathsData = fetchForPayerAtyear(locatiedata, taxYear, payerData, signed, true, template);
            payerPagePathsMulti.add(payerPathsData);
        }

        List<Path> resultPaths = getPdfPathsFromHtmlConversion(payerPagePathsMulti);

        return resultPaths;
    }

    private List<Path> getPdfPathsFromHtmlConversion(List<PayerPathsData> payerPagePathsMulti) {
        List<Path> resultPaths = new ArrayList<>();

        try {
            for (PayerPathsData payerPathsData : payerPagePathsMulti) {
                Path pdfFilePath = createPdfFromVisitorPathByPage(payerPathsData);
                resultPaths.add(pdfFilePath);
            }

            return resultPaths;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return new ArrayList<>();
    }


    public List<Path> fetchVisitorForLocatieSummary(LocatieData locatiedata, int taxYear) {
        List<Payer> payerDataList = payerRepo.payerVoorJaarList(locatiedata.getId(), taxYear, PageRequest.of(0, 999999999));
        List<PayerPathsData> payerPagePathsMulti = new ArrayList<>();

        for (Payer payer : payerDataList) {
            PayerData payerData = TransferUtils.fromPayer(payer);

            List<MaandTransacties> maandoverzicht = reportingService.vulMaandOverzicht(locatiedata.getId(), payerData.getId(), taxYear);
            List<JaarPerMethod> jaaroverzicht = jaarPerMethodRepo.findByMethodPayerJaarPerJaar(payerData.getId(), taxYear, locatiedata.getId());
            TransactPersonPageInfo transactPersonPageInfo = new TransactPersonPageInfo(payerData, taxYear, jaaroverzicht, maandoverzicht);

            try {
                Path htmlPath = transactPersonPageInfo.parseSummary();
                PayerPathsData payerPathsData = new PayerPathsData(payerData);
                payerPathsData.getPayerPagePaths().add(htmlPath);
                payerPagePathsMulti.add(payerPathsData);
            } catch (IOException e) {

            }
        }
        List<Path> resultPaths = getPdfPathsFromHtmlConversion(payerPagePathsMulti);
        return resultPaths;
    }

    public Path createVisitorSummaryBd(PayerData payerData, LocatieData locatiedata, int taxYear) {
        List<MaandTransacties> maandoverzicht = reportingService.vulMaandOverzicht(locatiedata.getId(), payerData.getId(), taxYear);
        List<JaarPerMethod> jaaroverzicht = jaarPerMethodRepo.findByMethodPayerJaarPerJaar(payerData.getId(), taxYear, locatiedata.getId());
        TransactPersonPageInfo transactPersonPageInfo = new TransactPersonPageInfo(payerData, taxYear, jaaroverzicht, maandoverzicht);

        try {
            Path htmlPath = transactPersonPageInfo.parseSummary();
            PayerPathsData payerPathsData = new PayerPathsData(payerData);
            payerPathsData.getPayerPagePaths().add(htmlPath);
            Path pdfFilePath = createPdfFromVisitorPathByPage(payerPathsData);
            return pdfFilePath;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }


    public Path createVisitorPageJaarNamedLocatie(PayerData payer, LocatieData locatiedata, int taxYear) {
        String signed = reportingService.getSignatureForLetter();
        Map<String,String> template = messageService.fetchLetterMapping();
        PayerPathsData payerPathsData = fetchForPayerAtyear(locatiedata, taxYear, payer, signed, true, template);

        try {
            Path pdfFilePath = createPdfFromVisitorPathByPage(payerPathsData);
            return pdfFilePath;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return null;

    }

    public List<Path> createPageJaarNamedLocatie(LocatieData locatiedata, int taxYear) {
        List<Payer> payerDataList = payerRepo.payerVoorJaarList(locatiedata.getId(), taxYear, PageRequest.of(0, 999999999));
        List<Path> payerPagePaths = new ArrayList<>();

        for (Payer payer : payerDataList) {
            PayerData payerData = TransferUtils.fromPayer(payer);
            PayerPathsData payerPathsData = fetchForPayerAtyear(locatiedata, taxYear, payerData, null, false, null);
            payerPagePaths.addAll(payerPathsData.getPayerPagePaths());
        }
        try {
            return createPdfFromPathByPage(payerPagePaths, locatiedata, taxYear, 4);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return new ArrayList<>();
    }


    public Path createPdfFromVisitorPathByPage(PayerPathsData payerPathsData) throws IOException {
        PayerData payerData = payerPathsData.getPayerData();
        List<Path> pathsSub = payerPathsData.getPayerPagePaths();

        Path basePath = Paths.get("/tmp");
        Path pdfFilePath = basePath.resolve(payerData.getUniqueName("pdf"));
        fillHtmlDataInPdf(pdfFilePath, pathsSub);
        return pdfFilePath;
    }


    public List<Path> createPdfFromPathByPage(List<Path> paths, LocatieData locatiedata, int taxYear, int rtype) throws IOException {
        List<Path> pathsPdf = new ArrayList<>();

        Path basePath = initAndReturnBasePath(taxYear, locatiedata.getUkgrcode());

        int sets = paths.size() / MAX_SET;

        for (int i = 0; i <= sets; i++) {
            int startIndex = i * MAX_SET;
            int endIndex = startIndex + MAX_SET;

            if (endIndex >= paths.size()) {
                endIndex = paths.size();
            }
            if (startIndex >= paths.size()) {
                break;
            }

            List<Path> pathsSub = paths.subList(startIndex, endIndex);
            String stampName = String.format("rtype%02d_page%02d_ukgrcode%02d_%d", rtype, i + 1,
                    locatiedata.getUkgrcode(), taxYear);
            Path pdfFilePath = basePath.resolve(stampName + ".pdf");
            pathsPdf.add(pdfFilePath);

            fillHtmlDataInPdf(pdfFilePath, pathsSub);
        }
        return pathsPdf;
    }


    private void fillHtmlDataInPdf(Path pdfFilePath, List<Path> pathsSub) {
        try (OutputStream outputStream = Files.newOutputStream(pdfFilePath)) {
            writePdfFromPaths(outputStream, pathsSub, new PdfMarginValues(95f, 40f, 90f, 50f), null);

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }


    public void writePdfFromPaths(OutputStream outputStream, List<Path> paths, PdfMarginValues pdfMarginValues, Map<String, String> params) {
        Document document = new Document(PageSize.A4, pdfMarginValues.getLeft(), pdfMarginValues.getRight(),
                pdfMarginValues.getTop(), pdfMarginValues.getBottom());

        try {
            Pair<PdfWriter, CssResolverPipeline> result = pdfBytesProducer.initDocument(document, outputStream, params);

            result.getKey().setPageEvent(new HeaderFooterPdf(paths.size()));

            // XML Worker
            XMLWorker worker = new XMLWorker(result.getValue(), true);
            XMLParser p = new XMLParser(worker);

            for (Path aPath : paths) {
                document.newPage();
                p.parse(Files.newInputStream(aPath));
                Files.delete(aPath);
            }
            document.newPage();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            document.close();
        }
    }


    public String getRealReportFile(int ukgrCode, int taxYear, int rtypr, boolean isZip) {
        String baseUkgrMap = initBaseUkgrMap(taxYear, ukgrCode);
        String extensie = isZip ? "zip" : "pdf";

        String filename = String.format("%s/rtype%02d_ukgrcode%02d_%d.%s", baseUkgrMap, rtypr,
                ukgrCode, taxYear, extensie);
        return filename;
    }


    public String parseDownloadDataByPaths(List<Path> tmpPaths, LocatieData locatiedata, int taxYear, int rtype) throws IOException {
        String baseUkgrMap = initBaseUkgrMap(taxYear, locatiedata.getUkgrcode());
        initAndReturnBasePath(baseUkgrMap);

        String filename = getRealReportFile(locatiedata.getUkgrcode(), taxYear, rtype, true);

        try (OutputStream responseOutputStream = new FileOutputStream(filename);
             ZipOutputStream zos = new ZipOutputStream(responseOutputStream)
        ) {
            int index = 1;
            for (Path aPath : tmpPaths) {
                try (InputStream fileInputStream = new FileInputStream(aPath.toFile())) {
                    String pdfFileName;
                    if (rtype != 4) {
                        pdfFileName = aPath.getFileName().toString();

                    } else {
                        pdfFileName = String.format("set%03d_ukgrcode%02d_%d.pdf", index++,
                                locatiedata.getUkgrcode(), taxYear);
                    }

                    zos.putNextEntry(new ZipEntry(pdfFileName));

                    TransferUtils.handleStreamExchange(fileInputStream, zos);
                    zos.closeEntry();
                } catch (IOException e) {
                    logger.error(aPath.getFileName().toString(), e);
                }
            }
            responseOutputStream.flush();
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            tmpPaths.forEach(item -> {
                try {
                    Files.delete(item);
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            });
        }
        return filename;
    }


    private String initBaseUkgrMap(int taxYear, int ukgrCode) {
        String pdfPathUkgr = String.format("%d/UKGR%03d", taxYear, ukgrCode);
        return conceptContractSettings.getDirScanFiles() + pdfPathUkgr;
    }


    private Path initAndReturnBasePath(int taxYear, int ukgrCode) throws IOException {
        String baseUkgrMap = initBaseUkgrMap(taxYear, ukgrCode);
        Path basePath = Paths.get(baseUkgrMap);
        Files.createDirectories(basePath);
        return basePath;
    }

    private Path initAndReturnBasePath(String baseUkgrMap) throws IOException {
        Path basePath = Paths.get(baseUkgrMap);
        Files.createDirectories(basePath);
        return basePath;
    }


    private PayerPathsData fetchForPayerAtyear(LocatieData locatiedata, int taxYear, PayerData payerData,
                                               String signed, boolean withLetter, Map<String,String>  template) {
        int maxPerPage = 76;

        PayerPathsData payerPathsData = new PayerPathsData(payerData);

        int aantal = daoTransacties.findTransactionsInYear(payerData.getId(), taxYear, locatiedata.getId());
        int pages = TransferUtils.calculatePages(aantal, maxPerPage);

        List<JaarPerMethod> jaaroverzicht = jaarPerMethodRepo.findByMethodPayerJaarPerJaar(payerData.getId(), taxYear, locatiedata.getId());
        List<JaarPerMethod> setJaaroverzichtNoCash = jaarPerMethodRepo.findByMethodPayerJaarPerJaarExclContant(
                payerData.getId(), taxYear, locatiedata.getId(), "CONTANT");

        TransactPersonPageInfo transactPersonPageInfo = new TransactPersonPageInfo(payerData, taxYear, jaaroverzicht);
        transactPersonPageInfo.setJaaroverzichtNoCash(setJaaroverzichtNoCash);

        for (int index = 0; index < pages; index++) {
            transactPersonPageInfo.addTansactieList(reportingService.getPayerTransacties(locatiedata.getId(), payerData.getId(),
                    taxYear, index, maxPerPage));
        }

        try {
            String briefText = null;
            if (withLetter) {
                briefText = transactPersonPageInfo.createLetter(conceptContractSettings, locatiedata, taxYear, signed, template);
            }
            payerPathsData.addAll(transactPersonPageInfo.parseTransactions(briefText));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return payerPathsData;
    }

}
