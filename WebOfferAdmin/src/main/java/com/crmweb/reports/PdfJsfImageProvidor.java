package com.crmweb.reports;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.text.pdf.codec.PngImage;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class PdfJsfImageProvidor extends AbstractImageProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfJsfImageProvidor.class);

    private final String rootPath;

    private final String context;


    public PdfJsfImageProvidor(String rootPath,String context) {
        this.rootPath = rootPath;
        this.context = context;
    }

    @Override
    public Image retrieve(String src) {
        try {
            int pos = src.indexOf("base64,");
            if (src.startsWith("data") && pos > 0) {
                byte[] img = Base64.decode(src.substring(pos + 7));
                return src.startsWith("data:image/png") ? PngImage.getImage(img) : Image.getInstance(img);
            }
        } catch (Exception ex) {

        }

        if (!src.startsWith("/var/www/")) {
            src = getImageRootPath() + src;
            LOGGER.info(" src={}", src);
        } else {
            File file = new File(src);
            if (!file.exists() && src.endsWith("signature.png")) {
                src = getImageRootPath() + context + "/javax.faces.resource/" + "signature.png.xhtml?ln=images";
                LOGGER.error("No Signature set src={}", src);//javax.faces.resource
            }
        }

        try {
            return Image.getInstance(src);
        } catch (BadElementException | IOException ex) {
            LOGGER.error(ex.getMessage());
            return null;
        }
    }

    @Override
    public String getImageRootPath() {
        return rootPath;
    }

    public String getContext() {
        return context;
    }
}
