package com.crmweb.reports;

import com.shared.transfers.PayerData;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class PayerPathsData {
    private final PayerData payerData;
    private final List<Path> payerPagePaths = new ArrayList<>();

    public PayerPathsData(PayerData payerData) {
        this.payerData = payerData;

    }

    public void addAll(List<Path> paths) {
        payerPagePaths.addAll(paths);
    }

    public PayerData getPayerData() {
        return payerData;
    }

    public List<Path> getPayerPagePaths() {
        return payerPagePaths;
    }
}
