package com.crmweb.reports.html;

import com.crmweb.reports.ConceptContractSettings;
import com.crmweb.utils.MaandTransacties;
import com.domain.ukgr.tables.JaarPerMethod;
import com.shared.transfers.LocatieData;
import com.shared.transfers.PayerData;
import com.shared.transfers.TransactieData;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TransactPersonPageInfo implements Serializable {
    private static final long serialVersionUID = -4357483840175018146L;

    private final DateFormat outDateFormatter = new SimpleDateFormat("dd-MM-yyyy");

    private final PayerData payerData;
    private final int taxyear;
    ;
    private final List<List<TransactieData>> transactionPageList = new ArrayList<>();

    private final List<JaarPerMethod> jaaroverzicht;
    private List<JaarPerMethod> jaaroverzichtNoCash;
    private final String header;
    private final List<MaandTransacties> maandoverzicht;

    public TransactPersonPageInfo(PayerData payerData, int taxyear, List<JaarPerMethod> jaaroverzicht) {
        this(payerData, taxyear, jaaroverzicht, null);
    }


    public TransactPersonPageInfo(PayerData payerData, int taxyear, List<JaarPerMethod> jaaroverzicht, List<MaandTransacties> maandoverzicht) {
        this.taxyear = taxyear;
        this.payerData = payerData;
        this.jaaroverzicht = jaaroverzicht;
        this.header = "    <table class=\"header\">\n" +
                "        <tr>\n" +
                "            <td style=\"text-align: left;\">JaarOverzicht " + taxyear + "</td>\n" +
                "        </tr>\n" +
                "    </table>";
        if (maandoverzicht != null) {
            this.maandoverzicht = maandoverzicht;
        } else {
            this.maandoverzicht = new ArrayList<>();
        }
    }

    public int getTaxyear() {
        return taxyear;
    }

    public PayerData getPayerData() {
        return payerData;
    }

    public List<List<TransactieData>> getTransactionPageList() {
        return transactionPageList;
    }


    public void addTansactieList(List<TransactieData> indexList) {
        transactionPageList.add(indexList);
    }

    public int fetchPageCount() {
        return transactionPageList.size();
    }

    public List<JaarPerMethod> getJaaroverzicht() {
        return jaaroverzicht;
    }


    public String createPersonInfo() {
        String label = "";
        String invulling = "";
        if (payerData.getGeslacht().equalsIgnoreCase("B")) {
            label = "Bedrijf";
            invulling = payerData.getAchternaam();
        } else {
            label = "Naam";
            invulling = payerData.getVoornaam() + " " + payerData.getAchternaam();;

        }
            String basePersonInfo = "    <div style=\"margin-top:2cm;\">\n" +
                "        <div class=\"naamdata\">\n" +
                "            <div class=\"labelu\" >#label:</div>\n" +
                "            <div class=\"valueu\" style=\"clear: both;\">#invulling</div>\n" +
                "        </div>\n" +
                "        <div  class=\"naamdata\">\n" +
                "            <div  class=\"labelu\" >Adres:</div>\n" +
                "            <div class=\"valueu\" style=\"clear: both;\"> #straatnaam #nummer #toevoeging</div>\n" +
                "        </div>\n" +
                "        <div class=\"naamdata\">\n" +
                "            <div  class=\"labelu\" >Woonplaats:</div>\n" +
                "            <div class=\"valueu\">#postcode #woonplaats</div>\n" +
                "        </div>\n" +
                "        <div class=\"naamdata\">\n" +
                "            <div  class=\"labelu\" >Telefoon:</div>\n" +
                "            <div class=\"valueu\" >#telefoon</div>\n" +
                "        </div>\n" +
                "        <div class=\"naamdata\">\n" +
                "            <div  class=\"labelu\" >&#160;</div>\n" +
                "            <div class=\"valueu\" >&#160;</div>\n" +
                "        </div>\n" +
                "    </div>";

        return basePersonInfo.replaceAll("#toevoeging",
                payerData.getToevoeging() != null ? payerData.getToevoeging() : "")
                .replaceAll("#label", label)
                .replaceAll("#invulling", invulling)
                .replaceAll("#straatnaam", payerData.getStraatnaam())
                .replaceAll("#nummer", String.valueOf(payerData.getNummer()))
                .replaceAll("#woonplaats", payerData.getWoonplaats())
                .replaceAll("#postcode", payerData.getPostcode())
                .replaceAll("#telefoon", payerData.getTelefoon());


    }

    public String createLetter(ConceptContractSettings settings, LocatieData locatiedata, int taxYear, String signed, Map<String,String> templateMap) {
        String datum = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        double totaal = jaaroverzichtNoCash.stream().mapToDouble(JaarPerMethod::getTotaal).sum();
        String received = String.format("%.2f", totaal).replace(".", ",");

        String director = settings.getDirector();

        String aanhef = payerData.getGeslacht().equalsIgnoreCase("M") ? "Dhr." : "Mw.";
        String aanroep = payerData.getGeslacht().equalsIgnoreCase("M") ? "Meneer" : "Mevrouw";
        String fullname = payerData.getFullName();

        boolean bedrijf = payerData.getGeslacht().equalsIgnoreCase("B");

        if (payerData.getGeslacht().equalsIgnoreCase("B")) {
            aanhef = "Bedrijf";
            aanroep = "Bedrijf";
            fullname = payerData.getAchternaam();
        }

        String geachteTxt = bedrijf ?    fullname  : String.format("%s %s", aanroep, fullname);
        String titleText = String.format("%s %s", aanhef, fullname);
        String plaatsDatum = String.format("%s %s", locatiedata.getWoonplaats(), datum);
        String address = String.format("%s %d%s", payerData.getStraatnaam(), payerData.getNummer(), payerData.getToevoeging());
        String postCodeWoonpl = String.format("%s %s", payerData.getPostcode().toUpperCase(),
                StringUtils.capitalize(payerData.getWoonplaats()));

        String signedImg = "";
        if (signed != null) {
            signedImg = String.format(" <img src=\"%s\" height=\"80px\" />", signed);
        }

        Locale locale = new Locale("nl", "NL");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        String receivedEuro = currencyFormatter.format(totaal);

        String template = bedrijf ? templateMap.get("Bedrijf") : templateMap.get("Bezoeker");

        String updatedTEmplate = template.replace("#{geachteTxt}", geachteTxt);
        updatedTEmplate = updatedTEmplate.replace("#{bedag}", receivedEuro);
        updatedTEmplate = updatedTEmplate.replace("#{jaar}", String.valueOf(taxYear));


        String letterData = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <title>Jaaroverzicht</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<table class=\"headerLetter\">\n" +
                "    <tr>\n" +
                "        <td style=\"text-align: left;color:navy;\">Jaaroverzicht " + taxYear + "</td>\n" +
                "    </tr>\n" +
                "</table>\n" +
                "<div style=\"display:block;overflow:hidden;padding-bottom:2cm;\">\n<br />\n" +
                "    <span>" + titleText + "</span> <br />\n" +
                "    <span>" + address + "</span> <br />\n" +
                "    <span>" + postCodeWoonpl + "</span><br />\n" +
                "</div>"+
                "<div style=\"display:block;overflow:hidden;padding-bottom:1cm;\">\n" +
                "    <span>" + plaatsDatum + "</span><br />\n" +
                "     <br />\n<br />\n<br />\n<br />\n" +
                updatedTEmplate +
                "\n" +
                "     <br />\n <br />\n <br />\n<br />\n" +
                "\n" +
                "  " + signedImg +
                "\n" +
                "     <br />\n" +
                "     <br />\n" +
                "\n" +
                "    <span>Namens de UKGR</span><br />\n" +
                "    (Contactpersoon : " + director + ")\n" +
                "\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";

        return letterData;
    }

    public String createJaarDataInfo() {
        String jaarDataInfo = "<div style=\"float:right;width: 4.1cm !important;margin-top:2cm;\">";
        double totaal = 0.0;
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());

        for (JaarPerMethod jaarItem : jaaroverzicht) {
            String waarde = currencyFormatter.format( jaarItem.getTotaal());
            jaarDataInfo += "<div class=\"summary\">\n";
            jaarDataInfo += "   <div class=\"labelu\" >" + jaarItem.getBetaalmethode() + "</div>";
            jaarDataInfo += "   <div class=\"valueu\" >" + waarde + "</div>";
            jaarDataInfo += "</div>\n";
            totaal += jaarItem.getTotaal();
        }
        String waarde = currencyFormatter.format( totaal);
        jaarDataInfo += "   <div class=\"summary hr\" >";
        jaarDataInfo += "       <div class=\"labelu\" ><strong>Totaal</strong></div>";
        jaarDataInfo += "      <div class=\"valueu\" >" + waarde + "</div>";
        jaarDataInfo += "   </div>";
        jaarDataInfo += "</div>";
        return jaarDataInfo;
    }

    public List<Path> parseTransactions(String letter) throws IOException {
        List<Path> pathPageTrx = new ArrayList<>();

        if (letter != null) {
            String stampName = "Tmp" + payerData.getId() + "letter";
            Path tempPath = Files.createTempFile(stampName, ".html");
            Files.write(tempPath, letter.getBytes());
            pathPageTrx.add(tempPath);
        }

        for (int i = 0; i < getTransactionPageList().size(); i++) {
            List<TransactieData> lstTransacties = getTransactionPageList().get(i);
            String baseHtml = header;
            if (i == 0) {

                baseHtml += "<div style=\"display:block;overflow:hidden;padding-bottom:2cm;\">\n" +
                        "        <div style=\"display: block;overflow:hidden;\">\n" +
                        "            <div style=\"float:left;width:7cm !important;\">\n" +
                        createPersonInfo() +
                        "            </div>\n" +
                        "            <div style=\"float:right;width:7cm !important;\">\n" +
                        createJaarDataInfo() +
                        "            </div>\n" +
                        "        </div>\n" + createDuoOuterTable(lstTransacties) +
                        "</div>";
            } else {
                baseHtml += "<div style=\"display:block;overflow:hidden;padding-bottom:2cm;\">\n" +
                        "        <div style=\"display: block;overflow:hidden;\">\n" +
                        "            <div style=\"float:left;width:7cm !important;\">\n" +
                        createPersonInfo() +
                        "            </div>\n" +
                        "            <div style=\"float:right;width:7cm !important;text-align: right;\">\n" +
                        "                <span style=\"font-size:14px;font-weight: bold;color:darkslateblue;\">Vervolg Overzicht</span>\n" +
                        "            </div>\n" +
                        "        </div>\n" + createDuoOuterTable(lstTransacties) +
                        "</div>";
            }
            String stampName = "Tmp" + payerData.getId() + "Page" + (i + 1) + "X";
            Path tempPath = Files.createTempFile(stampName, ".html");
            Files.write(tempPath, baseHtml.getBytes());
            pathPageTrx.add(tempPath);
        }
        return pathPageTrx;
    }

    public Path parseSummary() throws IOException {
        String baseHtml = header;
        baseHtml += "<div style=\"display:block;overflow:hidden;padding-bottom:2cm;\">\n" +
                "        <div style=\"display: block;overflow:hidden;\">\n" +
                "            <div style=\"float:left;width:7cm !important;\">\n" +
                createPersonInfo() +
                "            </div>\n" +
                "            <div style=\"float:right;width:7cm !important;\">\n" +
                createJaarDataInfo() +
                "            </div>\n" +
                "        </div>\n" +
                getSummaryTable() +
                "</div>";

        String stampName = "TmpBD" + payerData.getId();
        Path tempPath = Files.createTempFile(stampName, ".html");
        Files.write(tempPath, baseHtml.getBytes());

        return tempPath;
    }

    private String getSummaryTable(){
        String summary = "    &#160; <br />\n" +
                "    &#160; <br />\n" +
                "\n" +
                "    <table  align=\"center\" class=\"mndtransacties\" cellpadding=\"5px\" cellspacing=\"1px\" border=\"0\">\n" +
                "        <tr style=\"margin:0;padding:0;color:darkred;\" >\n" +
                "            <th style=\"text-align: left;\">Maand</th>\n" +
                "            <th style=\"text-align: right;\">Contant</th>\n" +
                "            <th style=\"text-align: right;\">Pin</th>\n" +
                "            <th style=\"text-align: right;\">Bank</th>\n" +
                "        </tr>\n";


        for(MaandTransacties maandItem : this.maandoverzicht) {
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
            String contant = maandItem.getContant() != null ? currencyFormatter.format( maandItem.getContant()) : " &#160;";
            String pin = maandItem.getPin() != null ? currencyFormatter.format( maandItem.getPin()) : " &#160;";
            String bank =  maandItem.getBank() != null ? currencyFormatter.format( maandItem.getBank()) : " &#160;";

            String innerData =
                    "<tr style=\"margin:0;padding:0;border-bottom: 1px solid cornflowerblue;\" >\n" +
                    "    <td style=\"text-align: left;\">" + maandItem.getMaand() + "</td>\n" +
                    "    <td style=\"text-align:right;\">" + contant +"</td>" +
                    "    <td style=\"text-align:right;\">" + pin +"</td>" +
                    "    <td style=\"text-align:right;\">" + bank + "</td>\n" +
                    "</tr>\n";
            summary += innerData;
        }
        summary += "    </table>\n";
        return summary;
    }

    public String createDuoOuterTable(List<TransactieData> lstTransacties) {
        int leftSize = (int) Math.ceil(((double) lstTransacties.size()) / 2);

        List<TransactieData> payerYearPageStart = new ArrayList<>();
        List<TransactieData> payerYearPageEnd = new ArrayList<>();

        int nextPosition = 0;
        for (int i = 0; i < lstTransacties.size(); i++) {
            payerYearPageStart.add(lstTransacties.get(i));
            nextPosition++;
            if (nextPosition >= leftSize) break;
        }
        for (int i = nextPosition; i < lstTransacties.size(); i++) {
            payerYearPageEnd.add(lstTransacties.get(i));
        }

        final StringBuilder leftData = new StringBuilder();
        payerYearPageStart.forEach(trxData -> leftData.append(createTeableLeft(trxData)));

        final StringBuilder rightData = new StringBuilder();
        payerYearPageEnd.forEach(trxData -> rightData.append(createTeableRight(trxData)));


        String duodata = "    <div style=\"display: inline-block;overflow:hidden;white-space: nowrap;\">\n" +
                "        <div style=\"float:left;width:7cm !important;\">\n" +
                leftData.toString() +
                "        </div>\n" +
                "        <div style=\"float:right;width:7cm !important;\">\n" +
                rightData.toString() +
                "        </div>\n" +
                "    </div>";
        return duodata;
    }

    public String createTeableLeft(TransactieData transactieData) {
        Date trxDatum = transactieData.getTransactiedatum();
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String bedrag = currencyFormatter.format( transactieData.getBedrag());

        String leftData = "  <div class=\"transacties\">\n" +
                "                <div class=\"li\">\n" + outDateFormatter.format(trxDatum) + " </div>\n" +
                "                <div class=\"li method\">" + transactieData.getBetaalmethode().name() + "</div>\n" +
                "                <div class=\"li amount\" style=\"text-align: right;\">\n" + bedrag + "</div>\n" +
                "            </div>";
        return leftData;
    }

    public String createTeableRight(TransactieData transactieData) {
        Date trxDatum = transactieData.getTransactiedatum();
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String waarde = currencyFormatter.format( transactieData.getBedrag());

        String leftData = "  <div class=\"transacties\">\n" +
                "                <div class=\"li right amount\" style=\"text-align: right;\">\n" + waarde + "</div>\n" +
                "                <div class=\"li right method\">" + transactieData.getBetaalmethode().name() + "</div>\n" +
                "                <div class=\"li right\">\n" + outDateFormatter.format(trxDatum) + " </div>\n" +
                "            </div>";
        return leftData;
    }

    public void setJaaroverzichtNoCash(List<JaarPerMethod> jaaroverzichtNoCash) {
        this.jaaroverzichtNoCash = jaaroverzichtNoCash;
    }
}
