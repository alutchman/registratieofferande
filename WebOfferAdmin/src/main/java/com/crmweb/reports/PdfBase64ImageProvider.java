package com.crmweb.reports;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.itextpdf.text.ImgWMF;
import com.itextpdf.text.Jpeg;
import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.text.pdf.codec.PngImage;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;

import java.io.IOException;

public class PdfBase64ImageProvider extends AbstractImageProvider {

    @Override
    public Image retrieve(String src) {
        int pos = src.indexOf("base64,");
        try {
            if (src.startsWith("data") && pos > 0) {
                byte[] img = Base64.decode(src.substring(pos + 7));

                if (src.startsWith("data:image/png")) {
                   return  PngImage.getImage(img);
                }
                if (src.startsWith("data:image/jpg")) {
                    return   new Jpeg(img);
                }
                return src.startsWith("data:image/wmf") ? new ImgWMF(img) : Image.getInstance(img);
            } else {
                return null;
            }
        } catch (BadElementException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public String getImageRootPath() {
        return null;
    }
}
