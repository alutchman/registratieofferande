package com.crmweb.reports;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

@PropertySource("classpath:report.properties")
@Component
public class ConceptContractSettings {

    @Value("${report.conceptContract.pagenumbers}")
    private int[] pageNumbers;

    @Value("${report.conceptContract.jspPage}")
    private String jspPage;


    @Value("${report.conceptContract.serverBaseUrl}")
    private String baseUrlUnformatted;

    @Value("${report.conceptContract.pdfTitle}")
    private String pdfTitle;

    @Value("${report.conceptContract.headerTitle}")
    private String headerTitle;

    @Value("${report.conceptContract.pdfKeywords}")
    private String pdfKeywords;


    @Value("${report.conceptContract.pdfAuthor}")
    private String pdfAuthor;

    @Value("${report.conceptContract.pdfCreator}")
    private String pdfCreator;


    @Value("${report.conceptContract.location}")
    private String location;

    @Value("${report.conceptContract.director}")
    private String director;

    @Value("${report.conceptContract.watermark}")
    private String watermark;


    @Value("${report.conceptContract.title}")
    private String pageTitle;

    @Value("${report.conceptContract.footer}")
    private String pageFooter;

    @Value("${audit.folder.scanfiles}")
    private String dirScanFiles;

    @Value("${base.url.tomcat}")
    private String baseUrl;

    @Value("${report.language}")
    private String language;

    @Value("${report.country}")
    private String country;


    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        Locale.setDefault(new Locale(language.trim().toLowerCase(), country.trim().toUpperCase()));
    }


    public int[] getPageNumbers() {
        return pageNumbers;
    }

    public String getJspPage() {
        return jspPage;
    }

    public String getBaseUrlUnformatted() {
        return baseUrlUnformatted;
    }

    public String getPdfTitle() {
        return pdfTitle;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public String getPdfKeywords() {
        return pdfKeywords;
    }

    public String getPdfAuthor() {
        return pdfAuthor;
    }

    public String getPdfCreator() {
        return pdfCreator;
    }

    public String getLocation() {
        return location;
    }

    public String getDirector() {
        return director;
    }

    public String getWatermark() {
        return watermark;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public String getPageFooter() {
        return pageFooter;
    }

    public String getDirScanFiles() {
        return dirScanFiles;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getLanguage() {
        return language;
    }

    public String getCountry() {
        return country;
    }
}
