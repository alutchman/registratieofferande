package com.crmweb.reports;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;


@Component
public class PdfBytesProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfBytesProducer.class);


    @Autowired
    private ConceptContractSettings conceptContractSettings;


    /**
     * Create one PDF from an array of HTML Strings
     * Each entry will be added to e new page...
     *
     * @param html
     * @param params
     * @return
     */
    public byte[] createPdfDataFromHtml(String[] html, Map<String, String> params) {
        byte[] bytes = null;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            writePdf(outputStream, html, new PdfMarginValues(95f, 40f, 90f, 50f), params);
            bytes = outputStream.toByteArray();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return bytes;
    }


    public Pair<PdfWriter, CssResolverPipeline> initDocument(Document document, OutputStream outputStream, Map<String, String> params) throws DocumentException {
        PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);
        // Pipelines
        document.open();

        // CSS
        CSSResolver cssResolver = new StyleAttrCSSResolver();
        InputStream csspathtest = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("pdfoverzicht.css");
        CssFile cssfiletest = XMLWorkerHelper.getCSS(csspathtest);
        cssResolver.addCss(cssfiletest);

        // HTML
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
        if (params != null) {
            htmlContext.setImageProvider(new PdfJsfImageProvidor(params.get("serverURL"), params.get("appContext")));
        } else {
            htmlContext.setImageProvider(new PdfBase64ImageProvider());
        }

        PdfWriterPipeline pdf = new PdfWriterPipeline(document, pdfWriter);
        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

        document.addTitle(conceptContractSettings.getPdfTitle());
        document.addCreationDate();
        document.addKeywords(conceptContractSettings.getPdfKeywords());
        document.addAuthor(conceptContractSettings.getPdfAuthor());
        document.addCreator(conceptContractSettings.getPdfCreator());

        Pair<PdfWriter, CssResolverPipeline> result = new Pair<>(pdfWriter, css);

        return result;
    }


    /**
     * Using an output Stream the PDF content is writeen for each HTML page
     *
     * @param outputStream
     * @param htmlData
     * @param pdfMarginValues
     * @param params
     */
    private void writePdf(OutputStream outputStream, String[] htmlData, PdfMarginValues pdfMarginValues, Map<String, String> params) {
        Document document = new Document(PageSize.A4, pdfMarginValues.getLeft(), pdfMarginValues.getRight(),
                pdfMarginValues.getTop(), pdfMarginValues.getBottom());

        try {
            Pair<PdfWriter, CssResolverPipeline> result = initDocument(document, outputStream, params);

            result.getKey().setPageEvent(new HeaderFooterPdf(htmlData.length));

            // XML Worker
            XMLWorker worker = new XMLWorker(result.getValue(), true);
            XMLParser p = new XMLParser(worker);
            for (int i = 0; i < htmlData.length; i++) {
                document.newPage();
                p.parse(new ByteArrayInputStream(htmlData[i].getBytes()));
            }

            document.newPage();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            document.close();
        }
    }

}
