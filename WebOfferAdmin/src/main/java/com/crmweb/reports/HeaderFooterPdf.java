package com.crmweb.reports;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import java.io.IOException;

public class HeaderFooterPdf extends PdfPageEventHelper {

    private Font FONT_WM = new Font(Font.FontFamily.HELVETICA, 55, Font.BOLD, new GrayColor(0.98f));

    public static final String cssFooter = "<style>\n" +
            "    div.footer {\n" +
            "       font-size: 6pt !important;\n" +
            "       width : 100% !important; \n"+
            "       text-align:center; \n"+
            "    }\n" +
            "    div.footer label {\n" +
            "       font-size: 6pt !important;\n" +
            "        padding-top: 0px;\n" +
            "        font-family: Arial;\n" +
            "    }\n" +
            "\n" +
            "</style>\n" ;



    public static final String FOOTER_HTML= cssFooter+ "<div class=\"footer\"   >\n" +
            "            <label style=\"text-align:center;\">- #CURRENTPAGE/#TOTALPAGES -</label>\n" +
            "    </div>";


    private int totalPages=1;


    public HeaderFooterPdf(int totalPages) throws IOException {
        super();
        this.totalPages = totalPages;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        printFooter(writer, document);
    }



    private void printFooter(PdfWriter writer, Document document) {
        try {
            final int currentPageNumber = writer.getCurrentPageNumber();
            String footer2Use = FOOTER_HTML.
                    replaceAll("#CURRENTPAGE", String.valueOf(currentPageNumber)).
                    replaceAll("#TOTALPAGES", String.valueOf(totalPages));
            ElementList footer = XMLWorkerHelper.parseToElementList(footer2Use, cssFooter);

            ColumnText ct = new ColumnText(writer.getDirectContent());
            Rectangle pageRect = document.getPageSize();

            Rectangle footRect = new Rectangle(
                    pageRect.getLeft()+document.leftMargin()+400f,
                    pageRect.getBottom()+document.bottomMargin()+90f,
                    pageRect.getRight()+400f,
                    pageRect.getBottom()+document.bottomMargin());

            footRect.setTop(pageRect.getBottom() - document.bottomMargin());
            footRect.setLeft(pageRect.getLeft() + document.leftMargin());
            footRect.setRight(pageRect.getRight()-document.rightMargin());

            ct.setSimpleColumn(footRect);
            for (Element e : footer) {
                ct.addElement(e);
            }
            ct.go();

        } catch (Exception de) {


        }
    }
}
