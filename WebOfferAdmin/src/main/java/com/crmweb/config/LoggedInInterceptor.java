package com.crmweb.config;

import com.shared.transfers.WebUserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoggedInInterceptor implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggedInInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/login.html";
        String rootUri  = request.getContextPath() + "/";

        boolean loggedIn = session != null && session.getAttribute(WebUserData.class.getSimpleName()) != null;
        boolean loginRequest = request.getRequestURI().equals(rootUri);

        if (!loggedIn && !loginRequest ) {
            try {
                response.sendRedirect(loginURI);
            } catch (IOException e) {
                LOGGER.error("Redirect mislukt...",e);
            }
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
