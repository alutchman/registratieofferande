package com.crmweb.jsfbean;

import com.crmweb.reports.PdfBytesProducer;
import com.crmweb.services.LocatiesService;
import com.crmweb.services.PayerService;
import com.crmweb.services.ReportingService;
import com.crmweb.services.TurboReportsService;
import com.crmweb.utils.FacesInitRequestInfo;
import com.crmweb.utils.TransferUtils;
import com.crmweb.utils.ValueStringItem;
import com.crmweb.utils.ZipUtilExtra;
import com.shared.transfers.LocatieData;
import com.shared.transfers.LocatieYearTotal;
import com.shared.transfers.PayerData;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Year;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@ManagedBean
@ViewScoped
public class ReportBean extends BaseJsfBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportBean.class);

    private static final long serialVersionUID = 699843456618238856L;

    private transient PayerService payerService;

    private transient LocatiesService locatiesService;

    private transient ReportingService reportingService;

    private transient PdfBytesProducer pdfBytesProducer;

    private transient TurboReportsService turboReportsService;

    private List<PayerData> users = new ArrayList<>();

    private List<LocatieData> locaties;

    private List<LocatieData> payerLocaties;

    private int currentPayerUkgrCode;

    private List<ValueStringItem> rapporten = new ArrayList<>();

    private Integer currentRapport;

    private PayerData currentPayer;

    private DataTable payerTable;

    private DataTable payerTableAtYear;

    private List<Integer> yearOfReport = new ArrayList<>();

    private Long currentLocatie;

    private int taxYear;

    private String firstName = "";

    private String lastName = "";

    @Override
    protected void initialize() {
        if (!isForReport()) {
            return;
        }
        this.setActiveMenuDef("Rapporten");
        this.setViewTitle("Jaaroverzichten aanmaken");

        taxYear = Year.now().getValue() - 1;

        yearOfReport.add(taxYear - 4);
        yearOfReport.add(taxYear - 3);
        yearOfReport.add(taxYear - 2);
        yearOfReport.add(taxYear - 1);
        yearOfReport.add(taxYear);
        yearOfReport.add(taxYear + 1);

        locaties = locatiesService.findAllUkgrCode();

        currentLocatie = this.getSessionWallet().getLocatieId();
        if (currentLocatie == null && locaties.size() > 0) {
            currentLocatie = locaties.get(0).getId();
            this.getSessionWallet().setLocatieId(currentLocatie);
        }

        rapporten = new ArrayList<>();
        rapporten.add(new ValueStringItem(rapporten.size(), "Namen per locatie",
                "/report/PDF/jaarlocatie.html", 25));
        rapporten.add(new ValueStringItem(rapporten.size(), "AVG Jaaroverzicht per locatie",
                "/report/PDF/pageJaarLocatie.html", 78));
        rapporten.add(new ValueStringItem(rapporten.size(), "Accountant Jaaroverzicht per locatie",
                "/report/PDF/pageJaarNamedLocatie.html", 38));
        rapporten.add(new ValueStringItem(rapporten.size(), "Detail Jaaroverzicht per locatie",
                "TaxPayerJaarFullData.zip", 38).asDirectTable());
        rapporten.add(new ValueStringItem(rapporten.size(), "Bezoeker rapporten met brief",
                "VisitorPayerJaarFullData.zip", 38).asDirectTable());
        rapporten.add(new ValueStringItem(rapporten.size(), "Bezoeker rapporten maandtabel",
                "VisitorPayerJaarBdData.zip", 38).asDirectTable());
        if (currentRapport == null) {
            currentRapport = rapporten.get(0).getIndex();
        }
    }

    public void changeLocation(ValueChangeEvent e) {
        currentLocatie = Long.valueOf(e.getNewValue().toString());
        this.getSessionWallet().setLocatieId(currentLocatie);
    }

    public void changePayerLocation(ValueChangeEvent e) {
        currentPayerUkgrCode = Integer.valueOf(e.getNewValue().toString());
    }


    public void changeRapport(ValueChangeEvent e) {
        currentRapport = (Integer) e.getNewValue();
    }

    public void fetchUserAtYear() {
        if (currentRapport < rapporten.size() && currentRapport >= 0) {
            ValueStringItem valueStringItem = rapporten.get(currentRapport);
            if (valueStringItem.isDirectTable()) {
                fetchDirectTable(valueStringItem);
            } else {
                fetchGenericReport(valueStringItem);
            }

        } else {
            LOGGER.info("Unsupported report {}", currentRapport);
        }
    }

    private void fetchDirectTable(ValueStringItem valueStringItem) {
        LocatieData locatiedata = locatiesService.findLocatie(currentLocatie);
        try {
            if (valueStringItem.getReportURl().equalsIgnoreCase("TaxPayerJaarFullData.zip")) {

                String zipFile = turboReportsService.getRealReportFile(locatiedata.getUkgrcode(), taxYear, 4, true);

                Path pathToFile = Paths.get(zipFile);
                if (!pathToFile.toFile().exists()) {
                    List<Path> payerPagePaths = turboReportsService.createPageJaarNamedLocatie(locatiedata, taxYear);
                    zipFile = turboReportsService.parseDownloadDataByPaths(payerPagePaths, locatiedata, taxYear, 4);
                    pathToFile = Paths.get(zipFile);
                }
                transferZipFile(pathToFile);

            } else if ("VisitorPayerJaarFullData.zip".equalsIgnoreCase(valueStringItem.getReportURl())) {
                String zipFile = turboReportsService.getRealReportFile(locatiedata.getUkgrcode(), taxYear, 5, true);

                Path pathToFile = Paths.get(zipFile);
                if (!pathToFile.toFile().exists()) {
                    List<Path> payerPagePaths = turboReportsService.createVisitorPageJaarNamedLocatie(locatiedata, taxYear);
                    turboReportsService.parseDownloadDataByPaths(payerPagePaths, locatiedata, taxYear, 5);
                }
                transferZipFile(pathToFile);
            } else if ("VisitorPayerJaarBdData.zip".equalsIgnoreCase(valueStringItem.getReportURl()))  {
                String zipFile = turboReportsService.getRealReportFile(locatiedata.getUkgrcode(), taxYear, 6, true);
                Path pathToFile = Paths.get(zipFile);
                if (!pathToFile.toFile().exists()) {
                    List<Path> payerPagePaths = turboReportsService.fetchVisitorForLocatieSummary(locatiedata, taxYear);
                    turboReportsService.parseDownloadDataByPaths(payerPagePaths, locatiedata, taxYear, 6);
                }
                transferZipFile(pathToFile);
            } else {
                FacesMessage facesMessage = new FacesMessage("Ongeldige huisnummer ingevuld.");
                PrimeFaces.current().dialog().showMessageDynamic(facesMessage, true);
            }
        } catch (Exception e) {
            PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()), true);
        }


    }

    private void transferZipFile(Path zipFilePath) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = initFacesResponse();

        String filename = zipFilePath.getFileName().toString();

        response.setHeader("Content-disposition", "attachment;filename=" + filename);

        try (OutputStream responseOutputStream = response.getOutputStream();
             FileInputStream inputStream = new FileInputStream(zipFilePath.toString())
        ) {
            TransferUtils.handleStreamExchange(inputStream, responseOutputStream);

            responseOutputStream.flush();

        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } finally {
            // Important! Otherwise JSF will attempt to render the response which obviously will fail
            // since it's already written with a file and closed.
            facesContext.responseComplete();

        }
    }

    private void parseBytesForDownload(LocatieData locatiedata, ValueStringItem valueStringItem, byte[] pdfBytes) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String filename = String.format("rtype%02d_ukgrcode%02d_%d.pdf", valueStringItem.getIndex() + 1,
                locatiedata.getUkgrcode(), taxYear);
        parseDownloadData(facesContext, pdfBytes, filename);
    }


    private void fetchGenericReport(ValueStringItem valueStringItem) {
        Map<String, String> paramsRequest = new HashMap<>();
        paramsRequest.put("jaartal", String.valueOf(taxYear));
        paramsRequest.put("currentLocatie", String.valueOf(currentLocatie));
        paramsRequest.put("reportURL", valueStringItem.getReportURl());
        paramsRequest.put("maxPerPage", String.valueOf(valueStringItem.getMaxPerpage()));

        LocatieData locatiedata = locatiesService.findLocatie(currentLocatie);

        LocatieYearTotal locatieYearTotal = reportingService.yearLocatieTotal(taxYear, locatiedata.getUkgrcode());

        paramsRequest.put("locatieUkgrCode", String.valueOf(locatiedata.getUkgrcode()));
        paramsRequest.put("locatiePostCode", locatiedata.getPostcode());

        paramsRequest.put("locatieNummer", locatiedata.getNummer() + locatiedata.getToevoeging());
        paramsRequest.put("locatiePlaatsnaam", locatiedata.getWoonplaats());
        paramsRequest.put("locatieStraatnaam", locatiedata.getStraatnaam());
        if (locatieYearTotal.getOntvangen() != null) {
            paramsRequest.put("locatieOntvangen", String.valueOf(locatieYearTotal.getOntvangen()));
        } else {
            paramsRequest.put("locatieOntvangen", String.valueOf(0.0));
        }


        FacesContext facesContext = FacesContext.getCurrentInstance();
        byte[] pdfBytes = createPdfBytes(paramsRequest, facesContext);


        parseBytesForDownload(locatiedata, valueStringItem, pdfBytes);

        String filename = String.format("rtype%02d_ukgrcode%02d_%d.pdf", valueStringItem.getIndex() + 1,
                locatiedata.getUkgrcode(), taxYear);
        parseDownloadData(facesContext, pdfBytes, filename);
    }


    private byte[] createPdfBytes(Map<String, String> paramsRequest, FacesContext facesContext) {
        HttpServletRequest request =
                (HttpServletRequest) facesContext.getExternalContext().getRequest();
        String sessionId = request.getHeader("Cookie");
        return reportingService.createPDFLocation(request, paramsRequest, sessionId);

    }


    public void fetchUsers() {
        setCurrentPayer(null);
        users = payerService.findForNames(lastName, firstName);
    }

    public void cancelUsersList() {
        setCurrentPayer(null);
        users = new ArrayList<>();

    }

    public void gotoPerson() {
        PayerData payerToHandle = (PayerData) getPayerTable().getRowData();
        List<LocatieData> tmpList = reportingService.getListForPayer(payerToHandle.getId());

        if (tmpList.isEmpty()) {
            String fullname = payerToHandle.getAchternaam();
            PrimeFaces.current().dialog().
                    showMessageDynamic(new FacesMessage("Geen locaties beschikbaar met transacties gedurende de laatste 5 jaren voor "+ fullname + "."), true);
            cancelUsersList();
        } else {
            setCurrentPayer(payerToHandle);

            this.payerLocaties = tmpList;
            this.currentPayerUkgrCode = tmpList.get(0).getUkgrcode();

            taxYear = Year.now().getValue() - 1;
        }
    }

    public void startReportForVisitor() {
        LocatieData locnow = payerLocaties.stream().filter(it -> it.getUkgrcode() == currentPayerUkgrCode).
                findFirst().orElse(null);

        String pdfPayerFile = currentPayer.getUniqueName("pdf");
        String prefix = String.format("ukgrcode%02d_%d_", currentPayerUkgrCode, taxYear);
        String filename = prefix + pdfPayerFile;

        boolean gevonden = fetchFromZip(5, filename);

        if (!gevonden) {
            Path pathToFilex = turboReportsService.createVisitorPageJaarNamedLocatie(currentPayer, locnow, taxYear);
            if (pathToFilex != null && pathToFilex.toFile().exists()) {
                try {
                    byte[] vistorPdfBytes = Files.readAllBytes(pathToFilex);
                    FacesInitRequestInfo facesInitRequestInfo = new FacesInitRequestInfo(FacesContext.getCurrentInstance());
                    parseDownloadData(facesInitRequestInfo.getFacesContext(), vistorPdfBytes, filename);
                } catch (Exception e) {
                    PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()), true);
                } finally {

                }
            }
        }
    }


    public void startReportForTaxOffice() {

        LocatieData locnow = payerLocaties.stream().filter(it -> it.getUkgrcode() == currentPayerUkgrCode).
                findFirst().orElse(null);

        String pdfPayerFile = currentPayer.getUniqueName("pdf");
        String prefix = String.format("BD_ukgrcode%02d_%d_", currentPayerUkgrCode, taxYear);
        String filename = prefix + pdfPayerFile;

        boolean gevonden = fetchFromZip(6, filename);

        if (!gevonden) {
            Path pathToFilex = turboReportsService.createVisitorSummaryBd(currentPayer, locnow, taxYear);
            if (pathToFilex != null && pathToFilex.toFile().exists()) {
                try {
                    byte[] vistorPdfBytes = Files.readAllBytes(pathToFilex);
                    FacesInitRequestInfo facesInitRequestInfo = new FacesInitRequestInfo(FacesContext.getCurrentInstance());
                    parseDownloadData(facesInitRequestInfo.getFacesContext(), vistorPdfBytes, filename);
                    Files.deleteIfExists(pathToFilex);
                } catch (Exception e) {
                    PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()), true);
                }
            }
        }

    }


    private boolean fetchFromZip(int rType, String resultFilenaam){
        boolean gevonden = false;
        String pdfPayerFile = currentPayer.getUniqueName("pdf");
        String zipFileStr = turboReportsService.getRealReportFile(currentPayerUkgrCode, taxYear, rType, true);
        Path pathToFile = Paths.get(zipFileStr);
        if (pathToFile.toFile().exists()) {
            try {
                ZipFile zipFile = new ZipFile(zipFileStr);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();

                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().equals(pdfPayerFile)) {
                        gevonden = true;
                        PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(pdfPayerFile), true);
                        try (InputStream streamIn = zipFile.getInputStream(entry);
                             ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
                            byte[] vistorPdfBytes = ZipUtilExtra.toByteArray(streamIn, bos);
                            FacesInitRequestInfo facesInitRequestInfo = new FacesInitRequestInfo(FacesContext.getCurrentInstance());
                            parseDownloadData(facesInitRequestInfo.getFacesContext(), vistorPdfBytes, resultFilenaam);
                        } finally {
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()), true);
            }
        }
        return gevonden;
    }


    public void downloadZipFile() {
        int yearTax = this.getTaxYear();

        String filename = currentPayer.getAchternaam().replace(" ", "") + "_" +
                currentPayer.getVoornaam().replace(" ", "") + yearTax + ".zip";

        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setContentType("application/force-download");
        //response.setContentLength((int)zipData.length);
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment;filename=" + filename);
        try {
            reportingService.writeOSDownloadScanAsZip(yearTax, currentPayer, response.getOutputStream());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            facesContext.responseComplete();
            facesContext.renderResponse();
        }
    }

    private void parseDownloadData(FacesContext facesContext, byte[] dataToSend, String filename) {
        HttpServletResponse response = initFacesResponse();
        response.setHeader("Content-disposition", "attachment;filename=" + filename);
        response.setHeader("Content-Length", String.valueOf(dataToSend.length));

        OutputStream responseOutputStream = null;
        try {
            responseOutputStream = response.getOutputStream();
            responseOutputStream.write(dataToSend, 0, dataToSend.length);
            responseOutputStream.flush();
            responseOutputStream.close();

            facesContext.responseComplete();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private HttpServletResponse initFacesResponse() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response =
                (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setHeader("Content-Description", "File Transfer");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setHeader("Content-Type", "application/octet-stream");
        return response;
    }

    public void cancelReport() {
        setCurrentPayer(null);
    }

    public List<PayerData> getUsers() {
        return users;
    }

    public void setUsers(List<PayerData> users) {
        this.users = users;
    }

    public PayerData getCurrentPayer() {
        return currentPayer;
    }

    public void setCurrentPayer(PayerData currentPayer) {
        this.currentPayer = currentPayer;
    }

    public DataTable getPayerTable() {
        return payerTable;
    }

    public void setPayerTable(DataTable payerTable) {
        this.payerTable = payerTable;
    }

    public List<Integer> getYearOfReport() {
        return yearOfReport;
    }

    public void setYearOfReport(List<Integer> yearOfReport) {
        this.yearOfReport = yearOfReport;
    }

    public int getTaxYear() {
        return taxYear;
    }

    public void setTaxYear(int taxYear) {
        this.taxYear = taxYear;
    }


    public Long getCurrentLocatie() {
        return currentLocatie;
    }

    public void setCurrentLocatie(Long currentLocatie) {
        this.currentLocatie = currentLocatie;
    }

    public List<LocatieData> getLocaties() {
        return locaties;
    }

    public void setLocaties(List<LocatieData> locaties) {
        this.locaties = locaties;
    }

    public DataTable getPayerTableAtYear() {
        return payerTableAtYear;
    }

    public void setPayerTableAtYear(DataTable payerTableAtYear) {
        this.payerTableAtYear = payerTableAtYear;
    }

    public List<ValueStringItem> getRapporten() {
        return rapporten;
    }

    public int getCurrentRapport() {
        return currentRapport;
    }

    public void setCurrentRapport(int currentRapport) {
        this.currentRapport = currentRapport;
    }

    public List<LocatieData> getPayerLocaties() {
        return payerLocaties;
    }

    public void setPayerLocaties(List<LocatieData> payerLocaties) {
        this.payerLocaties = payerLocaties;
    }

    public int getCurrentPayerUkgrCode() {
        return currentPayerUkgrCode;
    }

    public void setCurrentPayerUkgrCode(int currentPayerUkgrCode) {
        this.currentPayerUkgrCode = currentPayerUkgrCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
