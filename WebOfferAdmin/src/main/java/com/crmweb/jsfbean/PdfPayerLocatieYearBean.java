package com.crmweb.jsfbean;

import com.crmweb.services.ReportingService;
import com.shared.transfers.LocatiePayerYearTotal;
import com.shared.transfers.LocatieYearTotal;
import com.shared.transfers.TransactieData;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ManagedBean
@RequestScoped
public class PdfPayerLocatieYearBean extends BaseJsfBean {
    private static final long serialVersionUID = -374925335918187735L;
    private transient ReportingService reportingService;

    private String imageBase;
    private Integer jaartal;

    private String locatieUkgrCode;
    private String locatiePostCode;
    private String locatieNummer;
    private String locatiePlaatsnaam;
    private String locatieStraatnaam;
    private Double locatieOntvangen;


    private List<LocatiePayerYearTotal> payerYearPageStart = new ArrayList<>();
    private List<LocatiePayerYearTotal> payerYearPageEnd   = new ArrayList<>();


    @Override
    protected void initialize() {
        if (!isForReport()) {
            return;
        }
        Map<String, String> requestData = FacesContext.getCurrentInstance().getExternalContext().
                getRequestParameterMap();

        jaartal = Integer.valueOf(requestData.get("jaartal"));
        locatieUkgrCode = requestData.get("locatieUkgrCode");
        locatiePostCode = requestData.get("locatiePostCode");
        locatieNummer = requestData.get("locatieNummer");
        locatiePlaatsnaam = requestData.get("locatiePlaatsnaam");
        locatieStraatnaam = requestData.get("locatieStraatnaam");
        locatieOntvangen = Double.valueOf(requestData.get("locatieOntvangen"));

        long locatieId = Long.valueOf(requestData.get("currentLocatie"));

        int port = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String contextPdf = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();

        imageBase = "http://localhost:" + port + contextPdf + "/javax.faces.resource";


        int currentIndex = Integer.valueOf(requestData.get("currentIndex"));
        int maxPerPage = Integer.valueOf(requestData.get("maxPerPage"));


        List<LocatiePayerYearTotal> data = reportingService.findpayerdPerYearAtLocation(jaartal, locatieId, currentIndex, maxPerPage, true);
        int leftSize = (int) Math.ceil(((double) data.size())/2);
        int nextPosition = 0;
        for (int i = 0; i < data.size(); i++) {
            payerYearPageStart.add(data.get(i));
            nextPosition++;
            if (nextPosition >= leftSize) break;
        }
        for (int i = nextPosition; i < data.size(); i++) {
            payerYearPageEnd.add(data.get(i));
        }
    }

    public String getImageBase() {
        return imageBase;
    }


    public Integer getJaartal() {
        return jaartal;
    }

    public List<LocatiePayerYearTotal> getPayerYearPageStart() {
        return payerYearPageStart;
    }

    public List<LocatiePayerYearTotal> getPayerYearPageEnd() {
        return payerYearPageEnd;
    }

    public String getLocatieUkgrCode() {
        return locatieUkgrCode;
    }

    public String getLocatiePostCode() {
        return locatiePostCode;
    }

    public String getLocatieNummer() {
        return locatieNummer;
    }

    public String getLocatiePlaatsnaam() {
        return locatiePlaatsnaam;
    }

    public String getLocatieStraatnaam() {
        return locatieStraatnaam;
    }

    public Double getLocatieOntvangen() {
        return locatieOntvangen;
    }
}
