package com.crmweb.jsfbean;


import com.crmweb.services.ReportingService;
import com.domain.ukgr.tables.JaarPerMethod;
import com.shared.transfers.PayerData;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ManagedBean
@RequestScoped
public class FrontPageVisitorPdf extends BaseJsfBean {
    private static final long serialVersionUID = 8213620271306709140L;

    private transient ReportingService reportingService;

     private PayerData currentPayer;

    private String imageBase;

    private double jaarTotaal = 0.0;

    private String aanhef;

    private String aanroep;

    private String plaats;

    private Date datum;

    private int jaartal;

    private String scanMap;

    private Long locationID;

    @Override
    protected void initialize() {
        if (!isForReport()) {
            return;
        }
       Map<String, String> requestData = FacesContext.getCurrentInstance().getExternalContext().
                getRequestParameterMap();
        Long payerId = Long.valueOf(requestData.get("payerID"));
        jaartal = Integer.valueOf(requestData.get("jaartal"));

        locationID = Long.valueOf(requestData.get("locationID"));

        int port = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String contextPdf = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        List<JaarPerMethod> jaaroverzicht = reportingService.getJaarOverzichtForPayer(payerId, jaartal, locationID);
        for (JaarPerMethod jaarItem : jaaroverzicht) {
            jaarTotaal += jaarItem.getTotaal();
        }

        imageBase = "http://localhost:" + port + contextPdf + "/javax.faces.resource";
        currentPayer = reportingService.getPayerInfo(payerId);

        aanhef = currentPayer.getGeslacht().equalsIgnoreCase("M") ? "Dhr." :"Mw.";

        aanroep = currentPayer.getGeslacht().equalsIgnoreCase("M") ? "Meneer" :"Mevrouw";

        datum = new Date();

        plaats = "Den Haag";

        scanMap = reportingService.getBaseProjectMap();


    }

    public String getPngSignature(){
        return reportingService.getSignatureForLetter();
    }

    public PayerData getCurrentPayer() {
        return currentPayer;
    }

    public void setCurrentPayer(PayerData currentPayer) {
        this.currentPayer = currentPayer;
    }

    public String getImageBase() {
        return imageBase;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public double getJaarTotaal() {
        return jaarTotaal;
    }

    public void setJaarTotaal(double jaarTotaal) {
        this.jaarTotaal = jaarTotaal;
    }


    public String getAanhef() {
        return aanhef;
    }

    public void setAanhef(String aanhef) {
        this.aanhef = aanhef;
    }

    public String getPlaats() {
        return plaats;
    }

    public void setPlaats(String plaats) {
        this.plaats = plaats;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getJaartal() {
        return jaartal;
    }

    public void setJaartal(int jaartal) {
        this.jaartal = jaartal;
    }

    public String getScanMap() {
        return scanMap;
    }

    public String getAanroep() {
        return aanroep;
    }

    public void setAanroep(String aanroep) {
        this.aanroep = aanroep;
    }

    public Long getLocationID() {
        return locationID;
    }

    public void setLocationID(Long locationID) {
        this.locationID = locationID;
    }
}
