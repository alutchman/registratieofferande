package com.crmweb.jsfbean;

import com.crmweb.services.CorrectionServices;
import com.crmweb.services.LocatiesService;
import com.crmweb.services.PayerService;
import com.shared.transfers.*;
import org.primefaces.component.datatable.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class TransxBean extends BaseJsfBean {
    private static final long serialVersionUID = -7308558025739225725L;
    private static final Logger LOGGER = LoggerFactory.getLogger(TransxBean.class);

    private final DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private transient CorrectionServices correctionServices;
    private transient LocatiesService locatiesService;

    private List<TransactCorrection> transactiesList;

    private List<LocatieData> locaties;

    private List<BetaalMethode> paymentMethod;

    private DataTable transactieTable;

    private TransactCorrection currentTrx;

    private String webUserId;

    private Date trxDateFrom = new Date();

    private String postcode = "";

    private String achternaam = "";

    private boolean userReadOnly;

    private boolean userSelectReadOnly;

    private Part pdfTransacties;

    @Override
    protected void initialize()  {
        if (!isForAdmin()) {
            return;
        }
        userSelectReadOnly =  getWebUserData().getCurrentlevel().ordinal() > UserLevel.BEHEER.ordinal();
        userReadOnly = getWebUserData().getCurrentlevel().ordinal() > UserLevel.ADMIN.ordinal();

        webUserId = getWebUserData().getUserid();

        this.setActiveMenuDef("Transacties");
        this.setViewTitle("Recente aanmaak transacties");

        locaties = locatiesService.findAllUkgrCode();
        paymentMethod    = Arrays.asList(BetaalMethode.values());


        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, -24); // adds one hour
        trxDateFrom  =cal.getTime();


        fetchData();
    }


    public void fetchData()  {
        if (webUserId != null && webUserId.trim().length() > 0 && currentTrx == null) {
            String formattedDate = inputFormatter.format(trxDateFrom);
            try {
                Date useDate  = inputFormatter.parse(formattedDate);
                transactiesList = correctionServices.findForCorrecting(webUserId,useDate,postcode,
                        "", achternaam);
            } catch (ParseException e) {

            }
        } else {
            transactiesList = new ArrayList<>();
        }

    }

    public void gotoTrx(){
        TransactCorrection restoredCurrentLocatie = (TransactCorrection) getTransactieTable().getRowData();
        try {
            currentTrx = restoredCurrentLocatie.clone();
        } catch (CloneNotSupportedException e) {

        }
    }

    public void deleteTrx(){
        TransactCorrection restoredCurrentLocatie = (TransactCorrection) getTransactieTable().getRowData();
        try {
            TransactCorrection delTrx = restoredCurrentLocatie.clone();
            correctionServices.removeTransaction(delTrx);
        } catch (CloneNotSupportedException e) {

        }
        fetchData();
    }



    public void downloadFile(TransactCorrection rowItem){
        if (rowItem.getTransactieData().getScanfile() == null) {
            return;
        }
        NewUploadFileInfo newUploadFileInfo = correctionServices.uploadFileFromScan(rowItem.getPayerData(), rowItem.getTransactieData());
        String downloadLocation = newUploadFileInfo.getDownloadLocation();

        if (downloadLocation == null) {
            return;
        }

        File file = new File(downloadLocation);

        if (!file.exists()){
            LOGGER.info("File dowas not exists: {}", downloadLocation);
            return;
        }

        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setContentType("application/force-download");
        response.setContentLength((int)file.length());
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment;filename="+newUploadFileInfo.getFileName());
        FileInputStream input= null;
        try {
            int i= 0;
            input = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            while ((i = input.read(buffer)) != -1) {
                response.getOutputStream().write(buffer);
                response.getOutputStream().flush();
            }

        } catch (IOException e) {

        } finally {
            try {
                if(input != null) {
                    input.close();
                }
            } catch(IOException e) {

            }
            facesContext.responseComplete();
            facesContext.renderResponse();
        }
    }

    public void changeLocation(ValueChangeEvent e){
        if (currentTrx == null) {
            return;
        }
        Long currentLocatie = Long.valueOf(e.getNewValue().toString());
        currentTrx.getTransactieData().setAdminlocatie(currentLocatie);
    }

    public void annuleerPayerTrxCorrectie(){
        currentTrx = null;
    }

    public void savePayerTrxCorrectie(){
        if (pdfTransacties != null) {
            try {
                //== nieuwe scan dus nieuwe aanmaak datum ====
                currentTrx.getTransactieData().setCreation_time(new Date());

                NewUploadFileInfo newUploadFileInfo =
                        correctionServices.uploadFileFromScan(currentTrx.getPayerData(), currentTrx.getTransactieData());
                newUploadFileInfo.createFilePath(pdfTransacties.getInputStream());

                //remove previous file if exists
                newUploadFileInfo.deleteOldInfo(currentTrx.getTransactieData().getScanfile());


                currentTrx.getTransactieData().setScanfile(newUploadFileInfo.getFileName());
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        correctionServices.updateTransaction(currentTrx);
        currentTrx = null;
        fetchData();
    }


    public List<TransactCorrection> getTransactiesList() {
        return transactiesList;
    }

    public void setTransactiesList(List<TransactCorrection> transactiesList) {
        this.transactiesList = transactiesList;
    }

    public DataTable getTransactieTable() {
        return transactieTable;
    }

    public void setTransactieTable(DataTable transactieTable) {
        this.transactieTable = transactieTable;
    }

    public TransactCorrection getCurrentTrx() {
        return currentTrx;
    }

    public void setCurrentTrx(TransactCorrection currentTrx) {
        this.currentTrx = currentTrx;
    }

    public String getWebUserId() {
        return webUserId;
    }

    public void setWebUserId(String webUserId) {
        this.webUserId = webUserId;
    }

    public Date getTrxDateFrom() {
        return trxDateFrom;
    }

    public void setTrxDateFrom(Date trxDateFrom) {
        this.trxDateFrom = trxDateFrom;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public boolean isUserReadOnly() {
        return userReadOnly;
    }

    public List<LocatieData> getLocaties() {
        return locaties;
    }

    public void setLocaties(List<LocatieData> locaties) {
        this.locaties = locaties;
    }

    public void setUserReadOnly(boolean userReadOnly) {
        this.userReadOnly = userReadOnly;
    }

    public List<BetaalMethode> getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(List<BetaalMethode> paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Part getPdfTransacties() {
        return pdfTransacties;
    }

    public void setPdfTransacties(Part pdfTransacties) {
        this.pdfTransacties = pdfTransacties;
    }

    public boolean isUserSelectReadOnly() {
        return userSelectReadOnly;
    }

    public void setUserSelectReadOnly(boolean userSelectReadOnly) {
        this.userSelectReadOnly = userSelectReadOnly;
    }
}
