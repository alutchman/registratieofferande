package com.crmweb.jsfbean;

import com.crmweb.services.LocatiesService;
import com.crmweb.services.PayerService;
import com.shared.transfers.BetaalMethode;
import com.shared.transfers.LocatieData;
import com.shared.transfers.NewUploadFileInfo;
import com.shared.transfers.OfferType;
import com.shared.transfers.PayerData;
import com.shared.transfers.PostCodeData;
import com.shared.transfers.TransactieData;
import com.shared.transfers.UserLevel;
import org.primefaces.component.datatable.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@ManagedBean
@ViewScoped
public class PayerBean extends BaseJsfBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(PayerBean.class);
    private static final long serialVersionUID = 4491235887854037648L;

    private transient PayerService payerService;
    private transient LocatiesService locatiesService;

    private String postcode;

    private String achternaam;

    private String voornaam;

    private Integer huisnummer;

    private PayerData currentPayer;

    private PayerData currentEditPayer;

    private PayerData payerData;

    private DataTable payerTable;

    private PostCodeData postCodeData = new PostCodeData();

    private TransactieData transactieData = new TransactieData();

    private List<PayerData> users = new ArrayList<>();

    private List<BetaalMethode> paymentMethod;


    private List<LocatieData> locaties;

    private Long currentLocatie;

    private List<OfferType> paymentCategorie;

    private String hintPlaats;

    private String hintStraat;

    private String lastPdfTransacties;

    private Part pdfTransacties;

    private boolean validAdresSearch;

    private boolean moreTransactions;

    private boolean userReadOnly;


    @Override
    protected void initialize() {
        if (!isForAdmin()) {
            return;
        }
        locaties = locatiesService.findAllUkgrCode();
        userReadOnly = getWebUserData().getCurrentlevel().ordinal() > UserLevel.ADMIN.ordinal();

        paymentMethod = new ArrayList<>();

        currentLocatie = this.getSessionWallet().getLocatieId();
        if (currentLocatie == null && locaties.size() > 0) {
            currentLocatie = locaties.get(0).getId();
            this.getSessionWallet().setLocatieId(currentLocatie);
        }

        paymentMethod    = Arrays.asList(BetaalMethode.values());
        paymentCategorie = Arrays.asList(OfferType.values());


        if (this.getSessionWallet().getPostCodeData() != null) {
            postCodeData = this.getSessionWallet().getPostCodeData();
            postcode = postCodeData.getPostcode();

            huisnummer = postCodeData.getHuisnummer();
            if (huisnummer != null && huisnummer.intValue() >0) {
                users = payerService.findForPostCodeHuisNummer(postcode, huisnummer);
            } else {
              users = new ArrayList<>();
            }

            if (postCodeData.getPlaats().length() > 0 &&
                    postCodeData.getStraat().length() > 0 )  {
                hintPlaats = postCodeData.getPlaats()+", ";
                hintStraat = postCodeData.getStraat();
                validAdresSearch = true;
            } else {
                validAdresSearch = users.size() > 0;
            }

        } else {
            this.achternaam = this.getSessionWallet().getSearchAchternaam();
            this.voornaam   = this.getSessionWallet().getSearchVoornaam();
            users = payerService.findForNames(achternaam, voornaam);
            validAdresSearch = users.size() > 0;
        }

        currentPayer = this.getSessionWallet().getCurrentPayer();

        this.setActiveMenuDef("Registratie");
        this.setViewTitle("Offerande Registratie");
    }

    public void postCodeEditPersonChanged(){
        if (currentEditPayer != null && currentEditPayer.getPostcode() != null && currentEditPayer.getPostcode().trim().length() >= 6 ){
            PostCodeData postCodeData = payerService.findDetailsPostCode(currentEditPayer.getPostcode().trim().substring(0,6).toUpperCase(), currentEditPayer.getNummer());
            if (postCodeData != null && postCodeData.getPlaats().length() > 0 &&
                    postCodeData.getStraat().length() > 0 )  {
                currentEditPayer.setPostcode(currentEditPayer.getPostcode().toUpperCase());
                currentEditPayer.setWoonplaats(postCodeData.getPlaats());
                currentEditPayer.setStraatnaam(postCodeData.getStraat());
            }
        }
    }

    public void postCodeChanged(){
        if (postcode != null && postcode.trim().length() >= 6 ){
            rebuildPostCodeSearch();
        } else {
            postCodeData = null;
            hintPlaats = null;
            hintStraat = null;
        }
    }

    public void fetchUsers() {
        setCurrentPayer(null);
        users = new ArrayList<>();

        validAdresSearch = false;

        transactieData = new TransactieData();
        if (postCodeData != null  && (huisnummer != null && huisnummer.intValue() >0 ) ) {
            postCodeData.setHuisnummer(huisnummer);
            postcode = postCodeData.getPostcode();
            users = payerService.findForPostCodeHuisNummer(postcode, huisnummer);
            this.getSessionWallet().setPostCodeData(postCodeData);
            validAdresSearch = true;
        }

        if (!validAdresSearch){
            users = payerService.findForNames(achternaam.trim(), voornaam);
            validAdresSearch = !achternaam.isEmpty()  && !voornaam.isEmpty();
            if (validAdresSearch) {
                this.getSessionWallet().setSearchActernaam(achternaam);
                this.getSessionWallet().setSearchVoornaam(voornaam);
                postCodeData = null;
                this.getSessionWallet().setPostCodeData(postCodeData);
            }

        }
    }

    public void addPerson(){
        setCurrentPayer(null);
        transactieData = new TransactieData();
        payerData = new PayerData();
        payerData.setGeslacht("V");
        if (!achternaam.isEmpty()) {
            payerData.setAchternaam(achternaam);
        }
        if (!voornaam.isEmpty()) {
            payerData.setVoornaam(voornaam);
        }

        payerData.setPostcode(postcode);
        payerData.setNummer(huisnummer);
        if (postCodeData != null) {
            payerData.setWoonplaats(postCodeData.getPlaats());
            payerData.setStraatnaam(postCodeData.getStraat());
        }
    }

    public void saveNewPerson(){
        payerService.addPayer(payerData);
        users = payerService.findForNames(payerData.getAchternaam(), payerData.getVoornaam());
        voornaam = payerData.getVoornaam();
        achternaam = payerData.getAchternaam();

        this.getSessionWallet().setSearchActernaam(achternaam);
        this.getSessionWallet().setSearchVoornaam(voornaam);

        payerData = null;
        setCurrentPayer(null);
        transactieData = new TransactieData();
    }

    public void cancelAddPerson(){
        payerData = null;
        setCurrentPayer(null);
        transactieData = new TransactieData();

        this.getSessionWallet().setSearchActernaam(null);
        this.getSessionWallet().setSearchVoornaam(null);
    }

    public void saveTransaction(){
        if (pdfTransacties != null) {
            lastPdfTransacties = null;
            try {
                transactieData.setCreation_time(new Date());
                NewUploadFileInfo newUploadFileInfo =
                    payerService.uploadFileFromScan(currentPayer, transactieData);
                newUploadFileInfo.createFilePath(pdfTransacties.getInputStream());
                lastPdfTransacties = newUploadFileInfo.getFileName();
                pdfTransacties = null;
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        transactieData.setScanfile(lastPdfTransacties);
        transactieData.setAdminlocatie(currentLocatie);
        payerService.addTransaction(currentPayer.getId(), transactieData, getWebUserData().getUserid());


        if (moreTransactions) {
            transactieData.setBedrag(1.0);

        } else {
            annuleerSearch();
        }
    }

    public void annuleerSearch(){
        setCurrentPayer(null);
        transactieData = new TransactieData();

        users = new ArrayList<>();
        postcode = null;
        huisnummer = null;
        achternaam = null;
        voornaam = null;
        this.getSessionWallet().setPostCodeData(null);
        this.getSessionWallet().setSearchActernaam(null);
        this.getSessionWallet().setSearchVoornaam(null);
        hintPlaats = null;
        hintStraat = null;
        validAdresSearch = false;
        lastPdfTransacties = null;
    }

    public void cancelTransaction(){
        setCurrentPayer(null);
        transactieData = new TransactieData();
        lastPdfTransacties = null;
    }


    public void changeLocation(ValueChangeEvent e){
        currentLocatie = Long.valueOf(e.getNewValue().toString());
        this.getSessionWallet().setLocatieId(currentLocatie);
    }

    public void gotoPersonTransactie(){
        transactieData = new TransactieData();
        PayerData payerToHandle = (PayerData) getPayerTable().getRowData();
        postcode = payerToHandle.getPostcode();
        huisnummer = payerToHandle.getNummer();
        setCurrentPayer(payerToHandle);
        moreTransactions = false;
    }

    public void gotoPersonEdit(){
        PayerData payerToHandle = (PayerData) getPayerTable().getRowData();
        try {
            PayerData editablePayer = (PayerData) payerToHandle.clone();
            setCurrentEditPayer(editablePayer);
        } catch (CloneNotSupportedException e) {

        }

    }

    public void cancelPersonEdit(){
        setCurrentEditPayer(null);
    }

    public void gotoPersonRemove(){
        PayerData payerToHandle = (PayerData) getPayerTable().getRowData();
        try {
            PayerData editablePayer = (PayerData) payerToHandle.clone();
            payerService.removePayer(editablePayer);
        } catch (CloneNotSupportedException e) {

        } finally {
            setCurrentEditPayer(null);
            fetchUsers();
        }
    }

    private void rebuildPostCodeSearch(){
        postCodeData = payerService.findDetailsPostCode(postcode.trim().substring(0,6).toUpperCase(), huisnummer);
        if (postCodeData != null && postCodeData.getPlaats().length() > 0 &&
                postCodeData.getStraat().length() > 0 )  {
            hintPlaats = postCodeData.getPlaats()+", ";
            hintStraat = postCodeData.getStraat();
            if (huisnummer != null && huisnummer.intValue() >0) {
                postCodeData.setHuisnummer(huisnummer);
            } else {
                postCodeData.setHuisnummer(null);
            }

            this.getSessionWallet().setPostCodeData(postCodeData);
        }
    }

    public void savePersonEdit(){
        payerService.updatePayerAdress(currentEditPayer);

        postcode = currentEditPayer.getPostcode().trim().toUpperCase();
        huisnummer = currentEditPayer.getNummer();

        rebuildPostCodeSearch();

        users = payerService.findForPostCodeHuisNummer(postcode.substring(0,6), huisnummer);

        setCurrentEditPayer(null);
    }

    public PayerData getPayerData() {
        return payerData;
    }

    public void setPayerData(PayerData payerData) {
        this.payerData = payerData;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Integer getHuisnummer() {
        return huisnummer;
    }

    public void setHuisnummer(Integer huisnummer) {
        this.huisnummer = huisnummer;
    }



    public List<PayerData> getUsers() {
        return users;
    }

    public void setUsers(List<PayerData> users) {
        this.users = users;
    }

    public PayerData getCurrentPayer() {
        return currentPayer;
    }

    public void setCurrentPayer(PayerData currentPayer) {
        this.currentPayer = currentPayer;
        this.getSessionWallet().setCurrentPayer(currentPayer);
    }

    public DataTable getPayerTable() {
        return payerTable;
    }

    public void setPayerTable(DataTable payerTable) {
        this.payerTable = payerTable;
    }

    public PostCodeData getPostCodeData() {
        return postCodeData;
    }

    public void setPostCodeData(PostCodeData postCodeData) {
        this.postCodeData = postCodeData;
    }

    public TransactieData getTransactieData() {
        return transactieData;
    }

    public void setTransactieData(TransactieData transactieData) {
        this.transactieData = transactieData;
    }

    public List<BetaalMethode> getPaymentMethod() {
        return paymentMethod;
    }

    public List<OfferType> getPaymentCategorie() {
        return paymentCategorie;
    }

    public String getHintPlaats() {
        return hintPlaats;
    }

    public void setHintPlaats(String hintPlaats) {
        this.hintPlaats = hintPlaats;
    }

    public String getHintStraat() {
        return hintStraat;
    }

    public void setHintStraat(String hintStraat) {
        this.hintStraat = hintStraat;
    }

    public boolean isValidAdresSearch() {
        return validAdresSearch;
    }

    public void setValidAdresSearch(boolean validAdresSearch) {
        this.validAdresSearch = validAdresSearch;
    }

    public List<LocatieData> getLocaties() {
        return locaties;
    }

    public void setLocaties(List<LocatieData> locaties) {
        this.locaties = locaties;
    }

    public Long getCurrentLocatie() {
        return currentLocatie;
    }

    public void setCurrentLocatie(Long currentLocatie) {
        this.currentLocatie = currentLocatie;
    }

    public boolean isMoreTransactions() {
        return moreTransactions;
    }

    public void setMoreTransactions(boolean moreTransactions) {
        this.moreTransactions = moreTransactions;
    }

    public Part getPdfTransacties() {
        return pdfTransacties;
    }

    public void setPdfTransacties(Part pdfTransacties) {
        this.pdfTransacties = pdfTransacties;
    }

    public String getLastPdfTransacties() {
        return lastPdfTransacties;
    }

    public PayerData getCurrentEditPayer() {
        return currentEditPayer;
    }

    public void setCurrentEditPayer(PayerData currentEditPayer) {
        this.currentEditPayer = currentEditPayer;
    }

    public boolean isUserReadOnly() {
        return userReadOnly;
    }

    public void setUserReadOnly(boolean userReadOnly) {
        this.userReadOnly = userReadOnly;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }
}
