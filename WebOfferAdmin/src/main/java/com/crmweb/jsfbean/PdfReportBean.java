package com.crmweb.jsfbean;


import com.crmweb.services.ReportingService;
import com.crmweb.utils.MaandTransacties;
import com.domain.ukgr.tables.JaarPerMethod;
import com.domain.ukgr.tables.MaandPerMethod;
import com.shared.transfers.PayerData;
import com.shared.transfers.TransactieData;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ManagedBean
@RequestScoped
public class PdfReportBean extends BaseJsfBean {
    private static final long serialVersionUID = 8213620271306709140L;

    private transient ReportingService reportingService;

    private List<JaarPerMethod> jaaroverzicht;

    private List<TransactieData> payerYearPageStart = new ArrayList<>();
    private List<TransactieData> payerYearPageEnd   = new ArrayList<>();

    private List<MaandTransacties> maandoverzicht = new ArrayList<>();

    private List<List<TransactieData>> payerWholeYearPager = new ArrayList<>();

    private PayerData currentPayer;

    private String imageBase;

    private double jaarTotaal = 0.0;

    @Override
    protected void initialize() {
        if (!isForReport()) {
            return;
        }

        Map<String, String> requestData = FacesContext.getCurrentInstance().getExternalContext().
                getRequestParameterMap();
        Long payerId = Long.valueOf(requestData.get("payerID"));
        int jaartal = Integer.valueOf(requestData.get("jaartal"));
        Long locationID =  Long.valueOf(requestData.get("locationID"));

        int port = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String contextPdf = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        jaaroverzicht = reportingService.getJaarOverzichtForPayer(payerId, jaartal, locationID);
        jaaroverzicht.stream().forEach(e -> jaarTotaal += e.getTotaal());

        imageBase = "http://localhost:" + port + contextPdf + "/javax.faces.resource";
        currentPayer = reportingService.getPayerInfo(payerId);

        int currentIndex = Integer.valueOf(requestData.get("currentIndex"));

        String reportType = requestData.get("reportType");

        if (reportType.equals("PERSONAL_PRINT")) {
            int maxPerPage = Integer.valueOf(requestData.get("maxPerPage"));
            List<TransactieData> allPayers = reportingService.getPayerTransacties(locationID, payerId, jaartal,currentIndex, maxPerPage);
            int leftSize = (int) Math.ceil(((double) allPayers.size())/2);
            int nextPosition = 0;
            for (int i = 0; i < allPayers.size(); i++) {
                payerYearPageStart.add(allPayers.get(i));
                nextPosition++;
                if (nextPosition >= leftSize) break;
            }
            for (int i = nextPosition; i < allPayers.size(); i++) {
                payerYearPageEnd.add(allPayers.get(i));
            }
        } else {
            maandoverzicht = reportingService.vulMaandOverzicht(locationID, payerId, jaartal);
        }

    }

    public PayerData getCurrentPayer() {
        return currentPayer;
    }

    public void setCurrentPayer(PayerData currentPayer) {
        this.currentPayer = currentPayer;
    }

    public List<JaarPerMethod> getJaaroverzicht() {
        return jaaroverzicht;
    }

    public void setJaaroverzicht(List<JaarPerMethod> jaaroverzicht) {
        this.jaaroverzicht = jaaroverzicht;
    }

    public List<TransactieData> getPayerYearPageStart() {
        return payerYearPageStart;
    }

    public void setPayerYearPageStart(List<TransactieData> payerYearPageStart) {
        this.payerYearPageStart = payerYearPageStart;
    }

    public List<List<TransactieData>> getPayerWholeYearPager() {
        return payerWholeYearPager;
    }

    public void setPayerWholeYearPager(List<List<TransactieData>> payerWholeYearPager) {
        this.payerWholeYearPager = payerWholeYearPager;
    }

    public String getImageBase() {
        return imageBase;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public double getJaarTotaal() {
        return jaarTotaal;
    }

    public void setJaarTotaal(double jaarTotaal) {
        this.jaarTotaal = jaarTotaal;
    }

    public List<TransactieData> getPayerYearPageEnd() {
        return payerYearPageEnd;
    }

    public void setPayerYearPageEnd(List<TransactieData> payerYearPageEnd) {
        this.payerYearPageEnd = payerYearPageEnd;
    }

    public List<MaandTransacties> getMaandoverzicht() {
        return maandoverzicht;
    }

    public void setMaandoverzicht(List<MaandTransacties> maandoverzicht) {
        this.maandoverzicht = maandoverzicht;
    }
}
