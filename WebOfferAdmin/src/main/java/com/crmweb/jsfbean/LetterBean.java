package com.crmweb.jsfbean;

import com.crmweb.services.MessageService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@Slf4j
@ManagedBean
@ViewScoped
public class LetterBean extends BaseJsfBean {
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient MessageService messageService;
    private String briefData;
    private String editTag;

    private String currentTag;

    private Map<String, Path> tagToPathMap;

    @Override
    protected void initialize() {
        if (!isForAdmin()) {
            return;
        }

        if (Objects.isNull(currentTag)) {
            currentTag = "Bezoeker";
        }

        tagToPathMap = messageService.getTagToPathMap();

        this.setActiveMenuDef("Brieven");
        this.setViewTitle("Brief instellingen");
    }

    public String getBriefData() {
        return briefData;
    }

    public void setBriefData(String value) {

        this.briefData = value;
    }


    public String getEditTag() {
        return editTag;
    }

    public void exitEdit(boolean update) {
        if (update) {
            messageService.updateBrief(editTag, briefData);
        }
        editTag = null;
    }



    public void startChange() {
        if (currentTag != null) {
            log.info("current tag  =   {}", currentTag );
            briefData = messageService.fetchLetterData(currentTag);
            editTag = currentTag;
        }
    }

    public List<String> getTags() {
        return tagToPathMap.keySet().stream().collect(Collectors.toList());
    }

    public String getCurrentTag() {
        return currentTag;
    }

    public void setCurrentTag(String value) {
        this.currentTag = value;
    }


    public void changeTag(ValueChangeEvent e){
        String val = e.getNewValue().toString();

    }
}
