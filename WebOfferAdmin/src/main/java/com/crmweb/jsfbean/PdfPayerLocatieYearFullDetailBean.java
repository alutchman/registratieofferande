package com.crmweb.jsfbean;

import com.crmweb.services.ReportingService;
import com.shared.transfers.LocatiePayerYearTotal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ManagedBean
@RequestScoped
public class PdfPayerLocatieYearFullDetailBean extends BaseJsfBean {
    private static final long serialVersionUID = 2721596637509000901L;

    private transient ReportingService reportingService;

    private String imageBase;
    private Integer jaartal;

    private String locatieUkgrCode;
    private String locatiePostCode;
    private String locatieNummer;
    private String locatiePlaatsnaam;
    private String locatieStraatnaam;
    private Double locatieOntvangen;


    private List<LocatiePayerYearTotal> payerYearPageStart = new ArrayList<>();


    @Override
    protected void initialize() {
        if (!isForReport()) {
            return;
        }
        Map<String, String> requestData = FacesContext.getCurrentInstance().getExternalContext().
                getRequestParameterMap();

        jaartal = Integer.valueOf(requestData.get("jaartal"));
        locatieUkgrCode = requestData.get("locatieUkgrCode");
        locatiePostCode = requestData.get("locatiePostCode");
        locatieNummer = requestData.get("locatieNummer");
        locatiePlaatsnaam = requestData.get("locatiePlaatsnaam");
        locatieStraatnaam = requestData.get("locatieStraatnaam");
        locatieOntvangen = Double.valueOf(requestData.get("locatieOntvangen"));

        long locatieId = Long.valueOf(requestData.get("currentLocatie"));

        int port = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String contextPdf = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();

        imageBase = "http://localhost:" + port + contextPdf + "/javax.faces.resource";


        int currentIndex = Integer.valueOf(requestData.get("currentIndex"));
        int maxPerPage = Integer.valueOf(requestData.get("maxPerPage"));

        payerYearPageStart = reportingService.findpayerdPerYearAtLocation(jaartal, locatieId, currentIndex, maxPerPage, false);

    }

    public String getImageBase() {
        return imageBase;
    }


    public Integer getJaartal() {
        return jaartal;
    }

    public List<LocatiePayerYearTotal> getPayerYearPageStart() {
        return payerYearPageStart;
    }

    public String getLocatieUkgrCode() {
        return locatieUkgrCode;
    }

    public String getLocatiePostCode() {
        return locatiePostCode;
    }

    public String getLocatieNummer() {
        return locatieNummer;
    }

    public String getLocatiePlaatsnaam() {
        return locatiePlaatsnaam;
    }

    public String getLocatieStraatnaam() {
        return locatieStraatnaam;
    }

    public Double getLocatieOntvangen() {
        return locatieOntvangen;
    }
}
