package com.crmweb.jsfbean;


import com.crmweb.services.LocatiesService;
import com.crmweb.services.PayerService;
import com.shared.transfers.LocatieData;
import com.shared.transfers.PayerData;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ManagedBean
@RequestScoped
public class PdfLocatiesBean extends BaseJsfBean {
    private static final long serialVersionUID = 8213620271306709140L;

    private transient LocatiesService locatiesService;
    private transient PayerService payerService;

    private String imageBase;

    private List<PayerData> payerLeftData = new ArrayList<>();

    private LocatieData locatiedata;
    private Integer jaartal;

    @Override
    protected void initialize() {
        if (!isForReport()) {
            return;
        }
        Map<String, String> requestData = FacesContext.getCurrentInstance().getExternalContext().
                getRequestParameterMap();

        jaartal = Integer.valueOf(requestData.get("jaartal"));

        Long currentLocatie = Long.valueOf(requestData.get("currentLocatie"));

        locatiedata = locatiesService.findLocatie(currentLocatie);

        int port = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
        String contextPdf = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();

        imageBase = "http://localhost:" + port + contextPdf + "/javax.faces.resource";

        int currentIndex = Integer.valueOf(requestData.get("currentIndex"));
        int maxPerPage   = Integer.valueOf(requestData.get("maxPerPage"));
        payerLeftData    = payerService.findInYearAtLocation(jaartal, currentLocatie, currentIndex, maxPerPage);
    }

    public LocatieData getLocatiedata() {
        return locatiedata;
    }

    public void setLocatiedata(LocatieData locatiedata) {
        this.locatiedata = locatiedata;
    }

    public String getImageBase() {
        return imageBase;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public Integer getJaartal() {
        return jaartal;
    }

    public void setJaartal(Integer jaartal) {
        this.jaartal = jaartal;
    }

    public List<PayerData> getPayerLeftData() {
        return payerLeftData;
    }

    public void setPayerLeftData(List<PayerData> payerLeftData) {
        this.payerLeftData = payerLeftData;
    }

}
