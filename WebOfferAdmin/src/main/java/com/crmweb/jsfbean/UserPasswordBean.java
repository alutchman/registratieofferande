package com.crmweb.jsfbean;

import com.crmweb.services.ReportingService;
import com.crmweb.services.ServiceLogin;
import com.shared.transfers.SearchUser;
import com.shared.transfers.UserLevel;
import com.shared.transfers.WebUserData;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.file.UploadedFileWrapper;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class UserPasswordBean extends BaseJsfBean {
    private static final long serialVersionUID = 6754119563563228374L;

    private transient ServiceLogin serviceLogin;

    private transient ReportingService reportingService;

    private WebUserData alterUser = new WebUserData();

    private String oldpassword;

    private String newPasswordA;

    private String newPasswordB;

    private SearchUser searchUser = new SearchUser();

    private List<WebUserData> otherUsers;

    private WebUserData currentUser;

    private Part signature;


    private transient UploadedFileWrapper newSignature;

    private List<UserLevel> accessLevels = new ArrayList<>();

    private DataTable userTable;
    private WebUserData newUser;

    @Override
    protected void initialize()  {
        for (UserLevel aUserLevel : UserLevel.values()) {
            accessLevels.add(aUserLevel);
        }

        try {
            alterUser = (WebUserData) getWebUserData().clone();
        } catch (CloneNotSupportedException e) {
        }
        this.setActiveMenuDef("Instellingen");
        this.setViewTitle("Uw instellingen");
    }

    public void haalGebruikersOp(){
        otherUsers = serviceLogin.haalGebruikersOp(searchUser, this.getWebUserData().getUserid());
        if (otherUsers == null || otherUsers.size() == 0) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( "Er zijn geen gebruikers gevonden.");
            facesContext.addMessage("ZoekGebruiker:voornaam", facesMessage);
            otherUsers = null;
        }
    }

    public void cancelListShow(){
        otherUsers = null;
    }

    public void annuleerEdit(){
        this.currentUser = null;
    }


    public String editUser(){
        currentUser = (WebUserData) getUserTable().getRowData();

        return null;
    }

    public void addNewUser(){
        newUser = new WebUserData();
        newUser.setCurrentlevel(UserLevel.GEEN);
    }

    public void saveNewUser(){
        boolean userIdAvailable = serviceLogin.isUserIdFree(newUser);
        if (!userIdAvailable) {

            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( "User ID `"+ newUser.getUserid() +
                    "` is al bezet.");
            facesContext.addMessage("GebruikerToevoegen:userID", facesMessage);
            return;
        }

        if (newUser.getUserid().trim().length() < 6){
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( "User ID  moet minimaal 6 karakters lang zijn.");
            facesContext.addMessage("GebruikerToevoegen:userID", facesMessage);
            return;
        }
        try {
            serviceLogin.addNewUser(newUser);
            newUser = null;
            otherUsers = null;
        } catch(Exception e) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( e.getMessage());
            facesContext.addMessage("GebruikerToevoegen:userID", facesMessage);
        }


    }

    public void cancleNewUser(){

        newUser = null;
    }

    public void oplaanEdit(){
        serviceLogin.updateUser(this.currentUser);

        this.currentUser = null;
    }


    private int wachtwoordControle(FacesContext facesContext){
        int errorCount = 0;

        if ((oldpassword == null || oldpassword.trim().length() == 0) &&
                (newPasswordA == null || newPasswordA.trim().length() == 0) &&
                (newPasswordB == null || newPasswordB.trim().length() == 0) ) {
            return 0;
        }

        if (oldpassword == null || oldpassword.trim().length() == 0) {
            FacesMessage fmActPassw = new FacesMessage( "Het Actuele wachtwoord is niet ingevuld.");
            facesContext.addMessage("updatePassword:ActueleWachtwoord", fmActPassw);
            errorCount++;
        }
        if (newPasswordA == null || newPasswordA.trim().length() == 0) {
            FacesMessage fmActPassw = new FacesMessage( "Het nieuwe wachtwoord is niet ingevuld.");
            facesContext.addMessage("updatePassword:NieuweWachtwoord", fmActPassw);
            errorCount++;
        }

        if (newPasswordB == null || newPasswordB.trim().length() == 0) {
            FacesMessage fmActPassw = new FacesMessage( "Controle nieuwe wachtwoord is niet ingevuld.");
            facesContext.addMessage("updatePassword:NieuweWachtwoordControle", fmActPassw);
            errorCount++;
        }

        if ((newPasswordA != null && newPasswordA.trim().length() > 0) &&
                (newPasswordB != null && newPasswordB.trim().length() > 0) ) {
            if(!newPasswordA.equals(newPasswordB)) {
                FacesMessage fmActPassw = new FacesMessage( "De nieuwe wachtwoorden zijn niet gelijk.");
                facesContext.addMessage("updatePassword:NieuweWachtwoordControle", fmActPassw);
                errorCount++;
            } else if (newPasswordA.equals(oldpassword)) {
                FacesMessage fmActPassw = new FacesMessage( "Het wachtwoord is niet gewijzigd.");
                facesContext.addMessage("updatePassword:WijzigWachtwoord", fmActPassw);
                errorCount++;
            }
        }

        if (newPasswordA.trim().length() < 8) {
            FacesMessage fmActPassw = new FacesMessage( "Het nieuwe wachtwoord moet minimaal 8 karakters bevatten.");
            facesContext.addMessage("updatePassword:NieuweWachtwoord", fmActPassw);
            facesContext.addMessage("updatePassword:NieuweWachtwoordControle", fmActPassw);
            errorCount++;
        }
        return errorCount;

    }

    public void updateMySelf(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        int errorCount = wachtwoordControle(facesContext);

        String oldVoornaam = getWebUserData().getVoornaam();
        String oldAchternaam = getWebUserData().getAchternaam();

        if (oldAchternaam.equals(alterUser.getAchternaam()) &&
                oldVoornaam.equals(alterUser.getVoornaam()) &&
                (oldpassword == null || oldpassword.trim().length() == 0 ) &&
                (newPasswordA == null || newPasswordA.trim().length() == 0 )) {
            FacesMessage fmActPassw = new FacesMessage( "Er zijn geen wijzigingen.");
            facesContext.addMessage("updatePassword:WijzigWachtwoord", fmActPassw);
            errorCount++;
        }

        if (errorCount > 0) {
            return;
        }

        if (!serviceLogin.wachtwoordNaInloggen(oldpassword, newPasswordA,alterUser)) {
            FacesMessage fmActPassw = new FacesMessage( "Het Actuele wachtwoord is niet correct.");
            facesContext.addMessage("updatePassword:ActueleWachtwoord", fmActPassw);
        } else {
            FacesMessage fmActPassw = new FacesMessage( "De wijzigingen zijn opgeslagen.");
            facesContext.addMessage("updatePassword:WijzigWachtwoord", fmActPassw);
        }
    }

    public void updateSignature(){
        if (newSignature != null) {
            try {
                byte[] dataImage = newSignature.getContent();
                String contentType = newSignature.getContentType();
                String filename = newSignature.getFileName();
                long size = newSignature.getSize();
                String extension = FilenameUtils.getExtension(filename);

                if (extension.equalsIgnoreCase("png")) {
                    String projectMap = reportingService.getBaseProjectMap();
                    Files.deleteIfExists(new File(projectMap, "signature.png").toPath());
                    Files.write(new File(projectMap, "signature.png").toPath(), dataImage);
                }
                newSignature = null;
            } catch (IOException e) {

            }
        }
    }

    public String getPngSignature(){
        return reportingService.getSignatureForLetter();
    }



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public WebUserData getAlterUser() {
        return alterUser;
    }

    public void setAlterUser(WebUserData alterUser) {
        this.alterUser = alterUser;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewPasswordA() {
        return newPasswordA;
    }

    public void setNewPasswordA(String newPasswordA) {
        this.newPasswordA = newPasswordA;
    }

    public String getNewPasswordB() {
        return newPasswordB;
    }

    public void setNewPasswordB(String newPasswordB) {
        this.newPasswordB = newPasswordB;
    }

    public List<UserLevel> getAccessLevels() {
        return accessLevels;
    }

    public void setAccessLevels(List<UserLevel> accessLevels) {
        this.accessLevels = accessLevels;
    }

    public SearchUser getSearchUser() {
        return searchUser;
    }

    public void setSearchUser(SearchUser searchUser) {
        this.searchUser = searchUser;
    }

    public List<WebUserData> getOtherUsers() {
        return otherUsers;
    }

    public void setOtherUsers(List<WebUserData> otherUsers) {
        this.otherUsers = otherUsers;
    }

    public DataTable getUserTable() {
        return userTable;
    }

    public void setUserTable(DataTable userTable) {
        this.userTable = userTable;
    }

    public WebUserData getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(WebUserData currentUser) {
        this.currentUser = currentUser;
    }

    public WebUserData getNewUser() {
        return newUser;
    }

    public void setNewUser(WebUserData newUser) {
        this.newUser = newUser;
    }

    public Part getSignature() {
        return signature;
    }

    public void setSignature(Part signature) {
        this.signature = signature;
    }

    public UploadedFileWrapper getNewSignature() {
        return newSignature;
    }

    public void setNewSignature(UploadedFileWrapper newSignature) {
        this.newSignature = newSignature;
    }
}
