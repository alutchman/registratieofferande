package com.crmweb.xmldata;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@XmlRootElement(name="FMPDSORESULT")
@XmlAccessorType(XmlAccessType.FIELD)
public class PayerListData {

    @XmlElement(name = "ROW")
    private List<PayerImport> rowData;


    public List<PayerImport> getRowData() {
        return rowData;
    }

    public void setRowData(List<PayerImport> rowData) {
        this.rowData = rowData;
    }

    public void cleanData(int year){
        for(PayerImport item : this.rowData) {
            item.fillLists(year);

            item.setAchternaam(item.getAchternaam().trim());
            item.setVoornaam(item.getVoornaam().trim());

            //item.setUkgrLocatieId(1);

            String postcode = item.getPostcode();
            String[] dataCode = postcode.split(" ");
            if (dataCode.length == 2) {
                String newPostCode = dataCode[0].substring(0,4)+ dataCode[1].trim();
                item.setPostcode(newPostCode);
            }

            String tel = item.getTelefonnNummer();
            item.setTelefonnNummer( tel.replaceAll(" ",""));
            item.setWoonplaats(item.getWoonplaats().trim());
            String adres = item.getAdres().trim();

            String numberPart = adres.replaceAll("[\\D]", "");
            item.setNummer(Integer.parseInt(numberPart));

            String numberTxt = String.valueOf(numberPart);
            int numberLength = numberTxt.length();
            int numberIndex = adres.indexOf(numberTxt);

            String streetTmp = adres.substring(0,numberIndex-1);
            item.setStraat(String.join(" ", streetTmp));
            item.setAdres(null);

            int afterNuberIndex = numberIndex+numberLength;
            if (afterNuberIndex < adres.length()-1) {
                item.setToevoeging(adres.substring(afterNuberIndex).trim());
            }



            if (item.getTitel().trim().equals("Mw.")) {
                item.setGender("V");
                item.setTitel(null);
            } else   if (item.getTitel().trim().equals("Dhr.")) {
                item.setTitel(null);
                item.setGender("M");
            }

            item.getTransfers().sort(new Comparator<TransferTransaction>() {
                @Override
                public int compare(TransferTransaction o1, TransferTransaction o2) {
                    return o1.getTransactiedatum().compareTo(o2.getTransactiedatum());
                }
            });
        }


    }

    @Override
    public String toString() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(PayerListData.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            StringWriter writer = new StringWriter();
            marshaller.marshal(this, writer);
            return writer.toString();
        } catch (JAXBException e) {
            return "PayerListData{" +
                    "rowData=" + rowData.toString() +
                    '}';
        }
    }
}
