package com.crmweb.xmldata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement(name="ROW")
@XmlAccessorType(XmlAccessType.FIELD)
public class PayerImport {

    @XmlElement(name="Achternaam")
    private String achternaam;


    @XmlElement(name="Naam")
    private String voornaam;

    @XmlElement(name="Gender")
    private String gender;

    @XmlElement(name="Adres")
    private String adres;


    @XmlElement(name="Straat")
    private String straat;

    @XmlElement(name="Nummer")
    private int nummer;

    @XmlElement(name="Toevoeging")
    private String toevoeging;


    @XmlElement(name="Postcode")
    private String postcode;

    @XmlElement(name="Woonplaats")
    private String Woonplaats;


    @XmlElement(name="Tel")
    private String telefonnNummer;

    @XmlElement(name="Titel")
    private String titel;

    @XmlElement(name="Vestiging")
    private String ukgrLocatie;


    @XmlElement(name="VestigingId")
    private Long ukgrLocatieId;

    @XmlElement(name="Transactie")
    private List<TransferTransaction> transfers;

    @XmlElement(name="Bank_Jan")
    private String bankJan;

    @XmlElement(name="Bank_Feb")
    private String bankFeb;

    @XmlElement(name="Bank_Mar")
    private String bankMar;

    @XmlElement(name="Bank_Apr")
    private String bankApr;

    @XmlElement(name="Bank_Mei")
    private String bankMei;

    @XmlElement(name="Bank_Jun")
    private String bankJun;

    @XmlElement(name="Bank_Jul")
    private String bankJul;

    @XmlElement(name="Bank_Aug")
    private String bankAug;

    @XmlElement(name="Bank_Sep")
    private String bankSep;

    @XmlElement(name="Bank_Okt")
    private String bankOkt;

    @XmlElement(name="Bank_Nov")
    private String bankNov;

    @XmlElement(name="Bank_Dec")
    private String bankDec;


    @XmlElement(name="Cash_Jan")
    private String cashJan;

    @XmlElement(name="Cash_Feb")
    private String cashFeb;

    @XmlElement(name="Cash_Mar")
    private String cashMar;

    @XmlElement(name="Cash_Apr")
    private String cashApr;

    @XmlElement(name="Cash_Mei")
    private String cashMei;

    @XmlElement(name="Cash_Jun")
    private String cashJun;

    @XmlElement(name="Cash_Jul")
    private String cashJul;


    @XmlElement(name="Cash_Aug")
    private String cashAug;

    @XmlElement(name="Cash_Sep")
    private String cashSep;

    @XmlElement(name="Cash_Okt")
    private String cashOkt;

    @XmlElement(name="Cash_Nov")
    private String cashNov;

    @XmlElement(name="Cash_Dec")
    private String cashDec;


    @XmlElement(name="PIN_Jan")
    private String pinJan;

    @XmlElement(name="PIN_Feb")
    private String pinFeb;

    @XmlElement(name="PIN_Mar")
    private String pinMar;

    @XmlElement(name="PIN_Apr")
    private String pinApr;

    @XmlElement(name="PIN_Mei")
    private String pinMei;

    @XmlElement(name="PIN_Jun")
    private String pinJun;

    @XmlElement(name="PIN_Jul")
    private String pinJul;

    @XmlElement(name="PIN_Aug")
    private String pinAug;

    @XmlElement(name="PIN_Sep")
    private String pinSep;

    @XmlElement(name="PIN_Okt")
    private String pinOkt;

    @XmlElement(name="PIN_Nov")
    private String pinNov;

    @XmlElement(name="PIN_Dec")
    private String pinDec;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getWoonplaats() {
        return Woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        Woonplaats = woonplaats;
    }

    public String getTelefonnNummer() {
        return telefonnNummer;
    }

    public void setTelefonnNummer(String telefonnNummer) {
        this.telefonnNummer = telefonnNummer;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getUkgrLocatie() {
        return ukgrLocatie;
    }

    public void setUkgrLocatie(String ukgrLocatie) {
        this.ukgrLocatie = ukgrLocatie;
    }

    public String getBankJan() {
        return bankJan;
    }

    public void setBankJan(String bankJan) {
        this.bankJan = bankJan;
    }

    public String getBankFeb() {
        return bankFeb;
    }

    public void setBankFeb(String bankFeb) {
        this.bankFeb = bankFeb;
    }

    public String getBankMar() {
        return bankMar;
    }

    public void setBankMar(String bankMar) {
        this.bankMar = bankMar;
    }

    public String getBankApr() {
        return bankApr;
    }

    public void setBankApr(String bankApr) {
        this.bankApr = bankApr;
    }

    public String getBankMei() {
        return bankMei;
    }

    public void setBankMei(String bankMei) {
        this.bankMei = bankMei;
    }

    public String getBankJun() {
        return bankJun;
    }

    public void setBankJun(String bankJun) {
        this.bankJun = bankJun;
    }

    public String getBankJul() {
        return bankJul;
    }

    public void setBankJul(String bankJul) {
        this.bankJul = bankJul;
    }

    public String getBankAug() {
        return bankAug;
    }

    public void setBankAug(String bankAug) {
        this.bankAug = bankAug;
    }

    public String getBankSep() {
        return bankSep;
    }

    public void setBankSep(String bankSep) {
        this.bankSep = bankSep;
    }

    public String getBankOkt() {
        return bankOkt;
    }

    public void setBankOkt(String bankOkt) {
        this.bankOkt = bankOkt;
    }

    public String getBankNov() {
        return bankNov;
    }

    public void setBankNov(String bankNov) {
        this.bankNov = bankNov;
    }

    public String getBankDec() {
        return bankDec;
    }

    public void setBankDec(String bankDec) {
        this.bankDec = bankDec;
    }

    public String getCashJan() {
        return cashJan;
    }

    public void setCashJan(String cashJan) {
        this.cashJan = cashJan;
    }

    public String getCashFeb() {
        return cashFeb;
    }

    public void setCashFeb(String cashFeb) {
        this.cashFeb = cashFeb;
    }

    public String getCashMar() {
        return cashMar;
    }

    public void setCashMar(String cashMar) {
        this.cashMar = cashMar;
    }

    public String getCashApr() {
        return cashApr;
    }

    public void setCashApr(String cashApr) {
        this.cashApr = cashApr;
    }

    public String getCashMei() {
        return cashMei;
    }

    public void setCashMei(String cashMei) {
        this.cashMei = cashMei;
    }

    public String getCashJun() {
        return cashJun;
    }

    public void setCashJun(String cashJun) {
        this.cashJun = cashJun;
    }

    public String getCashJul() {
        return cashJul;
    }

    public void setCashJul(String cashJul) {
        this.cashJul = cashJul;
    }

    public String getCashAug() {
        return cashAug;
    }

    public void setCashAug(String cashAug) {
        this.cashAug = cashAug;
    }

    public String getCashSep() {
        return cashSep;
    }

    public void setCashSep(String cashSep) {
        this.cashSep = cashSep;
    }

    public String getCashOkt() {
        return cashOkt;
    }

    public void setCashOkt(String cashOkt) {
        this.cashOkt = cashOkt;
    }

    public String getCashNov() {
        return cashNov;
    }

    public void setCashNov(String cashNov) {
        this.cashNov = cashNov;
    }

    public String getCashDec() {
        return cashDec;
    }

    public void setCashDec(String cashDec) {
        this.cashDec = cashDec;
    }

    public String getPinJan() {
        return pinJan;
    }

    public void setPinJan(String pinJan) {
        this.pinJan = pinJan;
    }

    public String getPinFeb() {
        return pinFeb;
    }

    public void setPinFeb(String pinFeb) {
        this.pinFeb = pinFeb;
    }

    public String getPinMar() {
        return pinMar;
    }

    public void setPinMar(String pinMar) {
        this.pinMar = pinMar;
    }

    public String getPinApr() {
        return pinApr;
    }

    public void setPinApr(String pinApr) {
        this.pinApr = pinApr;
    }

    public String getPinMei() {
        return pinMei;
    }

    public void setPinMei(String pinMei) {
        this.pinMei = pinMei;
    }

    public String getPinJun() {
        return pinJun;
    }

    public void setPinJun(String pinJun) {
        this.pinJun = pinJun;
    }

    public String getPinJul() {
        return pinJul;
    }

    public void setPinJul(String pinJul) {
        this.pinJul = pinJul;
    }

    public String getPinAug() {
        return pinAug;
    }

    public void setPinAug(String pinAug) {
        this.pinAug = pinAug;
    }

    public String getPinSep() {
        return pinSep;
    }

    public void setPinSep(String pinSep) {
        this.pinSep = pinSep;
    }

    public String getPinOkt() {
        return pinOkt;
    }

    public void setPinOkt(String pinOkt) {
        this.pinOkt = pinOkt;
    }

    public String getPinNov() {
        return pinNov;
    }

    public void setPinNov(String pinNov) {
        this.pinNov = pinNov;
    }

    public String getPinDec() {
        return pinDec;
    }

    public void setPinDec(String pinDec) {
        this.pinDec = pinDec;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }


    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    public List<TransferTransaction> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<TransferTransaction> transfers) {
        this.transfers = transfers;
    }

    public Long getUkgrLocatieId() {
        return ukgrLocatieId;
    }

    public void setUkgrLocatieId(Long ukgrLocatieId) {
        this.ukgrLocatieId = ukgrLocatieId;
    }

    public void fillLists(int year) {
        this.transfers = new ArrayList<>();
        addBedragToList(year, 1, this.bankJan, "BANK");
        this.bankJan = null;
        addBedragToList(year, 2, this.bankFeb, "BANK");
        this.bankFeb = null;
        addBedragToList(year, 3, this.bankMar, "BANK");
        this.bankMar = null;
        addBedragToList(year, 4, this.bankApr, "BANK");
        this.bankApr = null;
        addBedragToList(year, 5, this.bankMei, "BANK");
        this.bankMei = null;
        addBedragToList(year, 6, this.bankJun, "BANK");
        this.bankJun = null;
        addBedragToList(year, 7, this.bankJul, "BANK");
        this.bankJul = null;
        addBedragToList(year, 8, this.bankAug, "BANK");
        this.bankAug = null;
        addBedragToList(year, 9, this.bankSep, "BANK");
        this.bankSep = null;
        addBedragToList(year, 10, this.bankOkt, "BANK");
        this.bankOkt = null;
        addBedragToList(year, 11, this.bankNov, "BANK");
        this.bankNov = null;
        addBedragToList(year, 12, this.bankDec, "BANK");
        this.bankDec = null;

        addBedragToList(year, 1, this.pinJan, "PIN");
        this.pinJan = null;
        addBedragToList(year, 2, this.pinFeb, "PIN");
        this.pinFeb = null;
        addBedragToList(year, 3, this.pinMar, "PIN");
        this.pinMar = null;
        addBedragToList(year, 4, this.pinApr, "PIN");
        this.pinApr = null;
        addBedragToList(year, 5, this.pinMei, "PIN");
        this.pinMei = null;
        addBedragToList(year, 6, this.pinJun, "PIN");
        this.pinJun = null;
        addBedragToList(year, 7, this.pinJul, "PIN");
        this.pinJul = null;
        addBedragToList(year, 8, this.pinAug, "PIN");
        this.pinAug = null;
        addBedragToList(year, 9, this.pinSep, "PIN");
        this.pinSep = null;
        addBedragToList(year, 10, this.pinOkt, "PIN");
        this.pinOkt = null;
        addBedragToList(year, 11, this.pinNov, "PIN");
        this.pinNov = null;
        addBedragToList(year, 12, this.pinDec, "PIN");
        this.pinDec = null;

        addBedragToList(year, 1, this.cashJan, "CONTANT");
        this.cashJan = null;
        addBedragToList(year, 2, this.cashFeb, "CONTANT");
        this.cashFeb = null;
        addBedragToList(year, 3, this.cashMar, "CONTANT");
        this.cashMar = null;
        addBedragToList(year, 4, this.cashApr, "CONTANT");
        this.cashApr = null;
        addBedragToList(year, 5, this.cashMei, "CONTANT");
        this.cashMei = null;
        addBedragToList(year, 6, this.cashJun, "CONTANT");
        this.cashJun = null;
        addBedragToList(year, 7, this.cashJul, "CONTANT");
        this.cashJul = null;
        addBedragToList(year, 8, this.cashAug, "CONTANT");
        this.cashAug = null;
        addBedragToList(year, 9, this.cashSep, "CONTANT");
        this.cashSep = null;
        addBedragToList(year, 10, this.cashOkt, "CONTANT");
        this.cashOkt = null;
        addBedragToList(year, 11, this.cashNov, "CONTANT");
        this.cashNov = null;
        addBedragToList(year, 12, this.cashDec, "CONTANT");
        this.cashDec = null;
    }

    private Double convertStringToDouble(String value){
        if (value != null && value.trim().length() > 0) {
            value = value.replaceAll(",",".");
            try {
                return  Double.valueOf(value.trim());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void addBedragToList(int year, int month, String bedrag, String via) {
        if (bedrag == null) {
            return;
        }

        Double bedragDouble = convertStringToDouble(bedrag);
        if (bedragDouble == null) {
            return;
        }

        TransferTransaction transfer = new TransferTransaction();
        transfer.setBedrag(bedragDouble);
        transfer.setBetaalMethode(via);
        transfer.setOfferType("OFFERANDE");

        int month2Use = month < 12 ? month+1 : 1;
        int year2Use  = month < 12 ? year : year+1;
        Calendar myCalendar = new GregorianCalendar(year2Use, month2Use-1, 0);
        transfer.setTransactiedatum(myCalendar.getTime());
        transfers.add(transfer);
    }
}
