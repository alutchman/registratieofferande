package com.crmweb.xmldata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement(name="TRANSACTIES")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransferTransaction implements Serializable{

    private static final long serialVersionUID = 2961871788857623498L;


    @XmlElement(name="TransactiePeriod")
    private Date transactiedatum;

    @XmlElement(name="BetaalMethode")
    private String betaalMethode;

    @XmlElement(name="OfferType")
    private String offerType;

    @XmlElement(name="Bedrag")
    private double bedrag;

    public String getBetaalMethode() {
        return betaalMethode;
    }

    public void setBetaalMethode(String betaalMethode) {
        this.betaalMethode = betaalMethode;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public Date getTransactiedatum() {
        return transactiedatum;
    }

    public void setTransactiedatum(Date transactiedatum) {
        this.transactiedatum = transactiedatum;
    }

    public double getBedrag() {
        return bedrag;
    }

    public void setBedrag(double bedrag) {
        this.bedrag = bedrag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransferTransaction)) return false;

        TransferTransaction that = (TransferTransaction) o;

        if (Double.compare(that.getBedrag(), getBedrag()) != 0) return false;
        if (!getTransactiedatum().equals(that.getTransactiedatum())) return false;
        if (getBetaalMethode() != null ? !getBetaalMethode().equals(that.getBetaalMethode()) : that.getBetaalMethode() != null)
            return false;
        return getOfferType().equals(that.getOfferType());
    }

    @Override
    public int hashCode() {
        int result = getTransactiedatum().hashCode();
        result = 31 * result + getTransactiedatum().hashCode();
        result = 31 * result + getBetaalMethode().hashCode();
        return result;
    }
}
