package com.crmweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LocatieController {
    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value="locaties.html")
    public ModelAndView test(){
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        return new ModelAndView("locatie/handleLocaties",datamap);
    }
}
