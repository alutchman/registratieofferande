package com.crmweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LetterController {

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value="brief.html")
    public ModelAndView test(){
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        return new ModelAndView("brieven/index",datamap);
    }
}
