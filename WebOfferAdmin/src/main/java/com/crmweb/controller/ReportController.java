package com.crmweb.controller;

import com.crmweb.services.ReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class ReportController {
    @Autowired
    private ReportingService reportingService;

    @RequestMapping(value="report.html")
    public ModelAndView startup(){
        return new ModelAndView("report/index");
    }

    @RequestMapping(value="/pdfDynamic/page{\\d+}.html", method = RequestMethod.POST)
    public ModelAndView getContractPage(ModelMap modelMap, @RequestParam Map<String,String> params,
                                        HttpServletRequest request,
                                        @RequestParam("jspPage") String newView){
        reportingService.updateModelmap(modelMap,request, params);
        return new ModelAndView(newView, modelMap);
    }


    @RequestMapping(value="/pdfDynamic/brief.html", method = RequestMethod.POST)
    public ModelAndView getBrief(ModelMap modelMap, @RequestParam Map<String,String> params,
                                        HttpServletRequest request,
                                        @RequestParam("jspPage") String newView){
        reportingService.updateModelmap(modelMap,request, params);
        return new ModelAndView(newView, modelMap);
    }

    @RequestMapping(value="/report/PDF/{rapportID}.html", method = {RequestMethod.POST})
    public ModelAndView jaarTotalPersonsLocaties(ModelMap modelMap, @RequestParam Map<String,String> params,
                                                 HttpServletRequest request, @PathVariable("rapportID") String rapportID){
        reportingService.updateModelmap(modelMap,request, params);
        return new ModelAndView("pdfDynamic/"+rapportID, modelMap);
    }
}
