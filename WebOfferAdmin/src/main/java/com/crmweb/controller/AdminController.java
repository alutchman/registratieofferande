package com.crmweb.controller;

import com.crmweb.reports.ConceptContractSettings;
import com.shared.transfers.WebUserData;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;


@Controller
public class AdminController {
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ConceptContractSettings conceptContractSettings;

    @RequestMapping(value="register.html")
    public ModelAndView registratie(HttpSession session){
        if (session == null || session.getAttribute(WebUserData.class.getSimpleName()) == null){
            return new ModelAndView("redirect:/login.html");
        }

        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        return new ModelAndView("admin/payerAdmin",datamap);
    }

    @RequestMapping(value="settings.html")
    public ModelAndView settings(){
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        return new ModelAndView("auth/settings",datamap);
    }


    @RequestMapping(value = "signature.png", method = RequestMethod.GET, produces = MediaType.ALL_VALUE)
    public ResponseEntity<byte[]> imgFromMessage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            byte[] imgData = Files.readAllBytes(
                    Paths.get(getClass().getResource("/base_sign.png").toURI()));
            return getFromBytes(imgData);
        } catch (URISyntaxException e) {
            //byte[] encoded = Base64.getEncoder().encode("Hello".getBytes());
            String pathRedirect = request.getContextPath() + "/javax.faces.resource/signature.png.xhtml?ln=images";
            response.sendRedirect(pathRedirect);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    private ResponseEntity<byte[]> getFromBytes(byte[] input){
        final HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        headers.setContentLength(input.length);
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<>(input, headers, HttpStatus.OK);
    }

}
