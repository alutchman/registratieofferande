package com.crmweb.quartz;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseUkgrJob implements Job {
    public static final String JOB_NAME = "UKGR_JOB_%d";
    public static final String JOB_GROUP = "YEAR_ADMIN_REPORT%d";
    private static int jobmummer = 1;
    private static int priority = 10;

    private final Logger logger;
    private final JobKey jobKey;
    private final String jobName;
    private final String jobGroup;
    private final JobDetail jobDetail;

    private Trigger trigger;


    protected abstract String getCronTiming();


    public BaseUkgrJob() {
        this.jobGroup = String.format(JOB_GROUP, jobmummer);
        this.jobName  = String.format(JOB_NAME, jobmummer++);
        jobKey = new JobKey(jobName, jobGroup);

        logger = LoggerFactory.getLogger(getClass());

        jobDetail =
                JobBuilder.newJob(getClass())
                        .storeDurably()
                        .withIdentity(jobKey)
                        .build();
    }

    protected void setup(Scheduler scheduler){
        try {
            if (scheduler.checkExists(getJobKey())) {
                scheduler.deleteJob(getJobKey());
            }

            logger.info("SETTING UP Quartz Job {}, with group {} and timing {}", getJobName(), getJobGroup(), getCronTiming());

            TriggerKey tk1 = TriggerKey.triggerKey(getJobName(), getJobGroup());

            ScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(getCronTiming());

            trigger = TriggerBuilder.newTrigger()
                    .withIdentity(tk1)
                    .forJob(getJobDetail())
                    .withSchedule(scheduleBuilder)
                    .withPriority(priority--)
                    .startNow()
                    .build();

            scheduler.scheduleJob(getJobDetail(), getTrigger());
        } catch (SchedulerException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public JobKey getJobKey() {
        return jobKey;
    }

    public String getJobName() {
        return jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public JobDetail getJobDetail() {
        return jobDetail;
    }

    public Trigger getTrigger() {
        return trigger;
    }
}
