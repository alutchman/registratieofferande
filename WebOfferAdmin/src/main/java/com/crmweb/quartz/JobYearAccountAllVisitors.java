package com.crmweb.quartz;

import com.crmweb.services.QuartzReportingService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@DisallowConcurrentExecution
public class JobYearAccountAllVisitors extends BaseUkgrJob {

    @Autowired
    private QuartzReportingService quartzReportingService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        quartzReportingService.makeLocationReportCurrentYear();
    }

    @Override
    protected String getCronTiming() {
        return "0 0,15,30,45 * ? * *";
    }
}
