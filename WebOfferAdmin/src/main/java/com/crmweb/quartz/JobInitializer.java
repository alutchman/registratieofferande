package com.crmweb.quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;

@Component
public class JobInitializer {
    private static final Logger logger = LoggerFactory.getLogger(JobInitializer.class);

    @Autowired
    private Scheduler scheduler;


    @Autowired
    private List<BaseUkgrJob> jobs;

    @PostConstruct
    public void setup(){
        try {
            if (!scheduler.isStarted()) {
                scheduler.start();
                logger.info("scheduler.isStarted: {}", scheduler.isStarted());
            }
            jobs.forEach(this::setupBaseUkgrJobs);
        } catch (SchedulerException e) {
            logger.error(e.getMessage());
        }
    }

    @PreDestroy
    public void closeDown() throws SchedulerException {
        if (scheduler != null) {
            jobs.forEach(this::cleanupJob);
        }
        logger.info("::: SHUTTING DOWN Scheduler ::::");
        scheduler.shutdown();
    }

    private void cleanupJob(BaseUkgrJob job)  {
        try {
            if (scheduler.checkExists(job.getJobKey())) {
                logger.info("::::: Stopping Quartz Job {}, with group {}  :::::::", job.getJobName(), job.getJobGroup());
                scheduler.deleteJob(job.getJobKey());
            }
        } catch (SchedulerException e) {
            logger.error(e.getMessage());
        }
    }

    private void setupBaseUkgrJobs(BaseUkgrJob job) {
        job.setup(scheduler);
    }
}
