package com.crmweb.utils;

public class MaandTransacties {
    public static final String[] maandNames = {
            "UNDEF",
            "JAN","FEB", "MRT", "APR", "MEI", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEC"
    };
    private Double contant;
    private Double pin;
    private Double bank;
    private String maand;
    private int maandNummer;


    public MaandTransacties(int i) {
        this.maand = maandNames[i];
        this.maandNummer = i;
    }

    public int getMaandNummer() {
        return maandNummer;
    }

    public void setMaandNummer(int maandNummer) {
        this.maandNummer = maandNummer;
    }

    public String getMaand() {
        return maand;
    }

    public void setMaand(String maand) {
        this.maand = maand;
    }

    public Double getContant() {
        return contant;
    }

    public void setContant(Double contant) {
        this.contant = contant;
    }

    public Double getPin() {
        return pin;
    }

    public void setPin(Double pin) {
        this.pin = pin;
    }

    public Double getBank() {
        return bank;
    }

    public void setBank(Double bank) {
        this.bank = bank;
    }
}
