package com.crmweb.utils;

import java.io.Serializable;
import java.util.Objects;

public class ValueStringItem implements Serializable {
    private static final long serialVersionUID = -1335638773336146360L;

    private final int index;
    private final String label;
    private final String reportURl;
    private final int maxPerpage;

    private boolean directTable;



    public ValueStringItem(int index, String label, String reportURl, int maxPerpage) {
        this.index = index;
        this.label = label;
        this.reportURl = reportURl;
        this.maxPerpage = maxPerpage;
    }

    public ValueStringItem asDirectTable(){
        this.directTable = true;
        return this;
    }


    public boolean isDirectTable() {
        return directTable;
    }

    public int getIndex() {
        return index;
    }

    public String getLabel() {
        return label;
    }

    public String getReportURl() {
        return reportURl;
    }

    public int getMaxPerpage() {
        return maxPerpage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValueStringItem that = (ValueStringItem) o;
        return Objects.equals(getIndex(), that.getIndex());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIndex());
    }

    @Override
    public String toString() {
        return "ValueStringItem{" +
                "label='" + label + '\'' +
                '}';
    }
}
