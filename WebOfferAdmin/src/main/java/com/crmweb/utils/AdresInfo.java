package com.crmweb.utils;

import java.io.Serializable;

public class AdresInfo implements Serializable {

    private static final long serialVersionUID = -101925877073953868L;
    private String city;
    private String postalCode;
    private String street;
    private int houseNumber;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Override
    public String toString() {
        String result = "city = "+ city;
        result += ", street = " + street;
        result += ", postalCode = " + postalCode;
        result += ", houseNumber = " + houseNumber;
        return result;
    }
}
