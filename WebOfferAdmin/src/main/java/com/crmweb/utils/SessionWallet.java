package com.crmweb.utils;

import com.shared.transfers.LocatieData;
import com.shared.transfers.PayerData;
import com.shared.transfers.PostCodeData;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

public class SessionWallet implements Serializable {
    private static final long serialVersionUID = -7668683851388071762L;

    private List<LocatieData> locaties;

    private PostCodeData postCodeData;

    private PayerData currentPayer;

    private Long locatieId;

    private String searchAchternaam;

    private String searchVoornaam;

    public List<LocatieData> getLocaties() {
        return locaties;
    }

    public void setLocaties(List<LocatieData> locaties) {
        this.locaties = locaties;
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put(SessionWallet.class.getSimpleName(), this);

    }

    public PostCodeData getPostCodeData() {
        return postCodeData;
    }

    public void setPostCodeData(PostCodeData postCodeData) {
        this.postCodeData = postCodeData;
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put(SessionWallet.class.getSimpleName(), this);
    }

    public PayerData getCurrentPayer() {
        return currentPayer;
    }

    public void setCurrentPayer(PayerData currentPayer) {
        this.currentPayer = currentPayer;
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put(SessionWallet.class.getSimpleName(), this);
    }

    public Long getLocatieId() {
        return locatieId;
    }

    public void setLocatieId(Long locatieId) {
        this.locatieId = locatieId;
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put(SessionWallet.class.getSimpleName(), this);
    }

    public void setSearchActernaam(String achternaam) {
        this.searchAchternaam = achternaam;
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put(SessionWallet.class.getSimpleName(), this);

    }

    public String getSearchAchternaam() {
        return searchAchternaam;
    }

    public void setSearchVoornaam(String voornaam) {
        this.searchVoornaam = voornaam;
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put(SessionWallet.class.getSimpleName(), this);
    }

    public String getSearchVoornaam() {
        return searchVoornaam;
    }
}
