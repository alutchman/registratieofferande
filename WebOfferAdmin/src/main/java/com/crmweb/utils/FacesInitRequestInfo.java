package com.crmweb.utils;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class FacesInitRequestInfo {
    private final FacesContext facesContext;
    private final HttpServletRequest request;
    private final String sessionId;

    public FacesInitRequestInfo(FacesContext facesContext) {
       this.facesContext = facesContext;
        this.request =
                (HttpServletRequest) facesContext.getExternalContext().getRequest();
        this.sessionId = request.getHeader("Cookie");
    }

    public FacesContext getFacesContext() {
        return facesContext;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public String getSessionId() {
        return sessionId;
    }
}
