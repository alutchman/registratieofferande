package com.crmweb.utils;

import com.crmweb.reports.MiscCalculations;
import com.domain.ukgr.tables.JaarPerLocatie;
import com.domain.ukgr.tables.JaarPersonTransacties;
import com.domain.ukgr.tables.Locaties;
import com.domain.ukgr.tables.Payer;
import com.domain.ukgr.tables.Transacties;
import com.domain.ukgr.tables.Webuser;
import com.shared.transfers.LocatieData;
import com.shared.transfers.LocatiePayerYearTotal;
import com.shared.transfers.LocatieYearTotal;
import com.shared.transfers.PayerData;
import com.shared.transfers.TransactCorrection;
import com.shared.transfers.TransactieData;
import com.shared.transfers.UserLevel;
import com.shared.transfers.WebUserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

public final class TransferUtils {
    private static final Logger logger = LoggerFactory.getLogger(TransferUtils.class);

    private TransferUtils(){
        //===SINGLETON
    }

    public static LocatieData fromLocaties(Locaties locaties){
        LocatieData locatieData = new LocatieData();
        locatieData.setId(locaties.getId());
        locatieData.setNummer(locaties.getNummer());
        locatieData.setToevoeging(locaties.getToevoeging());
        locatieData.setPostcode(locaties.getPostcode());
        locatieData.setStraatnaam(locaties.getStraatnaam());
        locatieData.setTelefoon(locaties.getTelefoon());
        locatieData.setWoonplaats(locaties.getWoonplaats());
        locatieData.setUkgrcode(locaties.getUkgrcode());
        return locatieData;
    }



    public static WebUserData fromWebuser(Webuser webuser){
        WebUserData webUserData = new WebUserData();
        webUserData.setUserid(webuser.getUserid());
        webUserData.setAchternaam(webuser.getAchternaam());
        webUserData.setVoornaam(webuser.getVoornaam());

        webUserData.setCurrentlevel(UserLevel.REPORT);

        String groupType = webuser.getGroupnaam();
        for (UserLevel userLevel : UserLevel.values()) {
            if (userLevel.name().equals(groupType)) {
                webUserData.setCurrentlevel(userLevel);
            }
        }

        webUserData.setValidUser(true);
        return webUserData;
    }


    public static Payer toPayer(PayerData payerData) {
        Payer payer = new Payer();
        payer.setId(payerData.getId());
        payer.setTelefoon(payerData.getTelefoon());
        payer.setNummer(payerData.getNummer());
        payer.setToevoeging(payerData.getToevoeging());
        payer.setPostcode(payerData.getPostcode());
        payer.setAchternaam(payerData.getAchternaam());
        payer.setGeslacht(payerData.getGeslacht());
        payer.setStraatnaam(payerData.getStraatnaam());
        payer.setVoornaam(payerData.getVoornaam());
        payer.setWoonplaats(payerData.getWoonplaats());
        return payer;
    }

    public static PayerData fromPayer(Payer aPayer) {
        PayerData payerData = new PayerData();
        payerData.setId(aPayer.getId());
        payerData.setTelefoon(aPayer.getTelefoon());
        payerData.setNummer(aPayer.getNummer());
        payerData.setToevoeging(aPayer.getToevoeging());
        payerData.setPostcode(aPayer.getPostcode());
        payerData.setAchternaam(aPayer.getAchternaam());
        payerData.setGeslacht(aPayer.getGeslacht());
        payerData.setStraatnaam(aPayer.getStraatnaam());
        payerData.setVoornaam(aPayer.getVoornaam());
        payerData.setWoonplaats(aPayer.getWoonplaats());
        return payerData;

    }

    public static Locaties toLocaties(LocatieData newLocatie) {
        Locaties locatieData = new Locaties();
        locatieData.setId(newLocatie.getId());
        locatieData.setNummer(newLocatie.getNummer());
        locatieData.setToevoeging(newLocatie.getToevoeging());
        locatieData.setPostcode(newLocatie.getPostcode());
        locatieData.setStraatnaam(newLocatie.getStraatnaam());
        locatieData.setTelefoon(newLocatie.getTelefoon());
        locatieData.setWoonplaats(newLocatie.getWoonplaats());
        locatieData.setUkgrcode(newLocatie.getUkgrcode());
        return locatieData;
    }

    public static TransactieData fromTransacties(Transacties transacties){
        TransactieData transactieData = new TransactieData();
        transactieData.setId(transacties.getId());
        transactieData.setTransactiedatum(new Date(transacties.getTransactiedatum().getTime()));
        transactieData.setCreation_time(new Date(transacties.getCreation_time().getTime()));
        transactieData.setAdminlocatie(transacties.getAdminlocatie());
        transactieData.setBetaalmethode(transacties.getBetaalmethode());
        transactieData.setCategorie(transacties.getCategorie());
        transactieData.setBedrag(transacties.getBedrag());
        transactieData.setScanfile(transacties.getScanfile());

        Payer payer = transacties.getPayer();
        if (payer != null) {
            transactieData.setPayerNaam(String.format("%s %s", payer.getVoornaam(), payer.getAchternaam()));
            transactieData.setPayerPostcode(payer.getPostcode());
            String nummerInfo = String.valueOf(payer.getNummer());
            if (payer.getToevoeging() != null) {
                nummerInfo +=  payer.getToevoeging();
            }
            transactieData.setPayerAdres(String.format("%s %s", payer.getStraatnaam(), nummerInfo));
            transactieData.setPayerWoonplaats(payer.getWoonplaats());
        }
        return transactieData;
    }


    public static TransactCorrection fromTransactiesCombined(Transacties transacties){
        TransactCorrection transactieData = new TransactCorrection();
        transactieData.setTransactieData(fromTransacties(transacties));
        transactieData.setPayerData(fromPayer(transacties.getPayer()));
        return transactieData;
    }

    public static LocatieYearTotal fromJaarPerLocatie(JaarPerLocatie entity){
        LocatieYearTotal locatieYearTotal = new LocatieYearTotal();
        if (entity != null) {
            locatieYearTotal.setJaar(entity.getJaar());
            locatieYearTotal.setNummer(entity.getNummer());
            locatieYearTotal.setToevoeging(entity.getToevoeging());
            locatieYearTotal.setOntvangen(entity.getOntvangen());
            locatieYearTotal.setStraatnaam(entity.getStraatnaam());
            locatieYearTotal.setWoonplaats(entity.getWoonplaats());
            locatieYearTotal.setUkgrcode(entity.getUkgrcode());
            locatieYearTotal.setPostcode(entity.getPostcode());
        }
        return locatieYearTotal;
    }


    public static LocatiePayerYearTotal fromJaarPersonTransacties(JaarPersonTransacties entity){
        LocatiePayerYearTotal locatiePayerYearTotal = new LocatiePayerYearTotal();
        if (entity != null) {
            locatiePayerYearTotal.setJaar(entity.getJaar());
            locatiePayerYearTotal.setVoornaam(entity.getVoornaam());
            locatiePayerYearTotal.setAchternaam(entity.getAchternaam());
            locatiePayerYearTotal.setNummer(entity.getNummer());
            locatiePayerYearTotal.setToevoeging(entity.getToevoeging());
            locatiePayerYearTotal.setOntvangen(entity.getOntvangen());
            locatiePayerYearTotal.setStraatnaam(entity.getStraatnaam());
            locatiePayerYearTotal.setWoonplaats(entity.getWoonplaats());
            locatiePayerYearTotal.setAdminlocatie(entity.getAdminlocatie());
            locatiePayerYearTotal.setPostcode(entity.getPostcode().toUpperCase());
        }
        return locatiePayerYearTotal;
    }


    public static void fetchHtmlData(List<String> htmlData , Map<String, String> params, String url, String sessionID){
        try {
            ResponseEntity<String> result = MiscCalculations.postHashMapToUrl(url, params, sessionID);
            htmlData.add(result.getBody());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public static void handleStreamExchange(InputStream fileInputStream, OutputStream outputStream) throws IOException {
        byte[] bytesBuffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = fileInputStream.read(bytesBuffer)) > 0) {
            outputStream.write(bytesBuffer, 0, bytesRead);
        }
    }

    public static int calculatePages(int aantal, int maxPerPage){
        int pages = (int) Math.ceil(((double) aantal)/maxPerPage);
        if (pages == 0 ) {
            pages = 1;
        }
        return pages;
    }
}
