SET SQL_SAFE_UPDATES=0;
UPDATE transacties SET transactiedatum = DATE_FORMAT(transactiedatum, '2020-%m-%d')
WHERE year(transactiedatum) = 2018;
SET SQL_SAFE_UPDATES=1;


--swap data
SET SQL_SAFE_UPDATES=0;
update ukgradmin.transacties set ukgradmin.transacties.payerid=1000 where ukgradmin.transacties.payerid=1;
SET SQL_SAFE_UPDATES=1;