USE ukgradmin;

--
-- Table structure for table locaties
--

DROP TABLE IF EXISTS locaties;

CREATE TABLE locaties (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  straatnaam varchar(60) NOT NULL,
  nummer int(11) NOT NULL,
  toevoeging varchar(4) DEFAULT NULL,
  woonplaats varchar(25) NOT NULL,
  postcode varchar(8) DEFAULT NULL,
  telefoon varchar(20) DEFAULT NULL,
  ukgrcode int(11) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY ukgrcode_UNIQUE (ukgrcode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table payer
--

DROP TABLE IF EXISTS payer;
CREATE TABLE payer (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  voornaam varchar(25) NOT NULL,
  achternaam varchar(50) NOT NULL,
  geslacht varchar(10) NOT NULL,
  nummer int(11) NOT NULL,
  toevoeging varchar(4) DEFAULT NULL,
  woonplaats varchar(25) NOT NULL,
  postcode varchar(8) DEFAULT NULL,
  straatnaam varchar(60) NOT NULL,
  telefoon varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS postcodes;
CREATE TABLE postcodes (
                         PostCode varchar(6) NOT NULL,
                         Straat varchar(50) NOT NULL,
                         Plaats varchar(45) DEFAULT NULL,
                         Gemeente varchar(45) DEFAULT NULL,
                         Provincie varchar(45) DEFAULT NULL,
                         PRIMARY KEY (PostCode,Straat)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

--
-- Table structure for table transacties
--
DROP TABLE IF EXISTS transacties;
CREATE TABLE transacties (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  payerid bigint(20) NOT NULL,
  bedrag double NOT NULL,
  betaalmethode varchar(25) NOT NULL DEFAULT 'PIN',
  categorie varchar(25) NOT NULL DEFAULT 'OFFERANDE',
  transactiedatum datetime NOT NULL,
  adminlocatie bigint(20) DEFAULT NULL,
  webuserid varchar(15) DEFAULT NULL,
  creation_time     DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  scanfile varchar(80) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table webuser
--

DROP TABLE IF EXISTS webuser;
CREATE TABLE webuser (
  userid varchar(15) NOT NULL,
  wachtwoord varchar(50) NOT NULL,
  voornaam varchar(25) NOT NULL,
  achternaam varchar(50) NOT NULL,
  groupnaam varchar(15) DEFAULT NULL,
  PRIMARY KEY (userid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE OR REPLACE VIEW jaarmethodpay AS
  SELECT
  DISTINCT
    A.payerid,
    A.adminlocatie,
    A.betaalmethode,
    YEAR(A.transactiedatum) AS JAAR,
    SUM(A.bedrag) AS totaal
   FROM transacties A
  GROUP BY
    A.payerid,
    A.adminlocatie,
    A.betaalmethode,
    YEAR(A.transactiedatum)
ORDER BY
  YEAR(A.transactiedatum) DESC,
  A.betaalmethode ASC;


CREATE OR REPLACE VIEW maandmethodpay AS
  SELECT
  DISTINCT
    A.payerid,
    A.adminlocatie,
    A.betaalmethode,
    YEAR(A.transactiedatum) AS JAAR,
    MONTH(A.transactiedatum) AS MAAND,
    SUM(A.bedrag) AS totaal
   FROM transacties A
  GROUP BY A.payerid, A.adminlocatie,A.betaalmethode, YEAR(A.transactiedatum), MONTH(A.transactiedatum)
ORDER BY YEAR(A.transactiedatum) DESC, MONTH(A.transactiedatum) ASC,A.betaalmethode ASC;



CREATE OR REPLACE VIEW transact_year AS
SELECT
  DISTINCT
  A.adminlocatie,
  year(transactiedatum) as jaar,
  sum(bedrag) as ontvangen
FROM transacties A
GROUP BY A.adminlocatie, year(transactiedatum);

CREATE OR REPLACE VIEW jaarperlocatie AS
SELECT B.ukgrcode, A.jaar, A.ontvangen, B.straatnaam, B.nummer,B.toevoeging, B.woonplaats,B.postcode FROM transact_year A
                                                                                                            INNER JOIN locaties B ON B.id=A.adminlocatie
ORDER BY B.ukgrcode ASC, A.jaar DESC;


CREATE OR REPLACE VIEW jaarperlocatie AS
SELECT B.ukgrcode, A.jaar, A.ontvangen, B.straatnaam, B.nummer,B.toevoeging, B.woonplaats,B.postcode FROM
  (
    SELECT
      DISTINCT
      A.adminlocatie,
      year(transactiedatum) as jaar,
      sum(bedrag) as ontvangen
    FROM transacties A
    GROUP BY A.adminlocatie, year(transactiedatum)
  ) A
    INNER JOIN locaties B ON B.id=A.adminlocatie
ORDER BY B.ukgrcode ASC, A.jaar DESC;


CREATE OR REPLACE VIEW transact_year_personpayer AS
SELECT
  DISTINCT
  A.adminlocatie,
  A.payerid,
  year(transactiedatum) as jaar,
  sum(bedrag) as ontvangen
FROM transacties A
GROUP BY A.adminlocatie,A.payerid, year(transactiedatum);


CREATE OR REPLACE VIEW year_personpayer AS
SELECT
  A.adminlocatie,
  A.payerid,
  B.voornaam,
  B.achternaam,
  A.jaar ,
  A.ontvangen ,
  B.straatnaam,
  B.nummer,
  B.toevoeging,
  UPPER(B.postcode) as postcode,
  B.woonplaats
FROM transact_year_personpayer A
       inner JOIN payer B on B.id=A.payerid
ORDER BY A.adminlocatie ASC, B.achternaam ASC, B.voornaam ASC, UPPER(B.postcode)  ASC, B.nummer ASC;



INSERT INTO locaties
(straatnaam,nummer,toevoeging,woonplaats,postcode,telefoon,ukgrcode)
VALUES
('Fruitweg',4,NULL,'Den Haag','2525KH','0703888442',1),
('Diergaardesingel',70,NULL,'Rotterdam','3014AE',NULL,2),
('Westersingel',42,NULL,'Rotterdam','3014GT',NULL,3),
('Borneostraat',40,NULL,'Amsterdam Oost','1094CL',NULL,4),
('Jan Evertsenstraat',18,NULL,'Amsterdam West','1056EC',NULL,5),
('Piet Bakkerhove',7,NULL,'Zoetermeer','2717ZA',NULL,6),
('Grote Markt',27,NULL,'Almere','1315JA',NULL,7),
('Oudegracht',60,NULL,'Utrecht','3511AS',NULL,8),
('Boschdijk',258,NULL,'Eindhoven','5612HJ',NULL,9);


DROP TABLE IF EXISTS reports;
CREATE TABLE IF NOT EXISTS reports (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  report_type int(11) NOT NULL,
  report_year int(11) NOT NULL,
  report_item_id bigint(20) NOT NULL, -- prim key locid, payid
  report_loc_code int(11) NOT NULL,
  report_created datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;