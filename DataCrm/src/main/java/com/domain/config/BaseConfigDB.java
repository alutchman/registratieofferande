package com.domain.config;

import com.jndi.env.JndiTemplateDataBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

public class BaseConfigDB {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public JndiTemplateDataBase fetchJndiTemplate(){
        JndiTemplateDataBase template = new JndiTemplateDataBase();
        return template;
    }



    public DataSource getDataSource(String jndiDataSource)  {
        try {
            JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
            dsLookup.setResourceRef(true);
            return dsLookup.getDataSource(jndiDataSource);
        } catch (Exception e) {
            logger.error("Datasource Failure ...", e);
        }
        return null;
    }



    public JdbcTemplate createJdbCTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }


    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JndiTemplateDataBase template) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[] {"com.domain.ukgr.tables" });

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(template.getEnvironment());

        return em;
    }


    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }


}
