package com.domain.config;


import com.jndi.env.JndiTemplateDataBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@ComponentScan(
        basePackages = {"com.domain.ukgr"}
)
@EnableJpaRepositories(basePackages = {
        "com.domain.ukgr.repo"
})
@EnableTransactionManagement
public class HibernateConfig extends BaseConfigDB {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static final String DATA_SOURCE_NAME = "jdbc/ukgrDB";


    @Bean
    public JndiTemplateDataBase fetchJndiTemplate(){
        return super.fetchJndiTemplate();
    }


    @Bean
    public DataSource getDataSource()  {
        return this.getDataSource(DATA_SOURCE_NAME);
    }


    @Bean
    @Autowired
    public JdbcTemplate createJdbCTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }


    @Bean
    @Autowired
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JndiTemplateDataBase template) {
        return super.entityManagerFactory(dataSource, template);
    }


    @Bean
    @Autowired
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        return super.transactionManager(emf);
    }


}
