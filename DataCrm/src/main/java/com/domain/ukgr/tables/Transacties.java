package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;
import com.shared.transfers.BetaalMethode;
import com.shared.transfers.OfferType;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;


@NamedQueries({
        @NamedQuery(
                name = Transacties.PAYERVOORJAAR,
                query = "from Transacties e where e.payer=:payer and YEAR(e.transactiedatum)=:jaar and e.adminlocatie = :adminlocatie" +
                        " ORDER BY e.transactiedatum DESC "
        ),
        @NamedQuery(
                name = Transacties.CORRECTIONSEARCH,
                query = "from Transacties e where " +
                        " e.payer.postcode like :postcode " +
                        " and e.creation_time>=:aanmaakdatumstart and e.creation_time<=:aanmaakdatumend " +
                        " and e.webuserid=:webuserid and e.payer.voornaam like :voornaam " +
                        " and e.payer.achternaam like :achternaam" +
                        " ORDER BY e.creation_time DESC, e.payer.achternaam ASC, e.payer.voornaam ASC "
        )
})

@Entity
@Table(name = "transacties")
public class Transacties extends BaseEntityId {
  private static final long serialVersionUID = 3241813794169885008L;
  public final static String PAYERVOORJAAR = "findJaarRecordsPerJaarPayer";
  public final static String CORRECTIONSEARCH = "findTransactiesRecordsToCorrect";


  @ManyToOne (fetch=FetchType.LAZY)
  @JoinColumn(name = "payerid")
  private Payer payer;


  @Column
  private Double bedrag;

  @Enumerated(EnumType.STRING)
  @Column
  private BetaalMethode betaalmethode;

  @Enumerated(EnumType.STRING)
  @Column
  private OfferType categorie;

  @Column
  private java.sql.Timestamp transactiedatum;

  @Column
  private Long adminlocatie;

  @Column
  private String webuserid;

  @Column(length = 80)
  private String scanfile;

  @Column
  @CreationTimestamp
  private java.sql.Timestamp creation_time;

  public Double getBedrag() {
    return bedrag;
  }

  public void setBedrag(Double bedrag) {
    this.bedrag = bedrag;
  }

  public BetaalMethode getBetaalmethode() {
    return betaalmethode;
  }

  public void setBetaalmethode(BetaalMethode betaalmethode) {
    this.betaalmethode = betaalmethode;
  }

  public java.sql.Timestamp getTransactiedatum() {
    return transactiedatum;
  }

  public void setTransactiedatum(java.sql.Timestamp transactiedatum) {
    this.transactiedatum = transactiedatum;
  }

  public Long getAdminlocatie() {
    return adminlocatie;
  }

  public void setAdminlocatie(Long adminlocatie) {
    this.adminlocatie = adminlocatie;
  }

  public String getWebuserid() {
    return webuserid;
  }

  public void setWebuserid(String webuserid) {
    this.webuserid = webuserid;
  }

  public OfferType getCategorie() {
    return categorie;
  }

  public void setCategorie(OfferType categorie) {
    this.categorie = categorie;
  }

  public Timestamp getCreation_time() {
    return creation_time;
  }

  public String getScanfile() {
    return scanfile;
  }

  public void setScanfile(String scanfile) {
    this.scanfile = scanfile;
  }

  public Payer getPayer() {
    return payer;
  }

  public void setPayer(Payer payer) {
    this.payer = payer;
  }

  public void setCreation_time(Timestamp creation_time) {
    this.creation_time = creation_time;
  }
}
