package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntity;
import com.shared.transfers.WebUserData;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "webuser")
public class Webuser extends BaseEntity {
  private static final long serialVersionUID = 1128106384751953049L;
  @Id
  @Column( name = "userid",nullable = false, length = 15, unique = true)
  private String userid;

  @Column( name = "wachtwoord",nullable = false, length = 50)
  private String wachtwoord;

  @Column( name = "voornaam",nullable = false, length = 25)
  private String voornaam;

  @Column( name = "achternaam",nullable = false, length = 50)
  private String achternaam;

  @Column( name = "groupnaam",nullable = false, length = 15)
  private String groupnaam;

  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }

  public String getWachtwoord() {
    return wachtwoord;
  }

  public void setWachtwoord(String wachtwoord) {
    this.wachtwoord = wachtwoord;
  }

  public String getVoornaam() {
    return voornaam;
  }

  public void setVoornaam(String voornaam) {
    this.voornaam = voornaam;
  }

  public String getAchternaam() {
    return achternaam;
  }

  public void setAchternaam(String achternaam) {
    this.achternaam = achternaam;
  }

  public String getGroupnaam() {
    return groupnaam;
  }

  public void setGroupnaam(String groupnaam) {
    this.groupnaam = groupnaam;
  }

}
