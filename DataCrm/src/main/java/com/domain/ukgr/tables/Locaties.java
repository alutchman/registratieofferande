package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
        name = "locaties",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"ukgrcode"})}
        )
@NamedQueries({
        @NamedQuery(
                name = Locaties.PAYERVOORJAARLIST,
                query = "from Locaties e where e.id NOT IN " +
                        "( select f.reportItemId from  ReportQuartz f " +
                        "     WHERE " +
                        "      f.reportType = :repType and " +
                        "      f.reportYear = :jaar " +
                        ") " +
                        " ORDER BY e.id ASC "
        ),
        @NamedQuery(
                name = Locaties.UKGRCODEUSEDBYOTHERLOC,
                query = "from Locaties e where e.ukgrcode = :ukgrcode and e.id != :id"+
                        " ORDER BY e.straatnaam ASC "
        ),
        @NamedQuery(
                name = Locaties.ORDERBYWOONPLSTRAAT,
                query = "from Locaties e"+
                        " ORDER BY e.woonplaats ASC, e.straatnaam ASC "
        ),
        @NamedQuery(
                name = Locaties.ORDERBYUKGRCODE,
                query = "from Locaties e"+
                        " ORDER BY e.ukgrcode ASC"
        ),
        @NamedQuery(
                name = Locaties.PAYERLOCATIESELECTLIST,
                query = "from Locaties e where e.id IN " +
                        "( select f.adminlocatie from  JaarPersonTransacties f " +
                        "     WHERE " +
                        "      f.payerid = :payerid " +
                        ") " +
                        " ORDER BY e.ukgrcode ASC "
        )
})
public class Locaties extends BaseEntityId {
  private static final long serialVersionUID = -8648797440927902796L;
  public final static String PAYERVOORJAARLIST = "Locaties.allAtLocatieForYear";
  public final static String PAYERLOCATIESELECTLIST = "Locaties.usedLocationByPayer";
  public final static String UKGRCODEUSEDBYOTHERLOC = "Locaties.ukgrCodeUsedByOther";
  public final static String ORDERBYWOONPLSTRAAT = "Locaties.receiveOrderedWoonpl";
  public final static String ORDERBYUKGRCODE = "Locaties.receiveOrderedUkgrCode";

  @Column( name = "straatnaam",nullable = false, length = 60)
  private String straatnaam;

  @Column( name = "nummer",nullable = false)
  private Integer nummer;

  @Column( name = "toevoeging")
  private String toevoeging;

  @Column( name = "woonplaats",nullable = false, length = 25)
  private String woonplaats;

  @Column( name = "postcode",nullable = false, length = 8)
  private String postcode;

  @Column( name = "telefoon", length = 20)
  private String telefoon;

  @Column( name = "ukgrcode",nullable = false)
  private Integer ukgrcode;

  public String getStraatnaam() {
    return straatnaam;
  }

  public void setStraatnaam(String straatnaam) {
    this.straatnaam = straatnaam;
  }

  public Integer getNummer() {
    return nummer;
  }

  public void setNummer(Integer nummer) {
    this.nummer = nummer;
  }

  public String getWoonplaats() {
    return woonplaats;
  }

  public void setWoonplaats(String woonplaats) {
    this.woonplaats = woonplaats;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getTelefoon() {
    return telefoon;
  }

  public void setTelefoon(String telefoon) {
    this.telefoon = telefoon;
  }

  public Integer getUkgrcode() {
    return ukgrcode;
  }

  public void setUkgrcode(Integer ukgrcode) {
    this.ukgrcode = ukgrcode;
  }

  public String getToevoeging() {
    return toevoeging;
  }

  public void setToevoeging(String toevoeging) {
    this.toevoeging = toevoeging;
  }
}
