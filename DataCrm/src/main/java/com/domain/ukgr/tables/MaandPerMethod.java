package com.domain.ukgr.tables;

import com.domain.ukgr.idclassdefs.MaandBetallMethodeId;
import com.domain.ukgr.entity.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = MaandPerMethod.PAYERJAAR,
                query = "from MaandPerMethod e where e.payerid=:payerid and e.jaar=:jaar and e.adminlocatie = :adminlocatie" +
                        " ORDER BY e.maand ASC "
        )
})

@Entity
@IdClass(MaandBetallMethodeId.class)
@Immutable
@Table(name = "maandmethodpay")
public class MaandPerMethod extends BaseEntity {
    private static final long serialVersionUID = -8623547735254549120L;
    public final static String PAYERJAAR = "MaandPerMethod.findByMethodPayerInMaandPerJaar";

    private long payerid;
    private long adminlocatie;
    private String betaalmethode;
    private Integer jaar;
    private Integer maand;
    private Double totaal;

    @Id
    @Column(name = "payerid")
    public long getPayerid() {
        return payerid;
    }

    public void setPayerid(long payerid) {
        this.payerid = payerid;
    }

    @Id
    @Column(name = "adminlocatie")
    public long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    @Id
    @Column(name = "betaalmethode")
    public String getBetaalmethode() {
        return betaalmethode;
    }

    public void setBetaalmethode(String betaalmethode) {
        this.betaalmethode = betaalmethode;
    }

    @Id
    @Column(name = "JAAR")
    public Integer getJaar() {
        return jaar;
    }

    public void setJaar(Integer jaar) {
        this.jaar = jaar;
    }

    @Id
    @Column(name = "MAAND")
    public Integer getMaand() {
        return maand;
    }

    public void setMaand(Integer maand) {
        this.maand = maand;
    }

    @Column(name = "totaal")
    public Double getTotaal() {
        return totaal;
    }

    public void setTotaal(Double totaal) {
        this.totaal = totaal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MaandPerMethod that = (MaandPerMethod) o;

        if (payerid != that.payerid) return false;
        if (betaalmethode != null ? !betaalmethode.equals(that.betaalmethode) : that.betaalmethode != null)
            return false;
        if (jaar != null ? !jaar.equals(that.jaar) : that.jaar != null) return false;
        if (maand != null ? !maand.equals(that.maand) : that.maand != null) return false;
        if (totaal != null ? !totaal.equals(that.totaal) : that.totaal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (payerid ^ (payerid >>> 32));
        result = 31 * result + (betaalmethode != null ? betaalmethode.hashCode() : 0);
        result = 31 * result + (jaar != null ? jaar.hashCode() : 0);
        result = 31 * result + (maand != null ? maand.hashCode() : 0);
        result = 31 * result + (totaal != null ? totaal.hashCode() : 0);
        return result;
    }
}
