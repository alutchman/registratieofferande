package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "payer")
public class Payer extends BaseEntityId {
  private static final long serialVersionUID = 3704489693275737246L;

  @Column( name = "voornaam",nullable = false, length = 25)
  private String voornaam;

  @Column( name = "achternaam",nullable = false, length = 50)
  private String achternaam;

  @Column( name = "geslacht",nullable = false, length = 10)
  private String geslacht;

  @Column( name = "nummer")
  private Integer nummer;

  @Column( name = "toevoeging")
  private String toevoeging;

  @Column( name = "woonplaats",nullable = false, length = 25)
  private String woonplaats;

  @Column( name = "postcode",nullable = false, length = 8)
  private String postcode;

  @Column( name = "straatnaam",nullable = false, length = 60)
  private String straatnaam;

  @Column( name = "telefoon", length = 20)
  private String telefoon;

  public String getVoornaam() {
    return voornaam;
  }

  public void setVoornaam(String voornaam) {
    this.voornaam = voornaam;
  }

  public String getAchternaam() {
    return achternaam;
  }

  public void setAchternaam(String achternaam) {
    this.achternaam = achternaam;
  }

  public String getGeslacht() {
    return geslacht;
  }

  public void setGeslacht(String geslacht) {
    this.geslacht = geslacht;
  }

  public Integer getNummer() {
    return nummer;
  }

  public void setNummer(Integer nummer) {
    this.nummer = nummer;
  }

  public String getWoonplaats() {
    return woonplaats;
  }

  public void setWoonplaats(String woonplaats) {
    this.woonplaats = woonplaats;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getTelefoon() {
    return telefoon;
  }

  public void setTelefoon(String telefoon) {
    this.telefoon = telefoon;
  }

  public String getStraatnaam() {
    return straatnaam;
  }

  public void setStraatnaam(String straatnaam) {
    this.straatnaam = straatnaam;
  }

  public String getToevoeging() {
    return toevoeging;
  }

  public void setToevoeging(String toevoeging) {
    this.toevoeging = toevoeging;
  }
}
