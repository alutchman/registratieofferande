package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "reports")
public class ReportQuartz extends BaseEntityId {
    private static final long serialVersionUID = 3805659621975591003L;

    @Column( name = "report_type", nullable = false)
    private int reportType;

    @Column( name = "report_year", nullable = false)
    private int reportYear;

    @Column( name = "report_item_id", nullable = false)
    private Long reportItemId;

    @Column( name = "report_loc_code", nullable = false)
    private int reportLocation;

    @Column( name = "report_created", nullable = false)
    private LocalDateTime reportCreated;

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public int getReportYear() {
        return reportYear;
    }

    public void setReportYear(int reportYear) {
        this.reportYear = reportYear;
    }

    public Long getReportItemId() {
        return reportItemId;
    }

    public void setReportItemId(Long reportItemId) {
        this.reportItemId = reportItemId;
    }

    public int getReportLocation() {
        return reportLocation;
    }

    public void setReportLocation(int reportLocation) {
        this.reportLocation = reportLocation;
    }

    public LocalDateTime getReportCreated() {
        return reportCreated;
    }

    public void setReportCreated(LocalDateTime reportCreated) {
        this.reportCreated = reportCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReportQuartz)) return false;
        ReportQuartz that = (ReportQuartz) o;
        return getReportYear() == that.getReportYear() &&
                getReportType() == that.getReportType() &&
                getReportLocation() == that.getReportLocation() &&
                getReportItemId().equals(that.getReportItemId()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getReportLocation(), getReportYear(), getReportType());
    }
}
