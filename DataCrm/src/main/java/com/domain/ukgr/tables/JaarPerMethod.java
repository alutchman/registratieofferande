package com.domain.ukgr.tables;

import com.domain.ukgr.idclassdefs.JaarBetaalMethodeId;
import com.domain.ukgr.entity.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;


@NamedQueries({
        @NamedQuery(
                name = JaarPerMethod.PAYERJAAR,
                query = "from JaarPerMethod e where e.payerid=:payerid and e.jaar=:jaar and e.adminlocatie=:adminlocatie"
        ),
        @NamedQuery(
                name = JaarPerMethod.PAYERJAAR_NOCASH,
                query = "from JaarPerMethod e where e.payerid=:payerid and e.jaar=:jaar and e.adminlocatie=:adminlocatie and e.betaalmethode != :betaalmethode"
        )
})
@Entity
@IdClass(JaarBetaalMethodeId.class)
@Immutable
@Table(name = "jaarmethodpay")
public class JaarPerMethod extends BaseEntity {
    private static final long serialVersionUID = -3129684248105631216L;
    public final static String PAYERJAAR = "JaarPerMethod.findByMethodPayerJaarPerJaar";
    public final static String PAYERJAAR_NOCASH = "JaarPerMethod.findByMethodPayerJaarPerJaarExclContant";

    private long payerid;
    private long adminlocatie;
    private String betaalmethode;
    private Integer jaar;
    private Double totaal;

    @Id
    @Column(name = "payerid")
    public long getPayerid() {
        return payerid;
    }

    public void setPayerid(long payerid) {
        this.payerid = payerid;
    }


    @Id
    @Column(name = "adminlocatie")
    public long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    @Id
    @Column(name = "betaalmethode")
    public String getBetaalmethode() {
        return betaalmethode;
    }

    public void setBetaalmethode(String betaalmethode) {
        this.betaalmethode = betaalmethode;
    }

    @Id
    @Column(name = "JAAR")
    public Integer getJaar() {
        return jaar;
    }

    public void setJaar(Integer jaar) {
        this.jaar = jaar;
    }

    @Column(name = "totaal")
    public Double getTotaal() {
        return totaal;
    }

    public void setTotaal(Double totaal) {
        this.totaal = totaal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JaarPerMethod that = (JaarPerMethod) o;

        if (payerid != that.payerid) return false;
        if (betaalmethode != null ? !betaalmethode.equals(that.betaalmethode) : that.betaalmethode != null)
            return false;
        if (jaar != null ? !jaar.equals(that.jaar) : that.jaar != null) return false;
        if (totaal != null ? !totaal.equals(that.totaal) : that.totaal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (payerid ^ (payerid >>> 32));
        result = 31 * result + (jaar != null ? jaar.hashCode() : 0);
        return result;
    }
}
