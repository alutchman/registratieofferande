package com.domain.ukgr.tables;


import com.domain.ukgr.entity.BaseEntity;
import com.domain.ukgr.idclassdefs.JaarPersonTransActId;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Objects;


@NamedQueries({
        @NamedQuery(
                name = JaarPersonTransacties.POSTCODETRANSACTJAAR,
                query = "from JaarPersonTransacties e where e.adminlocatie=:adminlocatie and e.jaar=:jaar " +
                        "ORDER BY e.postcode ASC, e.nummer ASC "
        ),
        @NamedQuery(
                name = JaarPersonTransacties.PERSONTRANSACTJAAR,
                query = "from JaarPersonTransacties e where e.adminlocatie=:adminlocatie and e.jaar=:jaar " +
                        "ORDER BY e.achternaam ASC, e.voornaam ASC,  e.postcode ASC, e.nummer ASC"
        )
})
@Entity
@IdClass(JaarPersonTransActId.class)
@Immutable
@Table(name = "year_personpayer")
public class JaarPersonTransacties extends BaseEntity {
    private static final long serialVersionUID = -2607177765216957680L;
    public final static String PERSONTRANSACTJAAR = "personTransActJaarTotaal";
    public final static String POSTCODETRANSACTJAAR = "postcodeTransActJaarTotaal";

    @Id
    private Long adminlocatie;

    @Id
    private Long payerid;

    @Id
    private Integer jaar ;

    private String voornaam;

    private String achternaam;

    private Double  ontvangen ;

    private String straatnaam;

    private Integer nummer;

    private String toevoeging;

    private String postcode;

    private String woonplaats;

    public Long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(Long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    public Long getPayerid() {
        return payerid;
    }

    public void setPayerid(Long payerid) {
        this.payerid = payerid;
    }

    public Integer getJaar() {
        return jaar;
    }

    public void setJaar(Integer jaar) {
        this.jaar = jaar;
    }

    public Double getOntvangen() {
        return ontvangen;
    }

    public void setOntvangen(Double ontvangen) {
        this.ontvangen = ontvangen;
    }

    public String getStraatnaam() {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam) {
        this.straatnaam = straatnaam;
    }

    public Integer getNummer() {
        return nummer;
    }

    public void setNummer(Integer nummer) {
        this.nummer = nummer;
    }

    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }


    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JaarPersonTransacties that = (JaarPersonTransacties) o;
        return getAdminlocatie().equals(that.getAdminlocatie()) &&
                getPayerid().equals(that.getPayerid()) &&
                getJaar().equals(that.getJaar());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAchternaam(), getJaar());
    }
}
