package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntity;
import com.domain.ukgr.idclassdefs.JaarLocatieId;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Objects;

@NamedQueries({
        @NamedQuery(
                name = JaarPerLocatie.LOCATIEJAAR,
                query = "from JaarPerLocatie e where e.ukgrcode=:ukgrcode and e.jaar=:jaar"
        ),
        @NamedQuery(
                name = JaarPerLocatie.ALLLOCATIEJAAR,
                query = "from JaarPerLocatie e where  e.jaar=:jaar"
        )
})

@Entity
@IdClass(JaarLocatieId.class)
@Immutable
@Table(name = "jaarperlocatie")
public class JaarPerLocatie extends BaseEntity {
    private static final long serialVersionUID = 5265161852947198134L;
    public final static String LOCATIEJAAR = "findForLocatieerJaar";
    public final static String ALLLOCATIEJAAR = "findForAllLocatieerJaar";


    @Id
    private int ukgrcode;

    @Id
    private int jaar;

    private Double ontvangen;

    private String woonplaats;

    private String postcode;

    private String straatnaam;

    private int nummer;

    private String toevoeging;

    public int getUkgrcode() {
        return ukgrcode;
    }

    public void setUkgrcode(int ukgrcode) {
        this.ukgrcode = ukgrcode;
    }

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public Double getOntvangen() {
        return ontvangen;
    }

    public void setOntvangen(Double ontvangen) {
        this.ontvangen = ontvangen;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getStraatnaam() {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam) {
        this.straatnaam = straatnaam;
    }

    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JaarPerLocatie that = (JaarPerLocatie) o;
        return getUkgrcode() == that.getUkgrcode() &&
                getJaar() == that.getJaar();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUkgrcode());
    }
}
