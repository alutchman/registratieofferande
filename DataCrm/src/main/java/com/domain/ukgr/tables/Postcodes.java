package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntity;
import com.domain.ukgr.idclassdefs.JaarOverzichtId;
import com.domain.ukgr.idclassdefs.PostCodeStraat;
import com.shared.transfers.PostCodeData;


import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = "findByPostcode",
                query = "from Postcodes p where p.postcode = :postcode"
        )
})
@Entity
@IdClass(PostCodeStraat.class)
@Table(name = "postcodes")
public class Postcodes extends BaseEntity {

  private static final long serialVersionUID = 1705523150934689785L;
  @Id
  @Column( name = "PostCode",nullable = false, length = 6)
  private String postcode;

  @Id
  @Column( name = "Straat",nullable = false, length = 50)
  private String straat;

  @Column( name = "Plaats",nullable = false, length = 45)
  private String plaats;

  @Column( name = "Gemeente", length = 45)
  private String gemeente;

  @Column( name = "Provincie", length = 45)
  private String provincie;

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getStraat() {
    return straat;
  }

  public void setStraat(String straat) {
    this.straat = straat;
  }

  public String getPlaats() {
    return plaats;
  }

  public void setPlaats(String plaats) {
    this.plaats = plaats;
  }

  public String getGemeente() {
    return gemeente;
  }

  public void setGemeente(String gemeente) {
    this.gemeente = gemeente;
  }

  public String getProvincie() {
    return provincie;
  }

  public void setProvincie(String provincie) {
    this.provincie = provincie;
  }

  public PostCodeData buildTransferObject() {
    PostCodeData postCodeData = new PostCodeData();
    postCodeData.setPostcode(postcode);
    postCodeData.setGemeente(gemeente);
    postCodeData.setPlaats(plaats);
    postCodeData.setProvincie(provincie);
    postCodeData.setStraat(straat);
    return postCodeData;
  }
}
