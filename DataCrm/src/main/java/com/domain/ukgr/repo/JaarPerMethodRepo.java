package com.domain.ukgr.repo;

import com.domain.ukgr.idclassdefs.JaarBetaalMethodeId;
import com.domain.ukgr.tables.JaarPerMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JaarPerMethodRepo extends JpaRepository<JaarPerMethod, JaarBetaalMethodeId> {
    List<JaarPerMethod>  findByMethodPayerJaarPerJaar(@Param("payerid")  long payerid,
                                                      @Param("jaar")  int jaar,
                                                      @Param("adminlocatie")  long adminlocatie);
    List<JaarPerMethod>  findByMethodPayerJaarPerJaarExclContant(@Param("payerid")  long payerid,
                                                      @Param("jaar")  int jaar,
                                                      @Param("adminlocatie")  long adminlocatie,
                                                      @Param("betaalmethode")  String betaalmethode);
    //
}
