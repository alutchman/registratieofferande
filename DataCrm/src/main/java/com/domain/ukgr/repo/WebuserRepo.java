package com.domain.ukgr.repo;

import com.domain.ukgr.tables.Webuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WebuserRepo extends JpaRepository<Webuser, String> {

    Webuser findWebuserByUseridAndWachtwoord(String userid, String wachtwoord);

    Webuser findWebuserByUserid(String userid);

    @Query("SELECT e FROM  Webuser e WHERE  " +
            "e.voornaam like :voornaam AND e.achternaam like :achternaam AND e.userid <> :loggedin")
    List<Webuser> searchContainNotUserID(@Param("loggedin") String userid, @Param("voornaam") String voornaam, @Param("achternaam") String achternaam);

}
