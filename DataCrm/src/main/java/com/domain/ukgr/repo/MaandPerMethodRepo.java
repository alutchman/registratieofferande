package com.domain.ukgr.repo;

import com.domain.ukgr.idclassdefs.MaandBetallMethodeId;
import com.domain.ukgr.tables.MaandPerMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MaandPerMethodRepo extends JpaRepository<MaandPerMethod, MaandBetallMethodeId> {
    List<MaandPerMethod> findByMethodPayerInMaandPerJaar(@Param("adminlocatie") Long adminlocatie,
                                                         @Param("payerid") Long payerid,
                                                         @Param("jaar") int jaar);
}
