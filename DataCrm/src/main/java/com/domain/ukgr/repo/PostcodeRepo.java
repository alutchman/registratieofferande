package com.domain.ukgr.repo;

import com.domain.ukgr.tables.Postcodes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostcodeRepo extends JpaRepository<Postcodes, Long> {
    List<Postcodes> findByPostcode(@Param("postcode") String postCode);
}
