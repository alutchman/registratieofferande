package com.domain.ukgr.repo;

import com.domain.ukgr.tables.ReportQuartz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportQuartzRepo extends JpaRepository<ReportQuartz, Long> {

}
