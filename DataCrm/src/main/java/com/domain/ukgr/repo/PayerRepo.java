package com.domain.ukgr.repo;

import com.domain.ukgr.tables.Payer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface PayerRepo extends JpaRepository<Payer, Long> {
    @Query("SELECT t FROM Payer t where t.postcode = ?1 and t.nummer = ?2 ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    List<Payer> findbyPostcodeHuisNummer(String postCode, int huisnummer);

    @Query("SELECT t FROM Payer t where t.achternaam like ?1 ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    List<Payer> findByLastName(String achternaam);

    @Query("SELECT t FROM Payer t where t.postcode = ?1 and t.voornaam = ?2 and t.achternaam = ?3 ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    List<Payer>  findByAdresAndName(String postcode, String voornaam, String achternaam);

    @Query("SELECT t FROM Payer t where t.achternaam like ?1 and t.voornaam like ?2 ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    List<Payer> findByLastNameFirstName(String lastName, String firstName);


    @Query(value = "SELECT e FROM Payer e where e.id IN " +
            "                        ( select f.payer.id from  Transacties f " +
            "                             WHERE " +
            "                              f.payer.id=e.id and " +
            "                              f.adminlocatie=:locatie_id and " +
            "                              YEAR(f.transactiedatum)=:jaar " +
            "                        ) " +
            "                         ORDER BY e.voornaam ASC, e.achternaam ASC ")
    List<Payer> payerVoorJaarList(@Param("locatie_id") Long locatieID,@Param("jaar")  int year, Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT count(e.id) FROM payer e WHERE e.id IN " +
                    "( SELECT f.payerid from  transacties f " +
                    "  WHERE " +
                    "  f.payerid=e.id and " +
                    "  f.adminlocatie = :locatie_id and " +
                    "  YEAR(f.transactiedatum) = :jaar"+
                    ")")
    List<BigInteger> findPayersForYear(@Param("locatie_id") Long locatieID,@Param("jaar")  int year);
}
