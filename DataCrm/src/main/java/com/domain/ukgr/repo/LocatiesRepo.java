package com.domain.ukgr.repo;

import com.domain.ukgr.tables.Locaties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocatiesRepo extends JpaRepository<Locaties, Long> {

    Locaties findByUkgrcode(int ukgrCode);

    List<Locaties> allAtLocatieForYear(@Param("jaar")  int year, @Param("repType")  int repType);

    List<Locaties> usedLocationByPayer(@Param("payerid")  long payerid);

    Locaties ukgrCodeUsedByOther(@Param("id")  long id, @Param("ukgrcode")  int ukgrcode);

    List<Locaties> receiveOrderedWoonpl();

    List<Locaties> receiveOrderedUkgrCode();
}
