package com.domain.ukgr.idclassdefs;

import java.io.Serializable;

public class MaandBetallMethodeId implements Serializable {
    private static final long serialVersionUID = 5652614847025915939L;

    private Long payerid;

    private Long adminlocatie;

    private String betaalmethode;

    private int jaar;

    private int maand;

    public Long getPayerid() {
        return payerid;
    }

    public void setPayerid(Long payerid) {
        this.payerid = payerid;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public void setMaand(int maand) {
        this.maand = maand;
    }

    public String getBetaalmethode() {
        return betaalmethode;
    }

    public void setBetaalmethode(String betaalmethode) {
        this.betaalmethode = betaalmethode;
    }

    public int getJaar() {
        return jaar;

    }

    public int getMaand() {
        return maand;
    }

    public Long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(Long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MaandBetallMethodeId that = (MaandBetallMethodeId) o;

        if (!payerid.equals(that.payerid)) return false;
        if (!betaalmethode.equals(that.betaalmethode)) return false;
        if (jaar != jaar) return false;
        if (!adminlocatie.equals(that.adminlocatie)) return false;
        return maand == that.maand;
    }

    @Override
    public int hashCode() {
        int result = payerid.hashCode();
        result = 31 * result + adminlocatie.hashCode();
        result = 31 * result + betaalmethode.hashCode();
        result = 31 * result + jaar;
        result = 31 * result + maand;
        return result;
    }
}
