package com.domain.ukgr.idclassdefs;

import java.io.Serializable;

public class JaarPersonTransActId implements Serializable {
    private static final long serialVersionUID = -5254956855458995505L;

    private Long adminlocatie;

    private Long payerid;

    private int jaar;

    public Long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(Long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    public Long getPayerid() {
        return payerid;
    }

    public void setPayerid(Long payerid) {
        this.payerid = payerid;
    }

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    @Override
    public int hashCode() {
        int result = payerid.hashCode();
        result = 31 * result + adminlocatie.hashCode();
        result = 31 * result + jaar;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JaarPersonTransActId that = (JaarPersonTransActId) o;
        return getJaar() == that.getJaar() &&
                getAdminlocatie().equals(that.getAdminlocatie()) &&
                getPayerid().equals(that.getPayerid());
    }
}
