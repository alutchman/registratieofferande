package com.domain.ukgr.idclassdefs;

import java.io.Serializable;

public class MaandOverzichtId implements Serializable {
    private static final long serialVersionUID = 583882348423612607L;


    private Long payerid;

    private String categorie;

    private int jaar;

    private int maand;

    public Long getPayerid() {
        return payerid;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setPayerid(Long payerid) {
        this.payerid = payerid;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public void setMaand(int maand) {
        this.maand = maand;
    }

    public int getJaar() {
        return jaar;

    }

    public int getMaand() {
        return maand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MaandOverzichtId that = (MaandOverzichtId) o;

        if (!payerid.equals(that.payerid)) return false;
        if (!categorie.equals(that.categorie)) return false;
        if (jaar != jaar) return false;
        return maand == that.maand;
    }

    @Override
    public int hashCode() {
        int result = payerid.hashCode();
        result = 31 * result + categorie.hashCode();
        result = 31 * result + jaar;
        result = 31 * result + maand;
        return result;
    }
}
