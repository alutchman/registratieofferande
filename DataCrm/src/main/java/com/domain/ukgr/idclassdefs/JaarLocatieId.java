package com.domain.ukgr.idclassdefs;

import java.io.Serializable;
import java.util.Objects;

public class JaarLocatieId implements Serializable {
    private static final long serialVersionUID = 9199080964467974585L;

    private int ukgrcode;

    private int jaar;

    public int getUkgrcode() {
        return ukgrcode;
    }

    public void setUkgrcode(int ukgrcode) {
        this.ukgrcode = ukgrcode;
    }

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JaarLocatieId that = (JaarLocatieId) o;
        return getUkgrcode() == that.getUkgrcode() &&
                getJaar() == that.getJaar();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUkgrcode());
    }
}
