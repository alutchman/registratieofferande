package com.domain.ukgr.idclassdefs;

import java.io.Serializable;

public class JaarOverzichtId implements Serializable {

    private static final long serialVersionUID = -3291118473503073759L;

    private Long payerid;

    private String categorie;

    private int jaar;

    public Long getPayerid() {
        return payerid;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setPayerid(Long payerid) {
        this.payerid = payerid;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public int getJaar() {
        return jaar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JaarOverzichtId that = (JaarOverzichtId) o;

        if (!payerid.equals(that.payerid)) return false;
        if (!categorie.equals(that.categorie)) return false;
        return jaar==jaar;
    }

    @Override
    public int hashCode() {
        int result = payerid.hashCode();
        result = 31 * result + categorie.hashCode();
        result = 31 * result + jaar;
        return result;
    }
}
