package com.domain.ukgr.idclassdefs;

import java.io.Serializable;

public class JaarBetaalMethodeId implements Serializable {

    private static final long serialVersionUID = 3374677691154593461L;

    private Long payerid;

    private Long adminlocatie;

    private String betaalmethode;

    private int jaar;

    public Long getPayerid() {
        return payerid;
    }

    public void setPayerid(Long payerid) {
        this.payerid = payerid;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public int getJaar() {
        return jaar;
    }

    public String getBetaalmethode() {
        return betaalmethode;
    }

    public void setBetaalmethode(String betaalmethode) {
        this.betaalmethode = betaalmethode;
    }


    public Long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(Long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JaarBetaalMethodeId that = (JaarBetaalMethodeId) o;

        if (!payerid.equals(that.payerid)) return false;
        if (!betaalmethode.equals(that.betaalmethode)) return false;
        if (!adminlocatie.equals(that.adminlocatie)) return false;

        return jaar==jaar;
    }

    @Override
    public int hashCode() {
        int result = payerid.hashCode();
        result = 31 * result + betaalmethode.hashCode();
        result = 31 * result + jaar;
        return result;
    }
}
