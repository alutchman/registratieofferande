package com.domain.ukgr.idclassdefs;

import java.io.Serializable;
import java.util.Objects;

public class PostCodeStraat implements Serializable {

    private static final long serialVersionUID = 2029031176081312009L;

    private String postcode;

    private String straat;

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostCodeStraat that = (PostCodeStraat) o;
        return getPostcode().equals(that.getPostcode()) &&
                getStraat().equals(that.getStraat());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPostcode());
    }
}
