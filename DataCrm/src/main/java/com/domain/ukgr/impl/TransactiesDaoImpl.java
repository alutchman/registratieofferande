package com.domain.ukgr.impl;

import com.domain.ukgr.dao.DaoTransacties;
import com.domain.ukgr.tables.Payer;
import com.domain.ukgr.tables.Transacties;
import com.shared.transfers.BetaalMethode;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class TransactiesDaoImpl extends BaseDaoIdImpl<Transacties> implements DaoTransacties<Transacties> {

    @Override
    public List<Transacties> findByPayerYear(Long adminlocatie, Payer payer, int jaar, int offset, int count) {
        Query query1 = entityManager.createNamedQuery(Transacties.PAYERVOORJAAR);
        query1.setParameter("payer",payer);
        query1.setParameter("jaar",jaar);
        query1.setParameter("adminlocatie",adminlocatie);
        query1.setFirstResult(offset*count);
        query1.setMaxResults(count);
        return query1.getResultList();
    }

    @Override
    public List<Transacties> findForCorrecting(String webuserid, Date aanmaakdatum, String postcode,
                                               String voornaam, String achternaam) {
        String postCodeChk = postcode != null && postcode.trim().length() > 0 ? postcode.trim()+"%"  : "%";
        String achternaamChk = achternaam != null && achternaam.trim().length() > 0 ? achternaam.trim()+"%"  : "%";
        String voornaamChk = voornaam != null && voornaam.trim().length() > 0 ? voornaam.trim()+"%"  : "%";

        Query query1 = entityManager.createNamedQuery(Transacties.CORRECTIONSEARCH);

        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(aanmaakdatum); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, 48); // adds one hour
        Date endDate  =cal.getTime();

        query1.setParameter("postcode",postCodeChk);
        query1.setParameter("aanmaakdatumstart",new Timestamp(aanmaakdatum.getTime()));
        query1.setParameter("aanmaakdatumend",new Timestamp(endDate.getTime()));
        query1.setParameter("webuserid",webuserid);
        query1.setParameter("voornaam",voornaamChk);
        query1.setParameter("achternaam",achternaamChk);

        return query1.getResultList();
    }

    @Override
    public Integer findTransactionsInYear(Long payerID, int year, long adminlocatie) {

        String hql = "select count(payerid) from transacties WHERE payerid=:payerid and YEAR(transactiedatum)=:year  and adminlocatie = :adminloc";

        Query query = entityManager.createNativeQuery(hql);
        query.setParameter("payerid",payerID);
        query.setParameter("year",year);
        query.setParameter("adminloc",adminlocatie);
        List listResult = query.getResultList();
        BigInteger number = (BigInteger) listResult.get(0);

        return number.intValue();
    }

    @Override
    public void removePayerTransactions(Payer payer) {
        String hql = "DELETE from transacties WHERE payerid=:payerid";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter("payerid",payer.getId());
        query.executeUpdate();



    }
}
