package com.domain.ukgr.impl;

import com.domain.ukgr.dao.DaoLocatieJaar;
import com.domain.ukgr.tables.JaarPerLocatie;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class DaoLocatieJaarImpl extends BaseDaoImpl<JaarPerLocatie> implements DaoLocatieJaar<JaarPerLocatie> {
    @Override
    public List<JaarPerLocatie> findForJaar(int jaar) {
        Query query1 = entityManager.createNamedQuery(JaarPerLocatie.ALLLOCATIEJAAR);
        query1.setParameter("jaar",jaar);
        return query1.getResultList();
    }

    @Override
    public JaarPerLocatie findForJaarUkgrCode(int jaar, int ukgrcode) {
        Query query1 = entityManager.createNamedQuery(JaarPerLocatie.LOCATIEJAAR);    ;
        query1.setParameter("ukgrcode",ukgrcode);
        query1.setParameter("jaar",jaar);
        List<JaarPerLocatie> items = query1.getResultList();
        return items.size() > 0 ? items.get(0) : null;
    }
}
