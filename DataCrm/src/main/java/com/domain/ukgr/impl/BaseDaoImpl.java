package com.domain.ukgr.impl;


import com.domain.ukgr.dao.BaseDao;
import com.domain.ukgr.entity.BaseEntity;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@MappedSuperclass
@Transactional
public abstract class BaseDaoImpl<T extends BaseEntity> implements BaseDao<T> {

    protected final Class<T> persistentClass = (Class<T>) ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];

    protected final String selectAll   = "FROM "+persistentClass.getSimpleName();
    protected final String selectAllOrder   = selectAll + " ORDER BY %s ";

    protected final String selectWhere = selectAll + " WHERE %s ";
    protected final String selectWhereOrder = selectWhere + " ORDER BY %s ";

    protected final Table table = persistentClass.getAnnotation(Table.class);

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @PersistenceContext
    protected EntityManager entityManager;


    public Session getSession(){
       return entityManager.unwrap(Session.class);
    }


    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<T> fetchAll() {
        return createQuery().getResultList();
    }


    public Query createQuery(){
       return entityManager.createQuery(selectAll, persistentClass);
    }


    public Query createQuery(String conditions){
        return entityManager.createQuery(String.format(selectWhere, conditions), persistentClass);
    }

    public Query createOrderedQuery(String order){
        return entityManager.createQuery(String.format(selectAllOrder, order), persistentClass);
    }

    public Query createOrderedQuery(String conditions,String order){
        return entityManager.createQuery(String.format(selectWhereOrder, conditions, order), persistentClass);
    }


    /**
     *
     * @param model
     * @return
     */
    public boolean add(T model) {
        entityManager.persist(model);
        return true;
    }

    /**
     *
     * @param model
     * @return
     */
    public T update(T model) {
        return entityManager.merge(model);
    }

    public void delete(T model) {
        entityManager.remove(model);
    }

}
