package com.domain.ukgr.impl;

import com.domain.ukgr.dao.BaseDaoId;
import com.domain.ukgr.entity.BaseEntityId;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.MappedSuperclass;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;


@MappedSuperclass
@Transactional(propagation = Propagation.REQUIRED)
public class BaseDaoIdImpl<T extends BaseEntityId>  extends BaseDaoImpl<T> implements BaseDaoId<T>{
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public T fetchById(long id)  {
        return (T) entityManager.find(persistentClass, id);
    }


    @Override
    public Long lastId(){
        Query query = entityManager.createNativeQuery("SELECT LAST_INSERT_ID()");
        List listResult = query.getResultList();
        if (listResult.size() == 0) {
            return null;
        }
        BigInteger number = (BigInteger) listResult.get(0);
        return number.longValue();
    }

    @Override
    public void flush(){
        entityManager.flush();
    }
}
