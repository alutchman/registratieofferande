package com.domain.ukgr.impl;

import com.domain.ukgr.dao.DaoJaarPersonTransacties;
import com.domain.ukgr.tables.JaarPersonTransacties;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

@Repository
public class DaoJaarPersonTransactiesImpl extends BaseDaoImpl<JaarPersonTransacties> implements DaoJaarPersonTransacties<JaarPersonTransacties> {
    @Override
    public List<JaarPersonTransacties> findForJaarPostCodeSort(int jaar, Long locatieId, int offset, int count) {
        Query query1 = entityManager.createNamedQuery(JaarPersonTransacties.POSTCODETRANSACTJAAR);
        return getListSorted(query1, jaar, locatieId, offset, count);
    }


    @Override
    public List<JaarPersonTransacties> findForJaarNameSort(int jaar, Long locatieId, int offset, int count) {
        Query query1 = entityManager.createNamedQuery(JaarPersonTransacties.PERSONTRANSACTJAAR);
        return getListSorted(query1, jaar, locatieId, offset, count);
    }


    @Override
    public Integer findPayersForYearAtLocation(Long locatieID, int jaar) {
        String hql = "SELECT count(A.adminlocatie) FROM year_personpayer A " +
                "WHERE A.jaar=:jaar and A.adminlocatie=:locatie_id "  ;

        Query query = entityManager.createNativeQuery(hql);
        query.setParameter("locatie_id",locatieID);
        query.setParameter("jaar",jaar);
        List listResult = query.getResultList();
        BigInteger number = (BigInteger) listResult.get(0);

        return number.intValue();
    }

    private List<JaarPersonTransacties> getListSorted(Query query1, int jaar, Long locatieId, int offset, int count ){
        query1.setParameter("jaar",jaar);
        query1.setParameter("adminlocatie",locatieId);
        query1.setFirstResult(offset*count);
        query1.setMaxResults(count);
        return query1.getResultList();
    }
}
