package com.domain.ukgr.dao;

import com.domain.ukgr.entity.BaseEntityId;

public interface BaseDaoId<T extends BaseEntityId>  extends BaseDao<T> {
    T fetchById(long id);

    Long lastId();

    void flush();
}
