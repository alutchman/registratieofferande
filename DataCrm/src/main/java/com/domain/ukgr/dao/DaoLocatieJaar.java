package com.domain.ukgr.dao;

import com.domain.ukgr.entity.BaseEntity;
import com.domain.ukgr.idclassdefs.JaarLocatieId;
import com.domain.ukgr.tables.JaarPerLocatie;

import java.util.List;

public interface DaoLocatieJaar <T extends BaseEntity> extends BaseDao<T> {
    List<JaarPerLocatie> findForJaar(int jaar);

    JaarPerLocatie findForJaarUkgrCode(int jaar, int ukgrcode);

}
