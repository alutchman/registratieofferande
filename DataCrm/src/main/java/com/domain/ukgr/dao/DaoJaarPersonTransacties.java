package com.domain.ukgr.dao;

import com.domain.ukgr.entity.BaseEntity;
import com.domain.ukgr.tables.JaarPersonTransacties;

import java.util.List;

public interface DaoJaarPersonTransacties <T extends BaseEntity> extends BaseDao<T> {
    List<JaarPersonTransacties> findForJaarPostCodeSort(int jaar,Long locatieId, int offset, int count);

    List<JaarPersonTransacties> findForJaarNameSort(int jaar, Long locatieId, int offset, int count);

    Integer findPayersForYearAtLocation(Long locatieID, int jaar);
}
