package com.domain.ukgr.dao;


import com.domain.ukgr.entity.BaseEntityId;
import com.domain.ukgr.tables.Payer;
import com.domain.ukgr.tables.Transacties;

import java.util.Date;
import java.util.List;

public interface DaoTransacties<T extends BaseEntityId> extends BaseDaoId<T> {

    List<Transacties> findByPayerYear(Long adminlocatie, Payer payer, int jaar, int offset, int count);

    List<Transacties> findForCorrecting(String webuserId, Date aanmaakdatum, String postcode,
                                        String voornaam, String achternaam);

    Integer findTransactionsInYear(Long payerID, int jaar, long adminlocatie);

    void removePayerTransactions(Payer payer);
}
