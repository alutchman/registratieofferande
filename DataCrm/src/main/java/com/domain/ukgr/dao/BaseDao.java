package com.domain.ukgr.dao;

import com.domain.ukgr.entity.BaseEntity;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.List;

public interface BaseDao<T extends BaseEntity> {

    public Session getSession();
    public List<T> fetchAll() ;
    public Query createQuery();
    public Query createQuery(String conditions);
    public boolean add(T model) ;
    public void delete(T model);
    public T update(T model);

}
