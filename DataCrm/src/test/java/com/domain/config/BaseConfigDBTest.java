package com.domain.config;

import com.jndi.env.JndiTemplateDataBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.naming.Context;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import static com.domain.config.HibernateConfig.DATA_SOURCE_NAME;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertSame;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;


public class BaseConfigDBTest {
    private BaseConfigDB baseConfigDB = new BaseConfigDB();

    @Test
    public void fetchJndiTemplate() {
        JndiTemplateDataBase template =  baseConfigDB.fetchJndiTemplate();
        assertNotNull(template);
        String result = template.getProperty("hibernate.dialect");
        assertEquals("org.hibernate.dialect.H2Dialect", result);
    }

    @Test
    public void getDataSource() {

        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                InitialContextFactoryForTest.class.getName());
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).setName("AuditLog")
                .build();
        InitialContextFactoryForTest.bind(DATA_SOURCE_NAME, dataSource);



        DataSource dataSourceCreated = baseConfigDB.getDataSource(DATA_SOURCE_NAME);
        assertNotNull(dataSourceCreated);
        assertSame(dataSource, dataSourceCreated);

        System.clearProperty(Context.INITIAL_CONTEXT_FACTORY);
    }

    @Test
    public void createJdbCTemplate() {

        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                InitialContextFactoryForTest.class.getName());
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).setName("AuditLog")
                .build();
        InitialContextFactoryForTest.bind(DATA_SOURCE_NAME, dataSource);



        DataSource dataSourceCreated = baseConfigDB.getDataSource(DATA_SOURCE_NAME);
        assertNotNull(dataSourceCreated);
        assertSame(dataSource, dataSourceCreated);

        JdbcTemplate template =  baseConfigDB.createJdbCTemplate(dataSourceCreated);
        assertNotNull(template);

        System.clearProperty(Context.INITIAL_CONTEXT_FACTORY);
    }

    @Test
    public void entityManagerFactory() {
        JndiTemplateDataBase template =  baseConfigDB.fetchJndiTemplate();

        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                InitialContextFactoryForTest.class.getName());
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).setName("AuditLog")
                .build();
        InitialContextFactoryForTest.bind(DATA_SOURCE_NAME, dataSource);

        DataSource dataSourceCreated = baseConfigDB.getDataSource(DATA_SOURCE_NAME);

        LocalContainerEntityManagerFactoryBean emf =
        baseConfigDB.entityManagerFactory(dataSourceCreated, template);

        assertNotNull(emf);


        System.clearProperty(Context.INITIAL_CONTEXT_FACTORY);

    }

    @Test
    public void transactionManager() {
        EntityManagerFactory emf2 = mock(EntityManagerFactory.class);
        PlatformTransactionManager trx =   baseConfigDB.transactionManager(emf2);
        assertNotNull(trx);
    }
}