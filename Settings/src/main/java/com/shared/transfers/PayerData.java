package com.shared.transfers;


import org.springframework.util.StringUtils;

import java.io.Serializable;

public class PayerData implements Serializable,Cloneable{

    private static final long serialVersionUID = 2893901023395345698L;

    private Long id;

    private String voornaam;


    private String achternaam;

    private String geslacht;

    private Integer nummer;

    private String toevoeging;

    private String woonplaats;

    private String postcode;

    private String straatnaam;

    private String telefoon;

    public String getVoornaam() {
        return voornaam;
    }

    public String getTitle(){
        return geslacht.equalsIgnoreCase("B") ? achternaam : voornaam+" "+ achternaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getGeslacht() {
        return geslacht;
    }

    public void setGeslacht(String geslacht) {
        this.geslacht = geslacht;
    }

    public Integer getNummer() {
        return nummer;
    }

    public void setNummer(Integer nummer) {
        this.nummer = nummer;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getStraatnaam() {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam) {
        this.straatnaam = straatnaam;
    }

    public String getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(String telefoon) {
        this.telefoon = telefoon;
    }

    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getUniqueName(String extension){

        return  String.format("%s%s_%d.%s", StringUtils.capitalize(voornaam), StringUtils.capitalize(achternaam), id, extension);
    }

    public String getFullName(){
        return  String.format("%s %s", StringUtils.capitalize(voornaam), StringUtils.capitalize(achternaam));
    }
}
