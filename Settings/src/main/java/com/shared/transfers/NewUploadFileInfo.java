package com.shared.transfers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;

public class NewUploadFileInfo {
    private static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat dateFormatPath = new SimpleDateFormat("yyyyMMdd.HH_mm_ss_SSS");

    private final String baseDir;

    private PayerData payerData;
    private TransactieData transactieData;

    private String pathName;
    private String fileName;

    private Exception lastException;

    private String yearLocation;
    private String payerLocation;

    public NewUploadFileInfo(String baseDir , PayerData payerData, int year) {
        this.baseDir = baseDir.trim();
        this.payerData = payerData;
        this.transactieData = null;
        yearLocation = String.valueOf(year);
        payerLocation = calculaterPayerDir();
        pathName = baseDir+yearLocation+File.separator+ payerLocation;
    }


    public NewUploadFileInfo(String baseDir , PayerData payerData, TransactieData transactieData) {
        this.baseDir = baseDir.trim();
        this.payerData = payerData;
        this.transactieData = transactieData;
        initFileInfo(false);
    }

    private void initFileInfo(boolean isNew){
        lastException = null;
        yearLocation = yearFormat.format(getTransactieData().getTransactiedatum());
        this.payerLocation = calculaterPayerDir();
        pathName = baseDir+yearLocation+File.separator+ payerLocation;
        fileName = isNew ? calculaterFileName() : transactieData.getScanfile();
    }


    public String getDownloadLocation(){
        if (transactieData == null || transactieData.getScanfile() == null) {
            return null;
        }
        return pathName + File.separator + transactieData.getScanfile();
    }

    private String calculaterFileName(){
        String transactieDatum = dateFormat.format(getTransactieData().getTransactiedatum());
        String aanmaakDatum = dateFormatPath.format(getTransactieData().getCreation_time());
        String newFileNaam =  String.format("%s_%s.pdf",transactieDatum, aanmaakDatum);
        return newFileNaam;
    }

    private String calculaterPayerDir(){
        String payerLocationTMP = String.format("%s_%s",getPayerData().getAchternaam(), getPayerData().getVoornaam());
        return payerLocationTMP.replace(" ", "");
    }

    public void createFilePath(InputStream input){
        try {
            initFileInfo(true);
            new File(baseDir + yearLocation).mkdir();
            new File(baseDir + yearLocation + File.separator + payerLocation).mkdir();
            Files.copy(input, new File(pathName, fileName).toPath());
        } catch (IOException e) {
            lastException = e;
        }
    }


    public void deleteOldInfo(String scanfile) {
        if (scanfile == null) {
            return;
        }

        try {
            String jaar = scanfile.substring(0,4);
            String OldpathName = baseDir + jaar + File.separator+ payerLocation;
            Files.deleteIfExists(new File(OldpathName, scanfile).toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PayerData getPayerData() {
        return payerData;
    }

    public TransactieData getTransactieData() {
        return transactieData;
    }

    public Exception getLastException() {
        return lastException;
    }

    public String getPathName() {
        return pathName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getYearLocation() {
        return yearLocation;
    }

    public String getPayerLocation() {
        return payerLocation;
    }

}
