package com.shared.transfers;

import java.io.Serializable;

public class LocatieData implements Serializable,Cloneable {
    private static final long serialVersionUID = 3049838063512275692L;

    protected Long id;

    private String straatnaam;

    private Integer nummer;

    private String toevoeging;

    private String woonplaats;

    private String postcode;

    private String telefoon;

    private Integer ukgrcode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStraatnaam() {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam) {
        this.straatnaam = straatnaam;
    }

    public Integer getNummer() {
        return nummer;
    }

    public void setNummer(Integer nummer) {
        this.nummer = nummer;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(String telefoon) {
        this.telefoon = telefoon;
    }

    public Integer getUkgrcode() {
        return ukgrcode;
    }

    public void setUkgrcode(Integer ukgrcode) {
        this.ukgrcode = ukgrcode;
    }

    public String getToevoeging() {
        if (toevoeging == null) {
            return "";
        }
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    @Override
    public String toString() {
        return woonplaats + ", " + straatnaam + " UKGR CODE="+ ukgrcode;
    }

    @Override
    public LocatieData clone()throws CloneNotSupportedException{
        return (LocatieData) super.clone();
    }
}
