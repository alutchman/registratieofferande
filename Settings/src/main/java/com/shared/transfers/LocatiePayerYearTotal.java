package com.shared.transfers;

import java.io.Serializable;
import java.util.Objects;

public class LocatiePayerYearTotal implements Serializable,Cloneable {
    private static final long serialVersionUID = 1373279668233383540L;

    private Long adminlocatie;

    private Long payerid;

    private Integer jaar ;

    private Double  ontvangen ;

    private String voornaam;

    private String achternaam;

    private String straatnaam;

    private Integer nummer;

    private String toevoeging;

    private String postcode;

    private String woonplaats;

    public Long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(Long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    public Long getPayerid() {
        return payerid;
    }

    public void setPayerid(Long payerid) {
        this.payerid = payerid;
    }

    public Integer getJaar() {
        return jaar;
    }

    public void setJaar(Integer jaar) {
        this.jaar = jaar;
    }

    public Double getOntvangen() {
        return ontvangen;
    }

    public void setOntvangen(Double ontvangen) {
        this.ontvangen = ontvangen;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getStraatnaam() {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam) {
        this.straatnaam = straatnaam;
    }

    public Integer getNummer() {
        return nummer;
    }

    public void setNummer(Integer nummer) {
        this.nummer = nummer;
    }

    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocatiePayerYearTotal that = (LocatiePayerYearTotal) o;
        return getAdminlocatie().equals(that.getAdminlocatie()) &&
                getPayerid().equals(that.getPayerid()) &&
                getJaar().equals(that.getJaar());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPayerid(), getJaar());
    }

    @Override
    public LocatiePayerYearTotal clone()throws CloneNotSupportedException{
        return (LocatiePayerYearTotal) super.clone();
    }
}
