package com.shared.transfers;

import java.io.Serializable;

public class TransactCorrection implements Serializable,Cloneable{
    TransactieData transactieData;
    PayerData payerData;

    public TransactieData getTransactieData() {
        return transactieData;
    }

    public void setTransactieData(TransactieData transactieData) {
        this.transactieData = transactieData;
    }

    public PayerData getPayerData() {
        return payerData;
    }

    public void setPayerData(PayerData payerData) {
        this.payerData = payerData;
    }

    @Override
    public TransactCorrection clone() throws CloneNotSupportedException {
        TransactCorrection result = new TransactCorrection();
        result.setPayerData((PayerData) payerData.clone());
        result.setTransactieData(transactieData.clone());
        return result;
    }
}
