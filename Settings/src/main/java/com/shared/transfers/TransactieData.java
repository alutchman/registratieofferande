package com.shared.transfers;

import java.io.Serializable;
import java.util.Date;

public class TransactieData implements Serializable,Cloneable{
    private static final long serialVersionUID = -2790248039848641678L;
    private Long id;

    private Double bedrag = 1.0;
    private BetaalMethode betaalmethode = BetaalMethode.CONTANT;

    private OfferType categorie = OfferType.OFFERANDE;
    private Date transactiedatum = new Date();

    private Date creation_time;


    private Long adminlocatie =0l;

    private String scanfile;

    private String payerNaam;

    private String payerPostcode;

    private String payerAdres;

    private String payerWoonplaats;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBedrag() {
        return bedrag;
    }

    public void setBedrag(Double bedrag) {
        this.bedrag = bedrag;
    }

    public BetaalMethode getBetaalmethode() {
        return betaalmethode;
    }

    public void setBetaalmethode(BetaalMethode betaalmethode) {
        this.betaalmethode = betaalmethode;
    }

    public Date getTransactiedatum() {
        return transactiedatum;
    }

    public void setTransactiedatum(Date transactiedatum) {
        this.transactiedatum = transactiedatum;
    }

    public Long getAdminlocatie() {
        return adminlocatie;
    }

    public void setAdminlocatie(Long adminlocatie) {
        this.adminlocatie = adminlocatie;
    }

    public OfferType getCategorie() {
        return categorie;
    }

    public void setCategorie(OfferType categorie) {
        this.categorie = categorie;
    }

    public String getScanfile() {
        return scanfile;
    }

    public void setScanfile(String scanfile) {
        this.scanfile = scanfile;
    }

    public String getPayerNaam() {
        return payerNaam;
    }

    public void setPayerNaam(String payerNaam) {
        this.payerNaam = payerNaam;
    }

    public String getPayerPostcode() {
        return payerPostcode;
    }

    public void setPayerPostcode(String payerPostcode) {
        this.payerPostcode = payerPostcode;
    }

    public String getPayerAdres() {
        return payerAdres;
    }

    public void setPayerAdres(String payerAdres) {
        this.payerAdres = payerAdres;
    }

    public String getPayerWoonplaats() {
        return payerWoonplaats;
    }

    public void setPayerWoonplaats(String payerWoonplaats) {
        this.payerWoonplaats = payerWoonplaats;
    }

    public Date getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(Date creation_time) {
        this.creation_time = creation_time;
    }

    @Override
    public TransactieData clone()throws CloneNotSupportedException{
        return (TransactieData) super.clone();
    }
}
