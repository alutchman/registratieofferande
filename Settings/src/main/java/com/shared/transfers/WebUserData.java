package com.shared.transfers;

import org.joda.time.DateTime;

import java.io.Serializable;

public class WebUserData implements Serializable,Cloneable{
    private static final long serialVersionUID = -1642736405879013313L;

    private String userid;

    private String voornaam;

    private String achternaam;

    private UserLevel currentlevel;

    private boolean validUser;

    private DateTime  started = new DateTime();

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public DateTime getStarted() {
        return started;
    }

    public void setStarted(DateTime started) {
        this.started = started;
    }

    public boolean isValidUser() {
        return validUser;
    }

    public void setValidUser(boolean validUser) {
        this.validUser = validUser;
    }

    public UserLevel getCurrentlevel() {
        return currentlevel;
    }

    public void setCurrentlevel(UserLevel currentlevel) {
        this.currentlevel = currentlevel;
    }

    @Override
    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }
}

