package com.shared.transfers;

import java.io.Serializable;

public class LocatieYearTotal implements Serializable,Cloneable {
    private static final long serialVersionUID = -4844101102497094733L;

    private int ukgrcode;

    private int jaar;

    private Double ontvangen;

    private String woonplaats;

    private String postcode;

    private String straatnaam;

    private int nummer;

    private String toevoeging;

    public int getUkgrcode() {
        return ukgrcode;
    }

    public void setUkgrcode(int ukgrcode) {
        this.ukgrcode = ukgrcode;
    }

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public Double getOntvangen() {
        return ontvangen;
    }

    public void setOntvangen(Double ontvangen) {
        this.ontvangen = ontvangen;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getStraatnaam() {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam) {
        this.straatnaam = straatnaam;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public LocatieYearTotal clone()throws CloneNotSupportedException{
        return (LocatieYearTotal) super.clone();
    }
}
